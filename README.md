# Psyncho-Graph

###### // TODO: Add project description here

## Getting Started

###### // TODO: Add project Getting Started here


### Prerequisites

There are a few things you need to do before you begin using Psyncho-Graph

#### 1. Install MongoDb

##### OSX: Use [Homebrew](https://brew.sh/#install) to install [MongoDb on OSX](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/).
##### Linux: Read how to install [MongoDb on Linux](https://docs.mongodb.com/manual/administration/install-on-linux/).
##### Windows: Read how to install [MongoDb on Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)

#

#### 2. Setup MongoDb ReplicaSet for Db Streams


##### Stop the mongodb service 

##### For OSX:
```bash
brew services stop mongodb  
```

##### For Linux:
```bash
// TODO: ADD LINUX COMMAND FOR STOPPING MONGODB SERVICE
```

#### Edit mongod.conf
```bash
sudo vim /usr/local/etc/mongod.conf
```

#### Add these (2) Lines to config
```vim
replication:
  replSetName: "rs01"
```

#### Restart MongoDb Service

##### For OSX
```bash
brew services start mongodb
```

##### For Linux
```bash
// TODO: ADD LINUX COMMAND FOR STARTING MONGODB SERVICE
```

##### For Windows
```
// TODO: ADD WINDOWS COMMAND FOR STARTING MONGODB SERVICE
```

#### Initiate MongoDb Replication Set

##### OSX/Linux: From Terminal run the following commands:
```bash
mongo
rs.initiate()
exit
```

##### Windows:
```
// TODO: ADD WINDOWS PROCESS FOR INITIALIZING REPLICATION SET
```

## Installation
1. CLONE PROJECT

#### Install Dependencies and Build/Run Psyncho-Graph
Use the package manager [yarn](https://yarnpkg.com/en/) to install psyncho-graph and all related dependencies.

##### Install Dependencies
```bash
yarn
```

##### Build/Run Psyncho-Graph (DEVELOPMENT MODE)
```bash
yarn dev:ex
```

## Usage

```bash
// TODO: ADD COMMAND(S)/EXAMPLE(S) FOR RUNNING
```

### Installing
```
// TODO: ADD INSTALLATION/SETUP GUIDE
```

## Running the tests

```
// TODO: Explain how to run the automated tests for this system
```

### Break down into end to end tests

//TODO: Explain what these tests test and why

```
Give example(s)
```

## Deployment
```
// TODO: Add notes about how to deploy this on a live system
```

## Built With

* [//TODO:](http://www.addsomeothers.com) - Add other packages used
* [//TODO:](http://www.addsomeothers.com) - Add other packages used
* [//TODO:](http://www.addsomeothers.com) - Add other packages used
* [GraphQL](https://graphql.org/) - A query language for your API
* [Jest](https://jestjs.io/) - A delightful JavaScript Testing Framework with a focus on simplicity. 
* [Yarn](https://yarnpkg.com/en/) - Fast, Reliable, and Secure Dependency Management
* [MongoDb](https://www.mongodb.com/what-is-mongodb) - A document database with the scalability and flexibility that you want with the querying and indexing that you need.

## Contributing
```
// TODO: CREATE CONTRIBUTING.md FILE
```
Please read [CONTRIBUTING.md](https://link.to.contributing.md.file) for details on our code of conduct, and the process for submitting pull requests to us.

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Versioning
```
// TODO: ADD TAGS AND FINAL REPO LINK
```
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository]. 


```
// TODO: ADD CONTRIBUTORS LINK
```
See also the list of [contributors] who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to others whose code was used
* Inspiration
* etc