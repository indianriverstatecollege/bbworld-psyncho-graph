module.exports = {
  roots: [
    '<rootDir>/src',
  ],
  automock: false,
  coverageDirectory: './coverage',
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/.node/',
    '/jest/',
    '/docs/',
    '/doc-coverage/',
    '/database/',
    '/models/',
  ],
  setupFiles: [
    './jest.setup.ts',
  ],
  preset: 'ts-jest',
  collectCoverageFrom: [
    'src/**/*.ts',
    '!src/server/index.ts',
    '!src/server/configs/**',
    '!src/server/database/models/blackboard/**',
    '!src/**/*.mock.ts',
  ],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json',
    },
  },
  testEnvironment: 'node',
  testPathIgnorePatterns: [
    '/node_modules/',
    '/build/',
  ],
  resetMocks: true,
  resetModules: true,
  moduleFileExtensions: [
    'js',
    'json',
    'node',
    'ts',
  ],
  coverageReporters: [
    'lcov',
    'html',
  ],
  reporters: [
    'default',
    [
      './node_modules/jest-html-reporter',
      {
        pageTitle: 'Psyncho Test Report',
      },
    ],
  ],
  moduleNameMapper: {
    '^mongoose$': '<rootDir>/node_modules/mongoose',
  },
}