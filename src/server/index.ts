import 'reflect-metadata';
import { Container } from 'inversify';
import { Server, App, IServerConfig, INJECTION_TYPES } from './modules';
import { testServerConfig } from './configs/express-server.config';

const container = new Container();
container.bind<Server>(INJECTION_TYPES.Server).to(Server);
container.bind<App>(INJECTION_TYPES.App).to(App);
container
    .bind<IServerConfig>(INJECTION_TYPES.ServerConfig)
    .toConstantValue(testServerConfig);

const server = container.get<Server>(INJECTION_TYPES.Server);
server.runServer();
