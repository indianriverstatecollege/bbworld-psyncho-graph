import appFactory from './modules/appFactory';
import setupBbToken from './modules/setup/setupBbToken';
import setupDynamicRoutes from './modules/setup/setupDynamicRoutes';
import { GRAPHQL_CONFIG, APP_CONFIG } from './configs';
import { runCourseData, runCourseDefinitions } from './modules/taskRunner';
import ioStreams from './modules/ioStreams';

const { expressApp, io } = appFactory({
    app: APP_CONFIG,
    graphql: GRAPHQL_CONFIG
});

setupBbToken(expressApp);
setupDynamicRoutes(expressApp);

// runCourseData(expressApp, io);
// runCourseDefinitions(expressApp, io);

// TODO: why doesn't the server pick up on emissions and log: io.emit('channel') -> io.on('channel')
ioStreams(io);
