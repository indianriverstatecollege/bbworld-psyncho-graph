import { prop, Typegoose } from 'typegoose';

enum YesNo {
    Yes = 'Yes',
    No = 'No'
}

enum TermType {
    Continuous = 'Continuous',
    DateRange = 'DateRange',
    FixedNumDays = 'FixedNumDays'
}

class TermAvailabilityDuration extends Typegoose {
    @prop()
    type: TermType;

    @prop()
    start: string;

    @prop()
    end: string;

    @prop()
    daysOfUse: number;
}

class TermAvailability extends Typegoose {
    @prop({ enum: YesNo })
    available: string;

    @prop()
    duration: TermAvailabilityDuration;
}

export class Terms extends Typegoose {
    @prop()
    public id: string;

    @prop()
    public externalId: string;

    @prop()
    public name: string;

    @prop({
        default: ''
    })
    public description: string;

    @prop()
    availability: TermAvailability;
}
