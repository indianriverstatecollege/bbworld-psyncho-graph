import { prop, Typegoose } from 'typegoose';

enum YesNo {
    Yes = 'Yes',
    No = 'No'
}

enum MembershipRoleId {
    Instructor = 'Instructor',
    BbFacilitator = 'BbFacilitator',
    TeachingAssistant = 'TeachingAssistant',
    CourseBuilder = 'CourseBuilder',
    Grader = 'Grader',
    Student = 'Student',
    Guest = 'Guest'
}

class BbMembershipsAvailability extends Typegoose {
    @prop()
    public idavailable: YesNo;
}

export class BbMemberships extends Typegoose {
    @prop()
    public userId: string;

    @prop()
    public courseId: string;

    @prop()
    public childCourseId: string;

    @prop()
    public dataSourceId: string;
    @prop()
    public availability: BbMembershipsAvailability;

    @prop()
    public courseRoleId: MembershipRoleId;
}
