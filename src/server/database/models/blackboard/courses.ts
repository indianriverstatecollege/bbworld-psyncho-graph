import { prop, Typegoose } from 'typegoose';
enum BbCourseAvailabilityAvailable {
    Yes = 'Yes',
    No = 'No',
    Disabled = 'Disabled',
    Term = 'Term'
}

enum BbCourseAvailabilityDurationType {
    Continuous = 'Continuous',
    DateRange = 'DateRange',
    FixedNumDays = 'FixedNumDays',
    Term = 'Term'
}

enum BbCourseUltraStatus {
    Undecided = 'Undecided',
    Classic = 'Classic',
    Ultra = 'Ultra',
    UltraPreview = 'UltraPreview'
}

enum BbCourseEnrollmentType {
    InstructorLed = 'InstructorLed',
    SelfEnrollment = 'SelfEnrollment',
    EmailEnrollment = 'EmailEnrollment'
}

class BbCourseAvailabilityDuration extends Typegoose {
    @prop()
    public type: BbCourseAvailabilityDurationType;

    @prop()
    public start: string;

    @prop()
    public end: string;

    @prop()
    public daysOfUse: Number;
}

class BbCourseAvailability extends Typegoose {
    @prop()
    public available: BbCourseAvailabilityAvailable;

    @prop()
    public duration: BbCourseAvailabilityDuration;
}

class BbCourseEnrollment extends Typegoose {
    @prop()
    public type: BbCourseEnrollmentType;

    @prop()
    public start: string;

    @prop()
    public end: string;

    @prop()
    public accessCode: string;
}

class BbCourseLocale extends Typegoose {
    @prop()
    public id: string;

    @prop()
    public force: boolean;
}

export class BbCourses extends Typegoose {
    @prop()
    public id: string;

    @prop()
    public uuid: string;

    @prop()
    public externalId: string;

    @prop()
    public dataSourceId: string;

    @prop()
    public courseId: string;

    @prop()
    public name: string;

    @prop()
    public description: string;

    @prop()
    public created: string;

    @prop()
    public ultraStatus: BbCourseUltraStatus;

    @prop()
    public allowGuests: boolean;

    @prop()
    public readOnly: boolean;

    @prop()
    public termId: string;

    @prop()
    public availability: BbCourseAvailability;

    @prop()
    public enrollment: BbCourseEnrollment;

    @prop()
    public locale: BbCourseLocale;

    @prop()
    public hasChildren: boolean;

    @prop()
    public parentId: string;

    @prop()
    public externalAccessUrl: string;

    @prop()
    public guestAccessUrl: string;
}
