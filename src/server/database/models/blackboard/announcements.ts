import { prop, Typegoose } from 'typegoose';
// tslint:disable-next-line:max-classes-per-file
class Duration extends Typegoose {
    @prop()
    public type: string;

    @prop()
    public start: string;

    @prop()
    public end: string;
}
// tslint:disable-next-line:max-classes-per-file
class Availability extends Typegoose {
    @prop()
    public duration: Duration;
}
// tslint:disable-next-line:max-classes-per-file
export class Announcements extends Typegoose {
    @prop()
    public id: string;

    @prop()
    public title: string;

    @prop()
    public body: string;

    @prop()
    public availability: Availability;

    @prop()
    public showAtLogin: boolean;

    @prop()
    public showInCourses: boolean;

    @prop()
    public created: string;
}
