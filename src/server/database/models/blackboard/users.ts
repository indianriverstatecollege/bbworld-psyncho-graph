import { prop, Typegoose } from 'typegoose';

enum YesNo {
    Yes = 'Yes',
    No = 'No'
}

enum EducationLevel {
    K8 = 'K8',
    HighSchool = 'HighSchool',
    Freshman = 'Freshman',
    Sophomore = 'Sophomore',
    Junior = 'Junior',
    Senior = 'Senior',
    GraduateSchool = 'GraduateSchool',
    PostGraduateSchool = 'PostGraduateSchool',
    Unknown = 'Unknown'
}

enum MaleFemale {
    Female = 'Female',
    Male = 'Male',
    Unknown = 'Unknown'
}

enum SysteRoles {
    SystemAdmin = 'SystemAdmin',
    SystemSupport = 'SystemSupport',
    CourseCreator = 'CourseCreator',
    CourseSupport = 'CourseSupport',
    AccountAdmin = 'AccountAdmin',
    Guest = 'Guest',
    User = 'User',
    Observer = 'Observer',
    Integration = 'Integration',
    Portal = 'Portal'
}

class UserContact extends Typegoose {
    @prop()
    public homePhone: string;
    @prop()
    public mobilePhone: string;
    @prop()
    public businessPhone: string;
    @prop()
    public businessFax: string;
    @prop()
    public email: string;
    @prop()
    public webPage: string;
}

class UserJob extends Typegoose {
    @prop()
    public title: string;
    @prop()
    public department: string;
    @prop()
    public company: string;
}

class UserAvailability extends Typegoose {
    @prop({ enum: YesNo })
    public available: string;
}

class UserAddress extends Typegoose {
    @prop()
    public street1: string;
    @prop()
    public street2: string;
    @prop()
    public city: string;
    @prop()
    public state: string;
    @prop()
    public zipCode: string;
    @prop()
    public country: string;
}

class UserLocale extends Typegoose {
    @prop()
    public id: string;
    @prop()
    public calendar: Calendar;
    @prop()
    public firstDayOfWeek: FirstDayOfTheWeek;
}

class UserName extends Typegoose {
    @prop()
    public given: string;
    @prop()
    public family: string;
    @prop()
    public middle: string;
    @prop()
    public other: string;
    @prop()
    public suffix: string;
    @prop()
    public title: string;
}

enum Calendar {
    Gregorian = 'Gregorian',
    GregorianHijri = 'GregorianHijri',
    Hijri = 'Hijri',
    HijriGregorian = 'HijriGregorian'
}

enum FirstDayOfTheWeek {
    Sunday = 'Sunday',
    Monday = 'Monday',
    Saturday = 'Saturday'
}

export class BbUsers extends Typegoose {
    @prop()
    public id: string;
    @prop()
    public uuid: string;
    @prop()
    public externalId: string;
    @prop()
    public dataSourceId: string;
    @prop()
    public userName: string;
    @prop()
    public studentId: string;
    @prop({ enum: EducationLevel })
    public educationLevel: string;
    @prop({ enum: MaleFemale })
    public gender: string;
    @prop()
    public birthDate: string;
    @prop()
    public created: string;
    @prop()
    public modified: string;
    @prop()
    public lastLogin: string;
    @prop()
    public institutionRoleIds: string[];
    @prop({ enum: SysteRoles })
    public systemRoleIds: SysteRoles[];
    @prop()
    public availability: UserAvailability;
    @prop()
    public name: UserName;
    @prop()
    public job: UserJob;
    @prop()
    public contact: UserContact;
    @prop()
    public address: UserAddress;
    @prop()
    public locale: UserLocale;
}
