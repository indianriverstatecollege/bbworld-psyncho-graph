import { prop, Typegoose } from 'typegoose';
import { Pagination } from './pagination';

// tslint:disable-next-line:max-classes-per-file
export class DataSources extends Typegoose {
    @prop()
    public id: string;

    @prop()
    public externalId: string;

    @prop({
        default: ''
    })
    public description: string;
}
