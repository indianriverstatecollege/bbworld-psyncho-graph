import { prop as Property, Typegoose } from 'typegoose';

export class Pagination extends Typegoose {
    @Property()
    public nextPage: string;
}
