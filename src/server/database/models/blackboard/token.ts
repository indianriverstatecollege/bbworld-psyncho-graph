import { index as Index, prop as Proptery, Typegoose } from 'typegoose';
@Index({ expires_in: 1 }, { expireAfterSeconds: 0 })
export class Token extends Typegoose {
    @Proptery()
    public access_token: string;

    @Proptery()
    public token_type: string;

    @Proptery()
    public expires_in: number;
}
