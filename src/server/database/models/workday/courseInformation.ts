import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

export class CourseInformation extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public courseTags: string;

    @Property()
    public prefix: string;

    @Property()
    public allowedCampuses: string;

    @Property()
    public workdayReferenceId: string;

    @Property()
    public description: string;

    @Property()
    public title: string;

    @Property()
    public academicUnitsType: string;

    @Property()
    public academicPeriods: string;

    @Property()
    public number: string;

    @Property()
    public academicLevel: string;

    @Property()
    public workdayReferenceType: string;

    @Property()
    public courseFormat: string;

    @Property()
    public courseId: string;

    @Property()
    public courseIdTitle: string;
}
