import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

class CacheMap extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public key: string;
    
    @Property()
    public map: string;

    @Property()
    public value: any;
}

export const CacheMapModel = new CacheMap().setModelForClass(
    CacheMap,
    {
        schemaOptions: {
            collection: `cache`,
            timestamps: true,
            toJSON: { virtuals: true },
            toObject: { virtuals: true }
        }
    }
);