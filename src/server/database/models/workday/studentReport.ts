import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

export class StudentReports extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public homePhone: string;

    @Property()
    public homeAddressLine1: string;

    @Property()
    public cohort: string;

    @Property()
    public homeState: string;

    @Property()
    public birthMonthYear: string;

    @Property()
    public homePostalCode: string;

    @Property()
    public studentId: string;

    @Property()
    public firstName: string;

    @Property()
    public homeEmail: string;

    @Property()
    public homeCity: string;

    @Property()
    public workdayID: string;

    @Property()
    public middleName: string;

    @Property()
    public institutionalEmail: string;
}
