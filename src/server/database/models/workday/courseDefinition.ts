import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

// tslint:disable-next-line: max-classes-per-file
class ReportEntry extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public endDate: string;

    @Property()
    public enrolledCapacity: string;

    @Property()
    public section: string;

    @Property()
    public campusMeetingPattern: string;

    @Property()
    public shortTitle: string;

    @Property()
    public title: string;

    @Property()
    public capacity: string;

    @Property()
    public instructors: string;

    @Property()
    public academicYear: string;

    @Property()
    public term: string;

    @Property()
    public externalTermId: string;

    @Property()
    public courseId: string;

    @Property()
    public externalId: string;

    @Property()
    public delivery: string;

    @Property()
    public meetingDays: string;

    @Property()
    public courseTags: string;

    @Property()
    public courseSectionDefinition: string;

    @Property()
    public campus: string;

    @Property()
    public contactHours: string;

    @Property()
    public maximumUnits: string;

    @Property()
    public academicLevel: string;

    @Property()
    public termDates: string;

    @Property()
    public courseSubject: string;

    @Property()
    public instructor: string;

    @Property()
    public publicNotes: string;

    @Property()
    public startDate: string;

    @Property()
    public status: string;
}

// tslint:disable-next-line: max-classes-per-file
export class CourseDefinition extends Typegoose {
    @Property()
    // tslint:disable-next-line:variable-name
    public Report_Entry: ReportEntry[];
}

// export the model as just the 'get' (defined) model
// export const StudentReportsModel = new StudentReports().getModelForClass(
//     StudentReports
// );
