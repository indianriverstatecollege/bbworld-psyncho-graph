import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

// tslint:disable-next-line: max-classes-per-file
class ReportEntry extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public availabilty: string;

    @Property()
    public endDate: string;

    @Property()
    public campus: string;

    @Property()
    public session: string;

    @Property()
    public externalId: string;

    @Property()
    public sectionId: string;

    @Property({
        default: ''
    })
    public courseId: string;

    @Property({
        default: ''
    })
    public termId: string;

    @Property()
    public externalTermId: string;

    @Property()
    public startDate: string;
}

// tslint:disable-next-line: max-classes-per-file
export class CourseSection extends Typegoose {
    // @Property()
    // tslint:disable-next-line:variable-name
    // public Report_Entry: ReportEntry[];
    @Field()
    readonly _id: ObjectId;

    @Property()
    public availabilty: string;

    @Property()
    public endDate: string;

    @Property()
    public campus: string;

    @Property()
    public session: string;

    @Property()
    public externalId: string;

    @Property()
    public sectionId: string;

    @Property({
        default: ''
    })
    public courseId: string;

    @Property({
        default: ''
    })
    public termId: string;

    @Property()
    public externalTermId: string;

    @Property()
    public startDate: string;
}

// export the model as just the 'get' (defined) model
// export const StudentReportsModel = new StudentReports().getModelForClass(
//     StudentReports
// );
