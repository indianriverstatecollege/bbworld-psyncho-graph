import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

// tslint:disable-next-line: max-classes-per-file
class ReportEntry extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public studentElectronicID: string;

    @Property()
    public country: string;

    @Property()
    public studentElectronicMessageType: string;

    @Property()
    public institutionLevel: string;

    @Property()
    public addressLn1: string;

    @Property()
    public city: string;

    @Property()
    public institutionName: string;

    @Property()
    public postalCode: string;

    @Property()
    public institutionType: string;

    @Property()
    public state: string;
}

// tslint:disable-next-line: max-classes-per-file
export class EducationalInstitutions extends Typegoose {
    @Property()
    // tslint:disable-next-line:variable-name
    public Report_Entry: ReportEntry[];
}
