import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

export class EmployeeReports extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public homePhone: string;

    @Property()
    public given: string;

    @Property()
    public jobTitle: string;

    @Property()
    public externalId: string;

    @Property()
    public jobFamily: string;

    @Property()
    public company: string;

    @Property()
    public userName: string;

    @Property()
    public family: string;

    @Property()
    public department: string;

    @Property()
    public email: string;
}
