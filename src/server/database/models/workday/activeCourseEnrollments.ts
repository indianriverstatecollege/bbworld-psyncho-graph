import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

// tslint:disable-next-line: max-classes-per-file
class Enrollment extends Typegoose {
    @Property()
    public studenId: string;

    @Property()
    public studentName: string;

    @Property()
    public status: string;
}

// tslint:disable-next-line: max-classes-per-file
class ReportEntry extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property({ required: true, index: true })
    public externalId: string;

    @Property()
    public primaryInstructorEmail: string;

    @Property({ required: true })
    public availability: string;

    @Property()
    public enrollments: Enrollment[];
}

// tslint:disable-next-line: max-classes-per-file
export class ActiveCourseEnrollments extends Typegoose {
    @Property()
    // tslint:disable-next-line:variable-name
    public Report_Entry: ReportEntry[];
}

// export the model as just the 'get' (defined) model
// export const ActiveCourseEnrollmentsModel = new ActiveCourseEnrollments().getModelForClass(
//     ActiveCourseEnrollments
// );
