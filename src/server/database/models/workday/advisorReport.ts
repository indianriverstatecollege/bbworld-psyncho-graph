import { prop as Property, Typegoose } from 'typegoose';
import { ObjectId } from 'mongodb';
import { Field } from 'type-graphql';

class RoleAssignmentWorkerAssigned extends Typegoose {
    @Property()
    public firstName: string;
    @Property()
    public lastName: string;
    @Property()
    public employeeId: string;
    @Property()
    public middleName: string;
    @Property()
    public userName: string;
    @Property()
    public primaryWorkEmail: string;
}

// tslint:disable-next-line: max-classes-per-file
class ReportEntry extends Typegoose {
    @Field()
    readonly _id: ObjectId;

    @Property()
    public studentCohortType: string;

    @Property()
    public student: string;

    @Property()
    public cohort: string;

    @Property()
    public studentCohort: string;

    @Property()
    public roleAssignmentsWorkersAssigned: RoleAssignmentWorkerAssigned[];

    @Property()
    public roleAssignments: string;

}

// tslint:disable-next-line: max-classes-per-file
export class AdivsorReports extends Typegoose {
    @Property()
    // tslint:disable-next-line:variable-name
    public Report_Entry: ReportEntry[];
}

// export the model as just the 'get' (defined) model
// export const StudentReportsModel = new StudentReports().getModelForClass(
//     StudentReports
// );
