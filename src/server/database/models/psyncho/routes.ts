import { prop as Property, Typegoose } from 'typegoose';

export class Route extends Typegoose {
    @Property({
        index: true,
        unique: true
    })
    public path: string;

    @Property()
    public params: any;

    @Property()
    public middleware: any[];

    @Property({
        default: false
    })
    public private: boolean;
}
