import { prop as Property, Typegoose } from 'typegoose';
import { ObjectID } from 'mongodb';

export class Transaction extends Typegoose {
    @Property()
    public document: ObjectID;

    @Property()
    public type: string;

    @Property()
    public collection: string;

    @Property()
    public changes: any;
}
