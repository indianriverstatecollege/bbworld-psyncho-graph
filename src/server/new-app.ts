import { BehaviorSubject, of } from 'rxjs';
import { join } from 'path';
import appFactory from './modules/appFactory';
import setupBbToken from './modules/setup/setupBbToken';
import setupDbStreams from './modules/setup/setupDbStreams';
import setupDynamicRoutes from './modules/setup/setupDynamicRoutes';
import { interval } from 'rxjs';
import { switchMap, tap, map } from 'rxjs/operators';
import { wdGetActiveCourses } from './modules/dataFromWd';
import { TERMS } from './configs';
const { expressApp, io } = appFactory({
    app: {
        sets: [{ key: 'x-powered-by', value: false }],
        enables: ['trust proxy'],
        locals: {
            router: new BehaviorSubject(null),
            routes: {},
            blackboard: {
                token: new BehaviorSubject({}),
                tokenTimer: new BehaviorSubject(0)
            }
        }
    },
    graphql: {
        endpoint: '/psynchoql',
        apis: [
            {
                url:
                    'https://developer.blackboard.com/portal/docs/apis/learn-swagger.json',
                prefix: 'Bb',
                path: join(
                    __dirname,
                    './graphql/schemas/blackboard/typeDefs.gql'
                )
            }
            // {
            //     url: 'http://gateway.marvel.com/docs/public',
            //     prefix: 'Marvel',
            //     path: join(__dirname, './graphql/schemas/marvel/typeDefs.gql')
            // }
            // {
            //     url: 'https://petstore.swagger.io/v2/swagger.json',
            //     prefix: 'PetStore',
            //     path: join(__dirname, './graphql/schemas/petstore/typeDefs.gql')
            // },
            // {
            //     url:
            //         'https://raw.githubusercontent.com/kubernetes/kubernetes/master/api/openapi-spec/swagger.json',
            //     prefix: 'Kubernetes',
            //     path: join(
            //         __dirname,
            //         './graphql/schemas/kubernetes/typeDefs.gql'
            //     )
            // }
        ],
        graphiql: true
    }
});

setupBbToken(expressApp);
setupDbStreams(expressApp, io);
setupDynamicRoutes(expressApp);

/**
 * Create main app event loop
 */
// interval(1000).subscribe((i: number) => {
//     io.emit('test', { test: i });
// });

// const createCourse$ = new BehaviorSubject(null);
// const updateCourse$ = new BehaviorSubject(null);
// const unsubcribeCreateCourses$ = new Subject();

// createCourse$.pipe(map((course: any) => {}));

interval(10000)
    .pipe(
        switchMap(() => of(wdGetActiveCourses(TERMS, true)).toPromise())
        // tap(console.log)
    )
    .subscribe(async (courses: any) => {
        const data = { courses: await courses };
        // console.log(data);
        io.emit('active:courses', data);
    });

io.on('active:courses', (socket: any, data: any) => {
    console.log(data);
});
