import {
    WdIntCourseSectionsConfig,
    WdIntCourseDefinitionConfig,
    WdIntCourseDefinitionTermConfig,
    WdIntEmployeeReportConfig,
    WdIntStudentReportConfig
} from '../configs/workday/workday.config';
import { Integration } from './classes/integration';

export async function wdGetActiveCourses(terms: any, refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    const wdGAIntegration = new Integration(WdIntCourseSectionsConfig);
    wdGAIntegration.index = 'externalId';
    wdGAIntegration.endpoint = wdGAIntegration.endpoint.replace(
        ':academicPeriods',
        terms.map((t: string) => `ACADEMIC_PERIOD_${t}`).join('!')
    );
    console.log('fetching data.....');
    if (refresh) {
        wdGAIntegration.refresh = refresh;
    }
    return wdGAIntegration.fetchData();
}

export async function wdGetCourseDefinitionByTerms(
    terms: any,
    refresh: boolean
) {
    // do a Promise.all and loop over payloads to generate types
    const wdGDIntegration = new Integration(WdIntCourseDefinitionTermConfig);
    wdGDIntegration.index = 'externalId';
    wdGDIntegration.endpoint = wdGDIntegration.endpoint.replace(
        ':academicPeriods',
        terms.map((t: string) => `ACADEMIC_PERIOD_${t}`).join('!')
    );
    console.log('fetching data.....');
    if (refresh) {
        wdGDIntegration.refresh = refresh;
    }
    return wdGDIntegration.fetchData();
}

export async function wdGetCourseDefinition(
    externalId: any,
    term: any,
    refresh: boolean
) {
    // do a Promise.all and loop over payloads to generate types
    const wdGDIntegration = new Integration(WdIntCourseDefinitionConfig);
    wdGDIntegration.index = 'externalId';
    wdGDIntegration.endpoint = wdGDIntegration.endpoint
        .replace(':externalId', externalId)
        .replace(':term', term);
    console.log('fetching data.....');
    if (refresh) {
        wdGDIntegration.refresh = refresh;
    }
    return wdGDIntegration.fetchData();
}

export async function wdGetCourseDefinitionByTerm(term: any, refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    const wdGDIntegration = new Integration(WdIntCourseDefinitionConfig);
    wdGDIntegration.index = 'externalId';
    wdGDIntegration.endpoint = wdGDIntegration.endpoint
        .replace('&externalId=:externalId', '')
        .replace(':term', term);
    console.log('fetching data.....');
    if (refresh) {
        wdGDIntegration.refresh = refresh;
    }
    return wdGDIntegration.fetchData();
}

export async function wdGetEmployee(externalId: any, refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    const wdGEIntegration = new Integration(WdIntEmployeeReportConfig);
    wdGEIntegration.index = 'externalId';
    wdGEIntegration.endpoint = wdGEIntegration.endpoint.replace(
        ':externalId',
        externalId
    );

    console.log('fetching data.....');
    if (refresh) {
        wdGEIntegration.refresh = refresh;
    }
    return wdGEIntegration.fetchData();
}

export async function wdGetEmployees(refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    const wdGEIntegration = new Integration(WdIntEmployeeReportConfig);
    wdGEIntegration.index = 'externalId';
    wdGEIntegration.endpoint = wdGEIntegration.endpoint.replace(
        '&externalId=:externalId',
        ''
    );

    console.log('fetching data.....');
    if (refresh) {
        wdGEIntegration.refresh = refresh;
    }
    return wdGEIntegration.fetchData();
}

export async function wdGetStudent(studentId: any, refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    const wdSIntegration = new Integration(WdIntStudentReportConfig);
    wdSIntegration.index = 'studentId';
    wdSIntegration.endpoint = wdSIntegration.endpoint
        .replace(':studentId', studentId)
        .replace('activeOnly=1', '')
        .replace('&academicPeriod=:term', '')
        .replace('?&', '?');

    console.log('fetching data.....');
    if (refresh) {
        wdSIntegration.refresh = refresh;
    }
    return wdSIntegration.fetchData();
}

export async function wdGetStudents(term: string, refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    term = term || 'ALL';
    const wdSIntegration = new Integration(WdIntStudentReportConfig);
    wdSIntegration.index = 'studentId';
    wdSIntegration.endpoint = wdSIntegration.endpoint.replace(
        '&studentId=:studentId',
        ''
    );

    if (term === 'ALL') {
        wdSIntegration.endpoint = wdSIntegration.endpoint.replace(
            '&academicPeriod=:term',
            ''
        );
    } else {
        wdSIntegration.endpoint = wdSIntegration.endpoint.replace(
            ':term',
            term
        );
    }

    console.log('fetching data.....');
    if (refresh) {
        wdSIntegration.refresh = refresh;
    }
    return wdSIntegration.fetchData();
}

export async function wdGetStudentsByTerm(term: string, refresh: boolean) {
    // do a Promise.all and loop over payloads to generate types
    const wdSIntegration = new Integration(WdIntStudentReportConfig);
    wdSIntegration.index = 'studentId';
    wdSIntegration.endpoint = wdSIntegration.endpoint
        .replace(':term', term)
        .replace('&activeOnly=1', '')
        .replace('&studentId=:studentId', '');

    console.log('fetching data.....');
    if (refresh) {
        wdSIntegration.refresh = refresh;
    }
    return wdSIntegration.fetchData();
}
