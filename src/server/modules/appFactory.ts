import * as express from 'express';
import setupGraphQL from './setup/setupGraphQL';
import setupAppLocals from './setup/setupAppLocals';
import setupAppConfiguration from './setup/setupAppConfiguration';
import setupAppMiddleware from './setup/setupAppMiddleware';
import setupIO from './setup/setupIO';
import { wdResolverMap } from '../graphql/schemas/workday/resolvers';
import {
    bbResolverMap,
    bbMutationMap,
    bbFieldResolvers
} from '../graphql/schemas/blackboard/resolvers';
import setupDbStreams from './setup/setupDbStreams';

export default (config: any) => {
    const expressApp = express();
    const { app, graphql } = config;
    const appConfig = app;
    setupAppConfiguration(appConfig, expressApp);
    setupAppLocals(appConfig.locals, expressApp);
    /**
     * Middleware setups socketio and returns the io service
     */
    const io = setupAppMiddleware(appConfig, expressApp);
    setupIO(io);
    setupDbStreams(expressApp, io);
    // if a resolverMap was provided; use it or load custom here
    graphql.rootValue =
        graphql.rootValue ||
        Object.assign(
            {},
            bbFieldResolvers(io),
            wdResolverMap(io),
            bbResolverMap(io),
            bbMutationMap(io)
        );
    // console.log('ROOT VALUE =======================================');
    // console.log(graphql.rootValue);
    // console.log('ROOT VALUE =======================================');
    setupGraphQL(graphql, expressApp, {
        db: expressApp.locals.db,
        io,
        ...graphql.context
    });

    return { expressApp, io };
};
