export default (config: any, app: any) => {
    for (const [key, value] of Object.entries(config)) {
        app.locals[key] = value;
    }
};
