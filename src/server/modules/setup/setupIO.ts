export default (io: any) => {
    io.on('connection', (socket: any) => {
        console.log(socket.id, ` connected...`);
        socket.on('message', (data: any) => {
            console.log(data);
            socket.emit('message', {
                msg: 'Hello from Psyncho',
                data: -1
            });
        });
    });
};
