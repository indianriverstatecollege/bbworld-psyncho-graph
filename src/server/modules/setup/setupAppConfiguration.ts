export default (config: any, app: any) => {
    const { sets, enables } = config;

    for (const appSet of sets) {
        app.set(appSet.key, appSet.value);
    }

    for (const feature of enables) {
        app.enable(feature);
    }
};
