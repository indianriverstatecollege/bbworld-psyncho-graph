import * as express from 'express';
import * as session from 'express-session';
import * as mongoose from 'mongoose';
import * as mongoStore from 'connect-mongo';
import { Promise as bluebird } from 'bluebird';
import { mongooseConfig } from '../../configs/constants';

export default (app: any, io: any) => {
    mongoose.connect(mongooseConfig.database, mongooseConfig.options);
    mongoStore(session);
    (mongoose as any).Promise = bluebird;
    app.locals.db = mongoose.connection;

    mongoose.connection.once('open', () => {
        const routesCollection = mongoose.connection.collection('routes');
        const routesStream = routesCollection.watch();

        routesStream.on('change', (change: any) => {
            const dataStream = JSON.parse(JSON.stringify(change));
            console.log(dataStream);
            io.emit('psyncho:route:updates', dataStream);
            const document = change.fullDocument;
            switch (change.operationType) {
                case 'insert':
                    console.log('Inserted a Route', document);
                    app.locals.router.next(null);
                    let router = express.Router();
                    // check to see if routes exist
                    if (app.locals.routes != {}) {
                        // rebuild routes
                        for (const id of Object.keys(app.locals.routes)) {
                            router.use(app.locals.routes[id].router);
                        }
                    }
                    router.get(
                        document.path,
                        document.middleware,
                        (req: any, res: any) => {
                            res.send(document);
                        }
                    );
                    app.locals.routes[document._id] = {
                        path: document.path,
                        router
                    };

                    console.log(app.locals);
                    app.locals.router.next(router);
                    break;
                case 'update':
                    console.log('Updated a Route', document);
                    break;
                case 'delete':
                    const docId = change.documentKey._id.toString();
                    console.log(
                        'Deleting a Route:',
                        app.locals.routes[docId].path
                    );
                    // console.log(Array.isArray(app._router.stack));
                    delete app.locals.routes[docId];
                    console.log('Deleted a Route:', app.locals.routes);
                    // rebuild routes
                    app.locals.router.next(null);
                    router = express.Router();
                    for (const id of Object.keys(app.locals.routes)) {
                        router.use(app.locals.routes[id].router);
                    }
                    console.log(app.locals);
                    app.locals.router.next(router);
                    break;
                case 'rename':
                    console.log('Renamed A Route', document);
                    break;
                case 'drop':
                    console.log('Removed all Routes!!');
                    break;
            }
        });

        const tokenCollection = mongoose.connection.collection('tokens');
        const tokenStream = tokenCollection.watch();

        tokenStream.on('change', (change: any) => {
            const dataStream = JSON.parse(JSON.stringify(change));
            console.log(dataStream);
            io.emit('bb:token:updates', dataStream);
            const document = change.fullDocument;
            switch (change.operationType) {
                case 'insert':
                case 'update':
                    app.locals.blackboard.token.next(document);
                    app.locals.blackboard.tokenTimer.next(document.expires);
                    break;
                case 'delete':
                    console.log('Token Expired and was removed');
                    break;
            }
        });

        const wdActiveCoursesCollection = mongoose.connection.collection(
            'wd_student_active_courses'
        );
        const wdActiveCoursesStream = wdActiveCoursesCollection.watch();
        wdActiveCoursesStream.on('change', (change: any) => {
            const dataStream = JSON.parse(JSON.stringify(change));
            // console.log(dataStream);
            io.emit('active:courses:updates', dataStream);
            const document = change.fullDocument;
            switch (change.operationType) {
                case 'insert':
                case 'update':
                case 'delete':
                    break;
            }
        });

        const wdCourseDefinitionCollection = mongoose.connection.collection(
            'wd_student_course_definitions'
        );
        const wdCourseDefinitionStream = wdActiveCoursesCollection.watch();
        wdCourseDefinitionStream.on('change', (change: any) => {
            const dataStream = JSON.parse(JSON.stringify(change));
            // console.log(dataStream);
            io.emit('active:courses:definitions', dataStream);
            const document = change.fullDocument;
            switch (change.operationType) {
                case 'insert':
                case 'update':
                case 'delete':
                    break;
            }
        });

        mongoose.connection.on('error', (err: any) => {
            console.log(err);
        });
    });
};
