import oas20ToAst from '../generators/oas20ToGraphQLTypes';
import getBasicAuthData from '../getBasicAuthData';
import mergeGqlTypes from '../generators/mergeGqlTypes';
import { buildSchema } from 'graphql';
import * as graphqlHTTP from 'express-graphql';

export default (config: any, app: any, context: any) => {
    oas20ToAst(config.apis)
        // tap into this then chain and process other data types
        .then(getBasicAuthData)
        .then(mergeGqlTypes)
        // .then(generateQueriesAndMutations)
        // .then(generateGraphQLTests)
        // .then(async ({ rootObj, typeDefs }: any) => {
        //     const schemas = await Promise.all(rootObj);
        //     const _types = schemas.map((_type: any) => _type.schema);
        //     console.log(_types);
        //     return { rootObj, typeDefs };
        // })
        .then(({ rootObj, typeDefs }: any) => {
            // console.log('typeDefs:', typeDefs);
            // console.log('schema:', config.schema);
            const schema =
                typeDefs || rootObj.length > 0
                    ? buildSchema(typeDefs)
                    : config.schema;
            // console.log(schema);
            const rootValue =
                rootObj.resolvers && rootObj.resolvers !== []
                    ? rootObj.resolvers
                    : config.rootValue;

            // console.log('rootValue:', rootValue);
            // console.log('context:', context);
            const graphiql = config.graphiql || true;
            app.use(
                config.endpoint,
                graphqlHTTP({
                    schema,
                    graphiql,
                    rootValue,
                    context
                })
            );
        })
        .catch(console.log);
};
