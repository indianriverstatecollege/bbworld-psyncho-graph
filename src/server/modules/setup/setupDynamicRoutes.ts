import * as co from 'co';
import * as express from 'express';
import { RouteModel } from '../../configs/constants';

export default (app: any) => {
    co(function*() {
        const cursor = RouteModel.find({}).cursor();
        let router = express.Router();
        for (
            let doc = yield cursor.next();
            doc != null;
            doc = yield cursor.next()
        ) {
            router.get(doc.path, doc.middleware, (req: any, res: any) => {
                res.send(doc);
            });
            app.locals.routes[doc._id] = {
                path: doc.path,
                router
            };
        }
        app.locals.router.next(router);
    });
};
