import { getBbToken, setToken } from '../../configs/constants';
import { Subject, interval } from 'rxjs';
import { map } from 'rxjs/operators';
import { takeUntil, withLatestFrom } from 'rxjs/operators';

export default (app: any) => {
    getBbToken().then((token: any) => setToken(app, token));

    const unsubscribe = new Subject();
    interval(1000)
        .pipe(
            takeUntil(unsubscribe),
            withLatestFrom(
                app.locals.blackboard.token,
                app.locals.blackboard.tokenTimer
            ),
            map(([i, token, tokenTimer]: any) => {
                // console.log(i, token, tokenTimer);
                if (tokenTimer <= 0) {
                    // token expired
                    getBbToken()
                        .then((token: any) => setToken(app, token))
                        .catch((err: any) => unsubscribe.next(err));
                } else {
                    app.locals.blackboard.tokenTimer.next(tokenTimer - 1);
                }
                return i;
            })
        )
        .subscribe(() => {});
};
