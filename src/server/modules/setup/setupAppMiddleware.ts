import * as bodyParser from 'body-parser';
import * as methodOverride from 'method-override';
import * as morgan from 'morgan';
import * as http from 'http';
import * as socketio from 'socket.io';

let router: any;
export default (config: any, app: any) => {
    const server = http.createServer(app);
    const io = socketio(server);
    app.use(morgan('combined'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(
        methodOverride((req: any) => {
            if (
                req.body &&
                typeof req.body === 'object' &&
                '_method' in req.body
            ) {
                // look in urlencoded POST bodies and delete it
                const method = req.body._method;
                delete req.body._method;
                return method;
            }
        })
    );
    app.use((req: any, res: any, next: any) => {
        // this needs to be a function to hook on whatever the current router is
        if (!router) {
            app.locals.router.subscribe((_router: any) => (router = _router));
        }

        router(req, res, next);
    });

    /* istanbul ignore if */
    if (process.env.NODE_ENV !== 'test') {
        server.listen(config.port, () => {
            console.log(`Server is running on http://localhost:${config.port}`);
        });
    }
    return io;
};
