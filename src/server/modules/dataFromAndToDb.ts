export async function getDbData(model: any, criteria: any, options: any) {
    options = options || { isMany: false };
    const select = options.select || '';
    if (!options.isMany) {
        return await model
            .findOne(criteria)
            .select(select)
            .lean()
            .exec();
    }
    return await model
        .find(criteria)
        .select(select)
        .lean()
        .exec();
}

export async function saveData(
    model: any,
    criteria: any,
    data: any,
    options: any
) {
    options = options || { isMany: false, upsert: true, new: true };
    if (!options.isMany) {
        let opts = JSON.parse(JSON.stringify(options));
        delete opts.isMany;
        return await model.findOneAndUpdate(criteria, data, opts).exec();
    } else {
        if (options.bulkWrite) {
            const bulkOpts = [];
            for (const item in data) {
                bulkOpts.push({
                    updateOne: {
                        filter: criteria,
                        update: data,
                        upsert: options.upsert
                    }
                });
            }
            return await model.collection
                .bulkWrite(bulkOpts)
                // .then(console.log)
                .catch(console.log);
        }

        const results = [];
        let opts = JSON.parse(JSON.stringify(options));
        delete opts.isMany;
        for (let item in data) {
            results.push(
                await model.findOneAndUpdate(criteria, item, opts).exec()
            );
        }
        return await results;
    }
}
