import { Moment } from 'moment';
import { Mapper } from '../classes/mapper';
import { IParam } from './integration.types';

/**
 * Describes the state of the runner
 * @export
 * @readonly
 * @enum {number}
 */
export enum enumRunnerMode {
    started,
    running,
    errored,
    completed,
    stopped,
    paused,
    ready,
}

/**
 * Describes how often the runner should perform.
 *
 * @export
 * @interface IOccurrance
 */
export interface IOccurrance {
    /**
     * Sets the Runner to run on an hourly basis.
     *
     * @type {Moment}
     * @memberof IOccurrance
     */
    hourly?: Moment;
    /**
     * Sets the Runner to run on an daily basis.
     *
     * @type {Moment}
     * @memberof IOccurrance
     */
    daily?: Moment;
    /**
     * Sets the Runner to run on an weekly basis.
     *
     * @type {Moment}
     * @memberof IOccurrance
     */
    weekly?: Moment;
    /**
     * Sets the Runner to run on an monthly basis.
     *
     * @type {Moment}
     * @memberof IOccurrance
     */
    monthly?: Moment;
    /**
     * Sets the Runner to run on an yearly basis.
     *
     * @type {Moment}
     * @memberof IOccurrance
     */
    yearly?: Moment;
}

/**
 * Describes the Runner Class configuration
 *
 * @export
 * @interface IRunner
 */
export interface IRunner {
    /**
     * The actual Psyncho Mapper Object
     *
     * @type {Mapper}
     * @memberof IRunner
     */
    mapper: Mapper;
    /**
     * Determines whether this runner is to be repeated
     * or a single run instance.
     *
     * @type {boolean}
     * @memberof IRunner
     */
    repeatable: boolean;
    /**
     * How often this runner will be performed
     * if repeatable is set to true.
     *
     * @type {IOccurrance}
     * @memberof IRunner
     */
    occurrances?: IOccurrance;
    /**
     * When to perform the runner "task"
     *
     * @type {Moment}
     * @memberof IRunner
     */
    when: Moment;
    /**
     * Holds a list any key:value pair objects that
     * will be passed to the integration / connection.
     *
     * @type {IParam[]}
     * @memberof IRunner
     */
    parameters?: IParam[];

    /**
     * Determines whether this runner is self repeatable
     * / stackable. Allows for further processing.
     *
     * @type {boolean}
     * @memberof IRunner
     */
    iterable?: boolean;
    /**
     * Describes the state of the runner.
     *
     * @type {enumMode}
     * @memberof IRunner
     */
    mode: enumRunnerMode;
}
