import { Connector } from '../classes/connector';

/**
 * Enumeration Mode descibes the mode of the Integration.
 * @export
 * @enum {number}
 */
export enum enumMode {
    draft,
    test,
    active,
    disabled,
    deprecated
}

/**
 * Parameter Interface allows for custom parameters to be passed for the endpoint.
 * @export
 * @interface IParam
 */
export interface IParam {
    [key: string]: any;
}

export interface ITransformObject {
    args: string[];
    fn: Function;
}
export interface IInjectionObject {
    [key: string]: string | ITransformObject;
}
/**
 * Main Integration Interface that describes integration object.
 * @export
 * @interface IIntegration
 */
export interface IIntegration {
    /**
     * A connector that the integrations will use.
     *
     * @type {Connector[]}
     * @memberof IIntegration
     */
    connector: Connector;
    /**
     * The data payload tha will be stored comming from the connected system.
     *
     * @type {*}
     * @memberof IIntegration
     */
    data?: any;
    /**
     * The index to use if provided that will applied to the model if provided
     *
     * @type {*}
     * @memberof IIntegration
     */
    index?: string;

    /**
     * The path endpoint that will be used in retrieving the data.
     * For http url calls, you can add params to be replaced with values.
     *
     * Example:
     *  /path/:studentId/info
     *
     * The :studentId will be replaced if any value that was given in params.
     * @type {string}
     * @memberof IIntegration
     */
    endpoint: string;
    /**
     * Any error handlers that the integration will use in the event of an errors.
     *
     * @type {*}
     * @memberof IIntegration
     */
    errorHandlers: any;
    /**
     * The mode that will be used to help determine the status of the integration.
     *
     * @type {enumMode}
     * @memberof IIntegration
     */
    mode: enumMode;
    /**
     * A list of models that will be used to help store and validate the data payloads.
     *
     * @type {*}
     * @memberof IIntegration
     */
    model: any;
    /**
     * An object containig the key and or sub object to obtain the next page
     *
     * @type {*}
     * @memberof IIntegration
     */
    modelPagination?: any;
    /**
     * An object of key:value pairs to be injected into the model upon saving with defaults.
     *
     * @type {*}
     * @memberof IIntegration
     */
    modelInjectionFields?: IInjectionObject;

    /**
     * A list of model include fields that will be injected into the retreiving or comparing process.
     *
     * @type {*}
     * @memberof IIntegration
     */
    modelIncludeFields?: any;
    /**
     * A list of model exclude fields that will be removed from the retreiving or comparing process.
     *
     * @type {*}
     * @memberof IIntegration
     */
    modelExcludeFields?: any;

    /**
     * A string that represents the many payload response incase that the returned payload
     * does not match: single (object) vs. many (array of objects)
     *
     * @type {string}
     * @memberof IIntegration
     */
    modelManyKey: string;
    /**
     * The name of the collection that will be used for the payloads from the integration.
     *
     * @type {string}
     * @memberof IIntegration
     */
    collectionName: string;
    /**
     * A key:value pair object that will be used in retreiving data from the endpoint:
     * url param and or data payload in post, put, patch, etc.
     *
     * Example:
     *  {
     *     studentId: 'X12345678'
     *  }
     *
     * This will replace any :studentId in the endpoint url
     * @type {IParam}
     * @memberof IIntegration
     */
    params: IParam;
    /**
     * Determines the default http method to call for connections
     *
     * @type {any}
     * @memberof IIntegration
     */
    defaultHttpOptions?: any;
    /**
     * If available then adds the http methods for the crud call
     *
     * @type {any}
     * @memberof IIntegration
     */
    crudOps?: any;
}
