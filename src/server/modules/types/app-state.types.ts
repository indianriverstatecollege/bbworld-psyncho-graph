import { IServerConfig } from './server.types';

export interface IServices {
    [name: string]: any;
}

export interface IAppState {
    serverConfig: IServerConfig;
    services: IServices;
    test: string;
}