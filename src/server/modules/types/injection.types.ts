export const INJECTION_TYPES = {
    Server: 'Server',
    App: 'App',
    ServerConfig: 'ServerConfig',
    IntegrationConfig: 'IntegrationConfig',
    ConnectionConfig: 'ConnectionConfig'
};
