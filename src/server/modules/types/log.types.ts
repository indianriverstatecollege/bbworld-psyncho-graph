import { LoggerOptions } from 'winston';
import { MailTransportOptions } from 'winston-mail';

/**
 * Log interface for the Log class
 *
 * @export
 * @interface ILog
 */
export interface ILog {
    /**
     * The actual log configuration oprions
     *
     * @type {LoggerOptions}
     * @memberof ILog
     */
    config: LoggerOptions;
    /**
     * Optional Email options
     *
     * @type {MailTransportOptions}
     * @memberof ILog
     */
    emailConfig?: MailTransportOptions | any;
}
