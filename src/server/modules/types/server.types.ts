import { LoggerOptions } from 'winston';
import { Route } from '../classes/route';

/**
 * Server Configuration Interface
 *
 * @export
 * @interface IServerConfig
 */
export interface IServerConfig {
    /**
     * Middlewares to be applied to the main application.
     *
     * @type {any[]}
     * @memberof IServerConfig
     */
    middlewares?: any[];
    /**
     * Routes that will be applied for the application to listen for.
     *
     * @type {any[]}
     * @memberof IServerConfig
     */
    routers?: Route[];
    /**
     * Application configuration settings.
     *
     * @type {any[]}
     * @memberof IServerConfig
     */
    sets?: any[];
    /**
     * Turns on some application features fore express.
     *
     * @type {any[]}
     * @memberof IServerConfig
     */
    enables?: any[];
    /**
     * Adds app global variables with their default values to the express app.
     *
     * @type {any[]}
     * @memberof IServerConfig
     */
    locals?: any[];
    /**
     * The port that application will be listening on.
     *
     * @type {number}
     * @memberof IServerConfig
     */
    port?: number;
    /**
     * Static paths that application will serve out as public assets.
     *
     * @type {any[]}
     * @memberof IServerConfig
     */
    staticPaths: any[];
    /**
     * The logger configuration that application will use to log out errors.
     *
     * @type {LoggerOptions}
     * @memberof IServerConfig
     */
    loggerOptions?: LoggerOptions;
    /**
     * The graphQLHttpOptions for the GraphQL Server Middleware
     *
     * @type {*}
     * @memberof IServerConfig
     */
    graphQLHttpOptions: any;
}

/**
 * Describes the available methods to be used.
 * Although all these are optional, at least one is required
 * in order for the handlers to actually work.
 *
 * @export
 * @interface IHttpRequestMethods
 */
export interface IHttpRequestMethods {
    /**
     * Http Connect Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    connect?: any;
    /**
     * Http Delete Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    delete?: any;
    /**
     * Http Get Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    get?: any;
    /**
     * Http Head Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    head?: any;
    /**
     * Http Options Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    options?: any;
    /**
     * Http Patch Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    patch?: any;
    /**
     * Http Post Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    post?: any;
    /**
     * Http Put Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    put?: any;
    /**
     * Http Trace Request Method
     *
     * @type {*}
     * @memberof IHttpRequestMethods
     */
    trace?: any;
}

/**
 * Route interface for defining route.
 *
 * @export
 * @interface IRoute
 */
export interface IRoute {
    /**
     * The path that application router will mount to
     * example: /path
     * Always start the path with a forward slash "/"
     *
     * @type {string}
     * @memberof IRoute
     */
    path: string;
    /**
     * The http request methods as an object:
     * {
     *   get: (req: any , res: any) { ... },
     *   post: (req: any , res: any) { ... },
     *   put: (req: any , res: any) { ... },
     *   patch: (req: any , res: any) { ... },
     *   delete: (req: any , res: any) { ... },
     * }
     *
     * @type {*}
     * @memberof IRoute
     */
    handlers: IHttpRequestMethods;
    /**
     * Any child routes to mount to a particular mount path.
     *
     * @type {IRoute}
     * @memberof IRoute
     */
    childRoutes?: IRoute[];
    /**
     * Any Middlewares to apply to the mount path.
     *
     * @type {*}
     * @memberof IRoute
     */
    middlewares?: any;
    /**
     * Any extra options to be applied for the particular mount path.
     *
     * @type {*}
     * @memberof IRoute
     */
    options?: any;
}
