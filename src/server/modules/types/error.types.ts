import { PsynchoEmitter } from '../services/emitter.service';

/**
 * Error Action state.
 *
 * @export
 * @enum {number}
 */
export enum enumErrorAction {
    console,
    systemLog,
    email,
}

/**
 * Error Level state.
 *
 * @export
 * @enum {number}
 */
export enum enumErrorLevel {
    debug,
    info,
    warning,
    severe,
}

/**
 * Mapping to return the enumErrorLevel state to a string relation.
 * @constant errorLevelMapping
 */
export const errorLevelMapping: any = {
    0: 'debug',
    1: 'info',
    2: 'warning',
    3: 'severe',
};

/**
 * Error interface for the Error Handler Class
 *
 * @export
 * @interface IErrorHandler
 */
export interface IErrorHandler {

    /**
     * The friendle name for this Error Handler.
     *
     * @type {string}
     * @memberof IErrorHandler
     */
    name: string;

    /**
     * Describes the Error Handler and its purpose.
     *
     * @type {string}
     * @memberof IErrorHandler
     */
    description: string;

    /**
     * The incoming error object that will be thrown.
     *
     * @type {*}
     * @memberof IErrorHandler
     */
    error: any;
    /**
     * The incoming caller instance that trigger the error
     *
     * @type {IErrorService}
     * @memberof IErrorHandler
     */
    service: IErrorService;
    /**
     * The actual error handler function that will be called under the hood.
     *
     * @type {*}
     * @memberof IErrorHandler
     */
    handler: any;

    /**
     * The action type that the error handler is categorized and reacts to.
     *
     * @type {enumErrorAction}
     * @memberof IErrorHandler
     */
    action: enumErrorAction;

    /**
     * The path to where the error output should be.
     *
     * @type {string}
     * @memberof IErrorHandler
     */
    logPath?: string;

    /**
     * The level state of severity of the error.
     *
     * @type {enumErrorLevel}
     * @memberof IErrorHandler
     */
    level: enumErrorLevel;
    /**
     * A custom event emitter that will be used to isolate the error.
     *
     * @type {PsynchoEmitter}
     * @memberof IErrorService
     */
    emitter?: PsynchoEmitter;
}

/**
 * Error Service is used to identify the caller instance
 *
 * @export
 * @interface IErrorService
 */
export interface IErrorService {
    /**
     * Name is the actual service name
     *
     * @type {string}
     * @memberof IErrorService
     */
    name: string;
    /**
     * Caller is the actual name of the caller that trigger the error.
     *
     * @type {string}
     * @memberof IErrorService
     */
    caller: string;
}
