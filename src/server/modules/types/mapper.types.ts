import { Integration } from '../classes/integration';
import { ErrorHandler } from '../services/error.service';
import { Transformer } from '../classes/transformer';
import { enumMode, IParam } from './integration.types';

/**
 * IMapper describes the configuration for a Psyncho Mapper Object
 *
 * @export
 * @interface IMapper
 */
export interface IMapper {
    /**
     * A list of integrations that will run in sequence.
     *
     * @type {IIntegration[]}
     * @memberof IMapper
     */
    integrations: Integration[];

    /**
     * A list of transformations that will be applied to the incoming data properties
     *
     * @type {Transformer[]}
     * @memberof IMapper
     */
    transformations?: Transformer[];
    /**
     * A list of error handlers that will run in sequence if any errors occur.
     *
     * @type {IErrorHandler[]}
     * @memberof IMapper
     */
    errorHandlers: ErrorHandler[];
    /**
     * A Mode enumeration to describe the state of the mapper.
     *
     * @type {enumMode}
     * @memberof IMapper
     */
    mode: enumMode;
    /**
     * A unique prefix that will used to prefix data from
     * incoming/outgoing connections for the database collections.
     *
     * @type {string}
     * @memberof IMapper
     */
    prefix: string;
    /**
     *  A list of key:value pair objects that will contain parameter
     *  infomration for the integration/connections.
     *
     * @type {IParam[]}
     * @memberof IMapper
     */
    parameters: IParam[];
    /**
     * Determines if the current mapper is required or is 'skip-able'
     * during runner tasks. Essentially: cancel stacks mappers or halt.
     *
     * @type {boolean}
     * @memberof IMapper
     */
    required: boolean;
}
