
/**
 * Describes the Transformer context
 *
 * @export
 * @interface ITransformer
 */
export interface ITransformer {
    /**
     * The incoming data to be transformed
     *
     * @type {*}
     * @memberof ITransformer
     */
    data: any;
    /**
     * The code as a string that will be transpiled on the fly against the data.
     *
     * @type {string}
     * @memberof ITransformer
     */
    code: string;
    /**
     * Any parameters that will do an override on the data
     *
     * @type {*}
     * @memberof ITransformer
     */
    params: any;
    /**
     * A VM security context to pass in Node/Third Party Modules to be used on the data.
     *
     * @type {*}
     * @memberof ITransformer
     */
    context: any;
}
