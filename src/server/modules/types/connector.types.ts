/**
 * Credential Interface that hold the configuration for connections types
 *
 * @export
 * @interface ICredentials
 */
export interface ICredentials {
    /**
     * A key token to be use in connections that are looking for a key string.
     *
     * @type {string}
     * @memberof ICredentials
     */
    key?: string;
    /**
     * The path to a key file that holds the idientity for ssh connections.
     *
     * @type {string}
     * @memberof ICredentials
     */
    keyPath?: string;
    /**
     * The password that will be used in connection.
     *
     * @type {string}
     * @memberof ICredentials
     */
    password?: string;
    /**
     * Tehs secret to used when a connection is looking for key:secret credentials.
     *
     * @type {string}
     * @memberof ICredentials
     */
    secret?: string;
    /**
     * A token string to be used when dealing with connections such as OAuth.
     *
     * @type {string}
     * @memberof ICredentials
     */
    token?: string;
    /**
     * The key to retrieve the access/refesh token from the oauth request
     *
     * @type {string}
     * @memberof IConnector
     */
    tokenKey?: string;
    /**
     * The username that will be use in the connection.
     *
     * @type {string}
     * @memberof ICredentials
     */
    username?: string;
    /**
     * When using oauth, grantType is the oauth credentials use during transit.
     *
     * @type {string}
     * @memberof ICredentials
     */
    grantType?: string;
    /**
     * When using oauth, auth is the credentials use during transit.
     *
     * @type {string}
     * @memberof ICredentials
     */
    auth?: string;
}

/**
 * Connection Enumeration types for clarity
 *
 * @export
 * @interface enumType
 */
export enum enumType {
    httpx,
    sftp,
    ssh,
    oauth,
}

/**
 * Connector Main Interface that holds the complete connection object
 *
 * @export
 * @interface IConnector
 */
export interface IConnector {
    /**
     * Credentials is an optional object that stores the credential details.
     *
     * @type {ICredentials}
     * @memberof IConnector
     */
    credentials?: ICredentials;
    /**
     * Holds the description of the connector.
     *
     * @type {string}
     * @memberof IConnector
     */
    description: string;
    /**
     * The friendly name for the connector.
     *
     * @type {string}
     * @memberof IConnector
     */
    name: string;
    /**
     * Notes is an optional string to hold any extra details for the connector.
     *
     * @type {string}
     * @memberof IConnector
     */
    notes?: string;
    /**
     * The path the connector will use to connect to a system.
     *
     * @type {string}
     * @memberof IConnector
     */
    path: string;
    /**
     * The path to request for a access token
     *
     * @type {string}
     * @memberof IConnector
     */
    oauthPath?: string;
    /**
     * The prefix that will be used to keep all storeage payload unique to a particular system.
     *
     * @type {string}
     * @memberof IConnector
     */
    prefix: string;
    /**
     * The type of connection the connector will use to connect.
     *
     * @type {enumType}
     * @memberof IConnector
     */
    type: enumType;
}
