"use strict";
exports.__esModule = true;
var cache_map_1 = require("../../core/classes/cache-map");
var cacheMapFactory = function (name) {
    // tslint:disable-next-line
    return new(require('safe-eval')("(class " + name + " extends CacheMap {})", {
        CacheMap: cache_map_1.CacheMap
    }))();
};
var map = cacheMapFactory('MyCacheMap');
map.get('HiThere');