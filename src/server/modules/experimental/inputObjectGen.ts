import * as gql from 'graphql';
function genFields(fields: any) {
    let _fields = {};
    for (const key of Object.keys(fields)) {
        if (fields.hasOwnProperty(key)) {
            if (typeof fields[key] === 'string') {
                console.log(`key: ${key}, value: ${fields[key]}`);
                _fields[key] = { type: gql[fields[key]] };
                continue;
            } else {
                //is an object
                const field = fields[key];
                console.log(field);
                // if (field.custom) {
                //     _fields[key] = { type: new gql[field.type](gql[field.value]) };
                //     continue;
                // }
                _fields[key] = { type: new gql[field.type](gql[field.value]) };
            }
        }
    }
    return () => _fields;
}

export const inputObjectFactory = (config: any) => {
    return new gql.GraphQLInputObjectType({
        name: config.name,
        description: config.description,
        fields: genFields(config.fields)
    });
};

const iot = inputObjectFactory({
    name: 'MyTestInputObject',
    description: 'This is just test.',
    fields: {
        string: 'GraphQLString',
        boolean: 'GraphQLBoolean',
        int: 'GraphQLInt',
        float: 'GraphQLFloat',
        list: {
            type: 'GraphQLList',
            value: 'GraphQLString'
        }
    }
});
console.log(iot.getFields());

// function recursion(obj: any) {
//     for (const item in obj)
// }
