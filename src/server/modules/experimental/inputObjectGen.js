"use strict";
exports.__esModule = true;
var gql = require("graphql");
function genFields(fields) {
    var _fields = {};
    for (var _i = 0, _a = Object.keys(fields); _i < _a.length; _i++) {
        var key = _a[_i];
        if (fields.hasOwnProperty(key)) {
            if (typeof fields[key] === 'string') {
                console.log("key: " + key + ", value: " + fields[key]);
                _fields[key] = { type: gql[fields[key]] };
                continue;
            }
            else {
                //is an object
                var field = fields[key];
                console.log(field);
                // if (field.custom) {
                //     _fields[key] = { type: new gql[field.type](gql[field.value]) };
                //     continue;
                // }
                _fields[key] = { type: new gql[field.type](gql[field.value]) };
            }
        }
    }
    return function () { return _fields; };
}
exports.inputObjectFactory = function (config) {
    return new gql.GraphQLInputObjectType({
        name: config.name,
        description: config.description,
        fields: genFields(config.fields)
    });
};
var iot = exports.inputObjectFactory({
    name: 'MyTestInputObject',
    description: 'This is just test.',
    fields: {
        string: 'GraphQLString',
        boolean: 'GraphQLBoolean',
        int: 'GraphQLInt',
        float: 'GraphQLFloat',
        list: {
            type: 'GraphQLList',
            value: 'GraphQLString'
        }
    }
});
console.log(iot.getFields());
