import { CacheMap } from '../classes/cache-map';

const cacheMapFactory = (name: string) =>
    new (require('safe-eval')(`(class ${name} extends CacheMap {})`, {
        CacheMap
    }))();

const map = cacheMapFactory('MyCacheMap');
map.get('HiThere');
