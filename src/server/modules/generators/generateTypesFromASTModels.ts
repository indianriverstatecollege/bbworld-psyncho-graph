import { writeFileSync } from 'fs';
import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import GraphQLJSON from 'graphql-type-json';

export default async (astModel: any) => {
    const ast = await astModel.ast;
    const config = astModel.config;
    const rootObj = processDefinitions(ast, config);
    // console.log(Object.values(rootObj.typeDefs)[0]);
    writeOutTypeDef(config.path, rootObj);
    return { schema: rootObj, config };
};

function writeOutTypeDef(path: any, rootObj: any) {
    let typeDefs = Object.values(rootObj.scalars).join('\n');
    typeDefs += `\ntype Query {${Object.values(rootObj.typeDefs)
        .map((typeDef: any) => `\n  ${typeDef.name}: ${typeDef.name}`)
        .join('')}\n}`;
    typeDefs += Object.values(rootObj.typeDefs)
        .map((typeDef: any) => typeDef.definition)
        .join('')
        .replace(/""""/gim, '"""');
    writeFileSync(path, typeDefs);
}

function processDefinitions(ast: any, config: any) {
    // console.log(ast);
    let rootObj: any = {};
    if (!rootObj.typeDefs) {
        rootObj.typeDefs = {};
    }

    if (!rootObj.scalars) {
        rootObj.scalars = {};
    }
    if (!rootObj.scalars.JSON) {
        rootObj.scalars.JSON = 'scalar JSON';
    }

    if (!rootObj.resolvers) {
        rootObj.resolvers = {};
    }
    if (!rootObj.resolvers.JSON) {
        rootObj.resolvers.JSON = GraphQLJSON;
    }
    // iterate over all asts
    // clear file if already exists and or create first
    // writeFileSync(join(__dirname, '.test.json'), '');

    ast.types.forEach((definition: any) =>
        createDef(definition, config.prefix, rootObj)
    );
    return rootObj;
}
function isTypeDef(definition: any) {
    return definition.internalType && definition.internalType === 'object';
}
function createDef(definition: any, prefix: any, rootObj: any) {
    const name = `${prefix}${definition.title ||
        (definition.nameTitle || definition.name)}`.replace(/\.|-|\$/gi, '_');
    // console.log(definition);
    let description =
        definition.description || `No description for ${name} provided.`;
    description = description.replace(/`/gim, "'");
    const propsRequired = definition.propsRequired || [];
    const type = definition.type || definition.internalType;
    let nameTitle =
        definition.name.charAt(0).toUpperCase() + definition.name.slice(1);
    if (isTypeDef(definition)) {
        // if has title, then type def
        if (!rootObj.typeDefs[name.toLowerCase()]) {
            rootObj.typeDefs[name.toLowerCase()] = {
                name,
                type,
                prefix,
                nameTitle,
                description,
                propsRequired,
                definition: ''
            };
        }
        // console.log(name, description, prefix, rootObj);
        if (definition.properties) {
            processProperties(
                name,
                definition.properties || [
                    {
                        type,
                        format: 'JSON'
                    }
                ],
                rootObj
            );
        } else {
        }
    } else {
        // these are either enums, or some other 'type'
        if (definition._enum) {
            rootObj.typeDefs[name.toLowerCase()] = {
                name,
                type,
                description,
                prefix,
                nameTitle,
                propsRequired,
                definition: `\nenum ${definition.overrideType ||
                    name} {${definition._enum.map(
                    (item: string) => `\n  ${item}`
                )}\n}\n`
            };
        }
    }
}

function processProperties(name: any, properties: any, rootObj: any) {
    const astType = rootObj.typeDefs[name.toLowerCase()];
    astType.definition = `\n"""${astType.description ||
        `No description for ${name} provided.`}"""\ntype ${name} {\n${properties
        .map((prop: any) => {
            // console.log(prop);

            if (isTypeDef(prop) && prop.properties) {
                createDef(prop, name, rootObj);
            }
            let description =
                prop.description || `No description provided for ${name}.`;
            description = description.replace(/`/gim, "'");
            const example = prop.example ? '\n' + prop.example : '';
            const readOnly = prop.readyOnly || false;
            const isScalar = !!(
                !prop.internalType &&
                (prop.type && prop.format)
            );
            // TODO: Detect if a required field...
            if (isScalar) {
                // add the scalar to the rootObj.scalars
                // scalars are single types so they are most like the same defintion
                if (!rootObj.scalars[prop.format]) {
                    rootObj.scalars[prop.format] = `scalar ${prop.format}`;
                    return `\n  """${description}${example}"""\n  ${prop.name.replace(
                        /\.|-|\$/gi,
                        '_'
                    )}: ${prop.format}`;
                }

                // add the resolverMap for the scalar to rootObj.resolvers[scalarName]
                if (!rootObj.resolvers[prop.format]) {
                    rootObj.resolvers[prop.format] = new GraphQLScalarType({
                        name: prop.format,
                        description,
                        parseValue(value) {
                            return convertType(prop.type);
                        },
                        serialize(value) {
                            return value;
                        },
                        parseLiteral(ast) {
                            if (ast.kind === Kind.STRING) {
                                return ast.value;
                            }
                            return null;
                        }
                    });
                }
            } else {
                // not a scalar, primitive types
                prop.nameTitle =
                    prop.name.charAt(0).toUpperCase() + prop.name.slice(1);
                prop.nameTitle = prop.nameTitle.replace(/\.|-|\$/gi, '_');
                if (prop.internalType === 'array') {
                    return `\n  """${description}${example}"""\n  ${prop.name.replace(
                        /\.|-|\$/gi,
                        '_'
                    )}: [${prop.items.internalType ? '' : astType.prefix}${
                        prop.items.internalType
                            ? convertType(prop.items.internalType)
                            : prop.items.reference
                    }]`.replace(/\.|-|\$/gi, '_');
                } else if (prop._enum) {
                    // TODO: hi jack the return the enum type!!
                    // check if enum if exists, if not creates
                    prop.overrideType = `${astType.name}${
                        prop.nameTitle
                    }`.replace(/\.|-|\$/gi, '_');
                    // console.log(prop.overrideType);
                    createDef(prop, astType.name, rootObj);

                    return `\n  """${description}${example}"""\n  ${prop.name.replace(
                        /\.|-|\$/gi,
                        '_'
                    )}: ${prop.overrideType}`.replace(/\.|-|\$/gi, '_');
                } else {
                    if (prop.internalType === 'object') {
                        if (prop.additionalProperties) {
                            return `\n  """${description}${example}"""\n  ${prop.name.replace(
                                /\.|-|\$/gi,
                                '_'
                            )}: JSON`.replace(/\.|-|\$/gi, '_');
                        } else {
                            return `\n  """${description}${example}"""\n  ${prop.name.replace(
                                /\.|-|\$/gi,
                                '_'
                            )}: ${astType.name}${prop.title ||
                                prop.nameTitle}`.replace(/\.|-|\$/gi, '_');
                        }
                    } else {
                        return `\n  """${description}${example}"""\n  ${prop.name.replace(
                            /\.|-|\$/gi,
                            '_'
                        )}: ${convertType(prop.internalType)}`.replace(
                            /\.|-/gi,
                            '_'
                        );
                    }
                }
            }
        })
        .join('')}\n}\n`;
}

function convertType(type: string): string {
    // see: http://docs.swagger.io/spec.html#431-primitives
    // see: http://graphql.org/learn/schema/#scalar-types
    switch (type) {
        case 'integer':
        case 'int32':
            return 'Int'; // 32-bit
        case 'number':
        case 'float':
        case 'double':
            return 'Float'; // double precision
        case 'string':
            return 'String';
        case 'boolean':
            return 'Boolean';
        default:
            return 'String';
    }
}
