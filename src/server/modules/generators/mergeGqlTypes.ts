import { mergeTypes, fileLoader } from 'merge-graphql-schemas';
import { join } from 'path';
import { writeFileSync } from 'fs';

export default (rootObj: any) => {
    let typeDefs = mergeTypes(
        fileLoader(
            join(
                __dirname,
                '../',
                '../',
                'graphql',
                'schemas',
                '**',
                'types',
                '**',
                '*.gql'
            )
        ),
        {
            all: false
        }
    );
    writeFileSync(
        join(__dirname, '../', '..', 'graphql', 'schema.gql'),
        typeDefs
    );
    return { rootObj, typeDefs };
};
