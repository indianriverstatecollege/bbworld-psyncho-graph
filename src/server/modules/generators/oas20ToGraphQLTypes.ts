import * as request from 'request-promise';
import * as converter from 'oas-raml-converter';
import generateTypesFromASTModels from './generateTypesFromASTModels';

export default async (oasObjects: any[]) => {
    return await Promise.all(
        oasObjects.map(async (oasObj: any) => {
            return await {
                data: request({
                    url: oasObj.url,
                    json: false
                }),
                config: oasObj
            };
        })
    )
        .then((oasResults: any[]) => {
            // loop over each oas file and convert to ast
            const transformer = new converter.Converter(
                converter.Formats.OAS20,
                converter.Formats.RAML
            );

            return oasResults.map(async (oasResult: any) => {
                const { config } = oasResult;
                const data = await oasResult.data;
                return {
                    config,
                    ast: transformer.getModelFromData(data)
                };
            });
        })
        .then(async (asts: any) => {
            const _asts = await Promise.all(asts);
            return _asts.map(
                async (astModel: any) =>
                    await generateTypesFromASTModels(astModel)
            );
        });
};
