import { inspect } from 'util';
import { writeFileSync } from 'fs';
import { join } from 'path';
// Some basic ignores
const ignoreBasicProps = ['created', 'modified'];
const ignoreQueryProps = [''];
const ignoreMutationProps = ['id'];

function generateQueries(types: any) {
    const queries = Object.entries(types)
        .filter(([key, value]: any) => !/^type/gi.test(key))
        .map(([key, value]: any) => {
            return `  get${key}: ${key}`;
        })
        .join('\n');
    const query = `type Query {\n${queries}\n}`;
    return query;
}

function generateTypes(typeObj: any) {
    const fields = typeObj.fields.map((field: any) => {
        let description = field.description
            ? field.description.replace(/"""|`/g, '')
            : '';
        if (field.required && !ignoreBasicProps.includes(field.name)) {
            description += ' (Required)';
        }
        description = description != '' ? `\n  """${description}"""` : '';
        return `${description}\n  ${field.name}: ${field.type.trim()}`;
    });

    return `${typeObj.description || ''}\ntype ${typeObj.name} {${fields.join(
        ''
    )}
}`; // end type here for pretty output
}

export const generateQueriesAndMutations = (typeDefs: any) => {
    // generate root query for all types...

    // TODO: Find a better way to process types
    // const re = /type\s+(\w+)\s+\{\s+(([\B#]?.*\s+[^}].*)+)\s+\}/gmi;

    // For now scan the schema and build upon current data
    const types = typeDefs.split('\n');
    // console.log(types);
    // setup some rules based off of current schemas....
    const newTypes = {};
    const scalars = [];
    let schemaDef = '';
    let inSchema = false;
    let inType = false;
    let inField = false;
    let currentType = '';
    let currentField: any;
    for (let line of types) {
        const tempType = {
            name: null,
            description: null,
            fields: []
        };

        let tempField = {
            name: null,
            description: null,
            type: null,
            required: false
        };
        line = line.trim();

        if (line !== '') {
            // check if the schema is defined
            if (/^schema/i.test(line)) {
                //schema found
                inSchema = true;
                schemaDef = line;
                continue;
            }

            if (inSchema) {
                if (line === '}') {
                    inSchema = false;
                }
                schemaDef += inSchema ? `\n  ${line}` : `\n${line}`;
                continue;
            }

            // check for scalars
            if (/^scalar/i.test(line)) {
                scalars.push(line);
                continue;
            }

            if (/^type\s+/i.test(line)) {
                // found type
                inType = true;
                tempType.name = line.replace('type ', '').replace(' {', '');
                newTypes[tempType.name] = tempType;
                currentType = tempType.name;
                continue;
            }

            if (inType) {
                if (line === '}') {
                    inType = false;
                }

                if (/^#/i.test(line)) {
                    // found description for new field
                    if (inField) {
                        tempField.description = `"""${tempField.description
                            .replace('"""', '')
                            .replace(/#/gi, '')}\n${line
                            .replace('#', '')
                            .trim()}"""`;
                    } else {
                        tempField.description = `"""${line
                            .replace(/#/gi, '')
                            .trim()}"""`;
                    }
                    currentField = tempField;
                    // clear out tempField to not offset data
                    tempField = null;

                    if (!inField) {
                        inField = true;
                    }
                    continue;
                } else {
                    const field = currentField || tempField;

                    // is required?
                    if (/!$/i.test(line)) {
                        field.required = true;
                    }
                    const lineParts = line.split(':');
                    field.name = lineParts[0];
                    field.type = lineParts[1];
                    if (field.name !== '}') {
                        newTypes[currentType].fields.push(field);
                    }
                    inField = false;
                    // clear currentField for next iteration
                    currentField = null;
                }
            }
        } else {
            continue;
        }
    }
    console.log(inspect(newTypes, true, 99, true));
    // delete newTypes['type: String!'];
    // newTypes['BbQuestionHandler'] = {
    //     name: 'BbQuestionHandler',
    //     description: null,
    //     fields: [
    //         {
    //             name: 'type',
    //             description: `"""
    //                 Type of Question supported.
    //                 Presentation:  Since: 3300.9.0
    //                 EitherOr Read Only Since: 3300.9.0
    //                 Essay Read Only Since: 3300.9.0
    //                 MultipleAnswer Read Only Since: 3400.4.0
    //                 Numeric Read Only Since: 3400.4.0
    //                 MultipleChoice Read Only Since: 3400.4.0
    //                 Ordering Read Only Since: 3400.4.0
    //                 Matching Read Only Since: 3400.4.0
    //                 FillInTheBlank Read Only Since: 3400.4.0
    //                 Calculated Read Only Since: 3400.4.0
    //                 FileResponse Read Only Since: 3400.4.0
    //                 LikertOpinionScale Read Only Since: 3400.4.0
    //                 QuizBowl Read Only Since: 3400.4.0
    //                 HotSpot Read Only Since: 3400.4.0
    //                 JumbledSentence Read Only Since: 3400.4.0
    //                 FillInTheBlankPlus Read Only Since: 3400.4.0
    //                 """`,
    //             type: ' String!',
    //             required: true
    //         }
    //     ]
    // };
    const generatedTypes = Object.entries(newTypes)
        .map(([key, value]: any) => generateTypes(value))
        .join('\n');

    typeDefs = `# Auto Generated from Psyncho
${schemaDef}

${scalars.join('\n')}

${generateQueries(newTypes)}

${generatedTypes}
`;
    writeFileSync(join(__dirname, '../', 'graphql', 'schema.gql'), typeDefs);
    return typeDefs;
};
