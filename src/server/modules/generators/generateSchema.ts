// import { generateSchema } from 'graphql-liftoff';
import * as request from 'request-promise';
import { jsonToSchema } from '@walmartlabs/json-to-simple-graphql-schema/lib';
// export const genSchemaFromSwagger = (swaggerPath: string, prefix: string) =>
//     generateSchema('swagger', {
//         yaml: false,
//         data: swaggerPath,
//         p: prefix
//     });

export const genSchemaFromBasicAuth = async (requestOptions: any) => {
    return await request(requestOptions)
        .then((json: any) => {
            return jsonToSchema({
                jsonInput: JSON.stringify(json)
            }).value;
        })
        .catch((err: any) => new Error(err));
};
