import * as cmd from 'node-cmd';
import { join } from 'path';
export default ({ rootObj, typeDefs }: any) => {
        // generate queries for testing
        // gql-generator needs to be install globally
        const command = `gqlg --schemaFilePath ${join(
            __dirname,
            '..',
            'graphql',
            'schema.gql'
        )} --destDirPath ${join(
            __dirname,
            '..',
            'graphql',
            'output'
        )} --depthLimit 5`;
        cmd.get(command, (err: any, data: any, stderr: any) => {
            console.log('writing tests...');
            if (!err) {
                console.log(data);
            } else {
                console.log('ERROR', err);
            }

            console.log(stderr);
        });
        return { rootObj, typeDefs };
    }