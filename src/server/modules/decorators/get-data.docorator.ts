import * as request from 'request-promise';

export const DataResolver = () => {
    return (
        target: any,
        propertyKey: string | symbol,
        descriptor: TypedPropertyDescriptor<(...args: any[]) => any>,
        ) => {

        const originalMethod = descriptor.value;

        descriptor.value = async function(...args: any[]) {
            const config = this.config;

            for (const arg of args) {
                if (arg.config) {
                    delete args[arg].config;
                }
            }

            delete this.config;

            // check for params
            let endpoint = config.endpoint;

            if (config.params && config.params !== {}) {
                for (const key of Object.keys(config.params)) {
                    endpoint = endpoint.replace(new RegExp(`:${key}`), config.params[key]);
                }
            }

            const url = `${config.connectors[0].path}${endpoint}`;
            const options = {...config.defaultHttpOptions};

            options.uri = url;
            console.log(options);

            this.data = request(options);

            originalMethod.apply(this, args);
        };

        return descriptor;
    };
};
