import 'reflect-metadata';
import { GenericClassDecorator, Type } from '../types/generic-class-decorator.types';

export const Service = (): GenericClassDecorator<Type<object>> => (target: Type<object>) => {
    if (target) {
        const metadata = Reflect.getMetadata('design:paramtypes', target);
        if ( metadata) {
            console.log(metadata);
        }
    }
};
