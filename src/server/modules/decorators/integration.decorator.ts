import {
    GenericClassDecorator,
    Type
} from '../types/generic-class-decorator.types';
import { IIntegration } from '../types/integration.types';

export const integration = (): GenericClassDecorator<Type<object>> => (
    constructor: Type<IIntegration>
) => {
    const originalConstructor = constructor;

    const newConstructor = function(...args: any[]) {
        for (const arg of args) {
            if (arg.config) {
                delete args[arg].config;
            }
        }
        delete this.config;

        return new originalConstructor(...args);
    };

    newConstructor.prototype = originalConstructor.prototype;
    return newConstructor;
};
