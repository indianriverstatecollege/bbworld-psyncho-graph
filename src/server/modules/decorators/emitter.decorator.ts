import { GenericClassDecorator, Type } from '../types/generic-class-decorator.types';

export const Integration = (): GenericClassDecorator<Type<object>> => (constructor: Type<object>) => {
    // tslint:disable-next-line:no-commented-code
    const originalConstructor = constructor;

    const newConstructor = function(...args: any[]) {
        // implement emitter overrides here...
        for (const arg of args) {
            if (arg.config) {
                delete args[arg].config;
            }
        }
        delete this.config;

        return new originalConstructor(...args);
    };

    newConstructor.prototype = originalConstructor.prototype;
    return newConstructor;
};
