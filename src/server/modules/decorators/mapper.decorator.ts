import { GenericClassDecorator, Type } from '../types/generic-class-decorator.types';

export const Mapper = (): GenericClassDecorator<Type<object>> => (target: Type<object>) => {
    console.log(Reflect.getMetadata('design:paramtypes', target));
};
