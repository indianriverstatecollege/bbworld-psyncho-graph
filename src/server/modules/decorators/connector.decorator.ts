import { IConnector } from '../types/connector.types';
import { GenericClassDecorator, Type } from '../types/generic-class-decorator.types';

export const connector = (): GenericClassDecorator<Type<object>> => (constructor: Type<IConnector>) => {
    // tslint:disable-next-line:no-commented-code
    // return class extends constructor {};
    const originalConstructor = constructor;

    const newConstructor = function(...args: any[]) {
        // implement connector overrides here...
        for (const arg of args) {
            if (arg.config) {
                delete args[arg].config;
            }
        }
        delete this.config;

        return new originalConstructor(...args);
    };

    newConstructor.prototype = originalConstructor.prototype;
    return newConstructor;
};
