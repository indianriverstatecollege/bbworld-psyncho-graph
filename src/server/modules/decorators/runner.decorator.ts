import { GenericClassDecorator, Type } from '../types/generic-class-decorator.types';

export const Runner = (): GenericClassDecorator<Type<object>> => (target: Type<object>) => {
    console.log(Reflect.getMetadata('design:paramtypes', target));
};
