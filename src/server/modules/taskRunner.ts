import { timer } from 'rxjs';
import { graphql, buildSchema } from 'graphql';
import { wdResolverMap } from '../graphql/schemas/workday/resolvers';
import {
    bbResolverMap,
    bbMutationMap
} from '../graphql/schemas/blackboard/resolvers';
import {
    activeCoursesQuery,
    courseDefinitionsQuery,
    QUERY_REFRESH
} from '../configs/constants';
import {
    TERMS,
    GRAPHQL_CONFIG,
    WdIntCourseSectionsConfig,
    COURSE_DEFINITIONS_REFRESH,
    ACTIVE_COURSES_REFRESH
} from '../configs';
import PsynchoSchema from '../graphql/psyncho-schema';
import { getDbData } from './dataFromAndToDb';
import { Subject } from 'rxjs';
import { map, tap, exhaustMap, flatMap } from 'rxjs/operators';
import { inspect } from 'util';

export const ActiveCourses = new Subject();
export const CourseDefinitions = new Subject();

export const runCourseData = (expressApp: any, io: any) => {
    timer(1000, ACTIVE_COURSES_REFRESH)
        .pipe(
            map(async () => {
                const data: any = await graphql(
                    buildSchema(PsynchoSchema),
                    activeCoursesQuery,
                    Object.assign(
                        {},
                        wdResolverMap(io),
                        bbResolverMap(io),
                        bbMutationMap(io)
                    ),
                    {
                        db: expressApp.locals.db,
                        io,
                        ...GRAPHQL_CONFIG.context
                    },
                    { terms: TERMS, refresh: QUERY_REFRESH }
                ).catch((err: any) => {
                    throw new Error(err);
                });
                io.emit('active:courses', data);
                return data;
            }),
            tap(console.log)
        )
        .subscribe(async () => {
            console.log('Active Courses Updated.');
        });
};

const processCourseDefinitions = async (
    coursesTermMap: any,
    expressApp: any,
    io: any
) => {
    for (let [term, _courses] of Object.entries(coursesTermMap)) {
        console.log('TERM ==================>', term);
        console.log('COURSES ===============>', _courses);

        const data = await graphql(
            buildSchema(PsynchoSchema),
            courseDefinitionsQuery,
            Object.assign(
                {},
                wdResolverMap(io),
                bbResolverMap(io),
                bbMutationMap(io)
            ),
            {
                db: expressApp.locals.db,
                io,
                ...GRAPHQL_CONFIG.context
            },
            {
                externalIds: await _courses,
                term,
                refresh: QUERY_REFRESH
            }
        ).catch((err: any) => {
            throw new Error(err);
        });
        console.log(inspect(data, true, 5, true));
    }
};

export const runCourseDefinitions = (expressApp: any, io: any) => {
    // get courses from active_courses where courseId is empty
    timer(0, COURSE_DEFINITIONS_REFRESH)
        .pipe(
            map(async () => {
                let courses = await getDbData(
                    WdIntCourseSectionsConfig.model,
                    {
                        courseId: '',
                        externalTermId: { $in: TERMS },
                        availabilty: 'Y'
                    },
                    { isMany: true, select: '-_id externalId' }
                ).catch((err: any) => {
                    throw new Error(err);
                });
                courses = await courses.map((course: any) => course.externalId);
                return await courses;
            }),
            map(async (courses: any) => {
                // extract term since currently there is no mapping noe a way
                // through workday to do multi term polling in a single api endpoint
                courses = await courses;
                let coursesTermMap = {};
                courses.forEach((course: string) => {
                    // course: courseId-term-campus-session-section
                    // ASL1150-20193-51-B-001
                    const parts = course.split('-');
                    const term = `${parts[1]}${
                        parts[3] === 'M' ? '' : parts[3]
                    }`;
                    if (!coursesTermMap.hasOwnProperty(term)) {
                        coursesTermMap[term] = [];
                    }
                    coursesTermMap[term].push(course);
                });

                return coursesTermMap;
            }),
            exhaustMap(async (coursesTermMap: any) => {
                return processCourseDefinitions(
                    await coursesTermMap,
                    expressApp,
                    io
                );
            })
        )
        .subscribe(async () => {
            console.log('Course Definitions Updated.');
        });
};
