import * as express from 'express';
import { IRoute } from '../types/server.types';

/**
 * Creates an instance of a Route.
 * @export
 * @class Route
 */
export class Route {
    /**
     * The express.Router instance to attacht the route path.
     * @type {express.Router}
     * @memberof Route
     */
    public router: express.Router = express.Router();

    /**
     * The path that the router that application will mount.
     * @type {string}
     * @memberof Route
     */
    public path: string;

    /**
     * Creates an instance of Route.
     * @constructor
     * @param {IRoute} route Takes an IRoute interfaced object.
     * @memberof Route
     */
    constructor(private route: IRoute) {
        if (this.route.middlewares) {
            if (Array.isArray(this.route.middlewares)) {
                this.route.middlewares.forEach((middleware): any => {
                    this.router.use(middleware);
                });
            } else {
                this.router.use(this.route.middlewares);
            }
        }

        this.path = this.route.path;
        const _route = this.router.route('/');

        Object.keys(this.route.handlers).forEach((method: any) => {
            _route[method](this.route.handlers[method]);
        });

        if (this.route.childRoutes && this.route.childRoutes !== []) {
            this.setupChildRoutes();
        }
    }

    /**
     * This function will not only create the child routes for the given path
     * but will recursively create new instances of routers and attach them
     * to the child parent route.
     * @function setupChildRoutes Applies the child routes to the router
     * @memberof Route
     * @returns {void}
     */
    public setupChildRoutes(): void {
        this.route.childRoutes.forEach((childRoute: any) => {
            const _route = this.router.route(childRoute.path);

            if (childRoute.middlewares && childRoute.middlewares !== []) {
                _route.all(childRoute.middlewares);
            }

            Object.keys(childRoute.handlers).forEach((method: any) => {
                _route[method](childRoute.handlers[method]);
            });

            if (childRoute.childRoutes && childRoute.childRoutes !== []) {
                const newRouter = new Route(childRoute);
                this.router.use(newRouter.path, newRouter.router);
            }
        });
    }

    /**
     * Convenience getter to be used when attaching middlewares like in setupChildRoutes
     * @readonly
     * @memberof Route
     */
    get middlewares() {
        return this.route.middlewares;
    }
}
