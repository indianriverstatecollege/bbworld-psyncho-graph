import { enumType } from '../types/connector.types';
import { enumErrorAction, enumErrorLevel, IErrorHandler } from '../types/error.types';
import { enumMode, IIntegration } from '../types/integration.types';
import { IMapper } from '../types/mapper.types';
import { Connector } from './connector';
import { Integration } from './integration';
import { Mapper } from './mapper';
import { ErrorHandler } from '../services/error.service';

const errorConfig: IErrorHandler = {
    name: 'Test Error',
    description: 'A simple test for unit testing.',
    error: Error('Test Error'),
    service: { name: 'unit', caller: 'test'},
    handler: (err: any) => {
        if (this.action === enumErrorAction.console) {
            console.log(err);
        }
        return err;
    },
    action: enumErrorAction.console,
    level: enumErrorLevel.debug,
};

const integrationConfig: IIntegration = {
    connector: new Connector({
        type: enumType.httpx,
        description: 'This is a test description',
        name: 'Main Connector',
        path: 'https://username:password@google.com',
        prefix: 'main_connector',
    }),
    data: {},
    endpoint: '/workday/report',
    errorHandlers: [new ErrorHandler(errorConfig)],
    mode: enumMode.active,
    model: null,
    collectionName: null,
    params: [{
        student: '1234',
    }],
};

const mapperConfig: IMapper = {
    errorHandlers: [new ErrorHandler(errorConfig)],
    integrations: [new Integration(integrationConfig)],
    mode: enumMode.active,
    parameters: [],
    prefix: 'workday',
    required: true,
};

describe('Test suite for Mapper', () => {
    it('Create a new instance of Mapper', () => {
        const mapper = new Mapper(mapperConfig);

        expect(mapper).toBeInstanceOf(Mapper);
        expect(mapper.required).toBeTruthy();

    });

    it('Should determine the mapper mode and delegate actions.', () => {
        const mapper = new Mapper(mapperConfig);

        mapper.mode = enumMode.draft;
        let result = mapper.runIntegrations();
        expect(result).toBeUndefined();

        mapper.mode = enumMode.deprecated;
        result = mapper.runIntegrations();
        expect(result).toBeUndefined();

        mapper.mode = enumMode.disabled;
        result = mapper.runIntegrations();
        expect(result).toBeUndefined();

        mapper.mode = enumMode.test;
        result = mapper.runIntegrations();
        expect(result).not.toBeUndefined();

        mapper.mode = enumMode.active;
        result = mapper.runIntegrations();
        expect(result).not.toBeUndefined();
    });
});
