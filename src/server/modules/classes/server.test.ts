
import { testConfig } from '../../configs/express-server.config';
import { Server } from './server';
import { Route } from './route';
import { App } from './app';

describe('Test suite for Server', () => {
    it('Should create a new server', () => {
        const server = new Server(new App(testConfig));
        expect(server).toBeInstanceOf(Server);
    });

    it('Should test if not in test mode', (done) => {
        process.env.NODE_ENV = 'test';
        // testConfig.port = 4000;
        const server = new Server(new App(testConfig));
        server.runServer();
        expect(server).toBeInstanceOf(Server);
        done();
    });

    it('Should set the server/application config', () => {
        const server = new Server(new App(testConfig));
        server.runServer();
        testConfig.routers = [
            new Route({
                path: '/test',
                handlers: {
                    get: (req: any, res: any) => {
                        res.json({ msg: 'This is only a test.'});
                    },
                },
            }),
        ];
        server.setConfig(testConfig);
        server.configRoutes();

        expect(server.application.config.routers[0].path).toEqual('/test');
    });

});
