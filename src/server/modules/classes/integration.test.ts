import { inspect } from 'util';
import { WdIntRegStudentCSConfig } from '../../configs/workday/workday.config';
import { ErrorHandler } from '../services/error.service';
import { enumType } from '../types/connector.types';
import {
    enumErrorAction,
    enumErrorLevel,
    IErrorHandler
} from '../types/error.types';
import { enumMode } from '../types/integration.types';
import { Connector } from './connector';
import { Integration } from './integration';
const errorConfig: IErrorHandler = {
    name: 'Test Error',
    description: 'A simple test for unit testing.',
    error: Error('Test Error'),
    service: { name: 'unit', caller: 'test' },
    handler: (err: any) => {
        if (this.action === enumErrorAction.console) {
            console.log(err);
        }
        return err;
    },
    action: enumErrorAction.console,
    level: enumErrorLevel.debug
};

describe('Testing the main Integration class', () => {
    it('Should create a new Integration instance', () => {
        const integration = new Integration({
            connector: new Connector({
                type: enumType.httpx,
                description: 'This is a test description',
                name: 'Main Connector',
                path: 'https://username:password@google.com',
                prefix: 'main_connector'
            }),
            data: { hello: 'Hi from test' },
            endpoint: '/workday/report',
            errorHandlers: [new ErrorHandler(errorConfig)],
            mode: enumMode.active,
            model: null,
            modelManyKey: 'hello',
            collectionName: null,
            params: {
                student: '1234'
            },
            defaultHttpOptions: {
                method: 'get'
            }
        });
        integration.fetchData = jest.fn().mockReturnValue({
            connectors: [{ type: enumType.httpx }],
            endpoint: '/workday/report',
            params: {
                student: '1234'
            }
        });
        integration.fetchData('/irsc/workday/report');
        expect(integration).toBeInstanceOf(Integration);
        expect((integration as any).connectors[0].type).toBe(enumType.httpx);
        expect((integration as any).endpoint).toEqual('/workday/report');
        expect((integration as any).params.student).toEqual('1234');
    });

    it('Should connect to Workday and return data', async () => {
        const intConfig = { ...WdIntRegStudentCSConfig };
        const externalCourseId = 'WOH2040-20192-1V-M-001';
        const courseParts = externalCourseId.split('-');
        const _course = courseParts[0];
        const term = courseParts[1];
        const campus = courseParts[2];
        const session = courseParts[3];
        const section = courseParts[4];
        const courseId = `${_course}-${campus}-${section}`;
        const academicPeriod =
            term.toString() + (session === 'M' ? '' : session);

        intConfig.params = {
            courseId,
            academicPeriod
        };

        const integration: Integration = new Integration(intConfig);
        const data = await (integration as any).fetchData();
        console.log(inspect(data, true, 99, true));
        // tslint:disable-next-line:no-commented-code
        // data.subscribe(async (payload: any) => {
        //     const _data = await payload;
        //     // tslint:disable-next-line: no-commented-code
        //     // console.log(util.inspect(_data, false, null, true /* enable colors */));
        //     expect(_data).toHaveProperty('Report_Entry');
        // });
    });

    // tslint:disable-next-line:no-commented-code
    // it('Should connect to Workday and fail', (done) => {
    //     const intConfig = {...WdIntRegStudentCSConfig};
    //     intConfig.endpoint = '';
    //     intConfig.connectors[0].config.path = '';
    //     intConfig.connectors[0].config.credentials.token = '';
    //     const integration = new BaseIntegration(intConfig);
    //     const data = (integration as any).fetchData();
    //     data.subscribe(async (payload: any) => {
    //         const _data = await payload;
    //         // tslint:disable-next-line: no-commented-code
    //         // console.log(util.inspect(_data, false, null, true /* enable colors */));
    //         expect(_data).toBeUndefined();
    //         done();
    //     });
    // });
});
