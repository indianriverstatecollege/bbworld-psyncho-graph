import { Transformer } from './transformer';

describe('Test suite for Transformer', () => {
    it('Should create and instance of Transformer', () => {
        const transformer = new Transformer({
            data: { transform: 'Hello'},
            context: {},
            code: 'data.transform += " world!"',
            params: { msg: 'Hi, again ;p'},
        });
        expect(transformer).toBeInstanceOf(Transformer);
    });

    it('Should perform a transformation', async () => {
        const transformer = new Transformer({
            data: { transform: 'Hello'},
            context: {global},
            code: 'data.transform += " world!"',
            params: { msg: 'Hi, again ;p'},
        });
        const result = await transformer.run();
        console.log(result);
        expect(result.transform).toBe('Hello world!');
    });

    it('Should return a rejected error', async () => {
        const transformer = new Transformer({
            data: null,
            context: null,
            code: null,
            params: null,
        });

        transformer.run = jest.fn().mockImplementation().mockRejectedValue('Error');
        const result = transformer.run();
        return expect(result).rejects.toMatch('Error');
    });
});
