import { createTransport } from 'nodemailer';
import { createLogger, Logger } from 'winston';
import * as emailConfig from '../../configs/emailer.config';
import { ILog } from '../types/log.types';
/**
 * Patches and exposes the Mail transport for winston
 * @constant
 */
// tslint:disable-next-line:no-var-requires
const { Mail } = require('winston-mail');

/**
 * Log class that wraps around winston logger.
 *
 * @export
 * @class Log
 */
export class Log {
    /**
     * The convenience logger instance;
     *
     * @type {LoggerOptions}
     * @memberof Log
     */
    public logger: Logger;

    /**
     * Mailer, the mail server that will send that email
     *
     * @memberof Log
     */
    public mailer = undefined;
    /**
     * Creates an instance of Log.
     * @param {Logger} config
     * @memberof Log
     */
    constructor(private iLog: ILog) {
        this.logger = createLogger(iLog.config);

        // detect config settings and error levels
        // if email call it separatly as the mail transport is added differently
        if (iLog.emailConfig) {
            this.logger.add(new Mail(iLog.emailConfig));
            this.mailer = createTransport(emailConfig as any);
        }

    }

    /**
     * Console.log() convenience method.
     *
     * @param {*} msg
     * @memberof Log
     */
    public console(msg: any) {
        console.log(msg);
    }

    // for cli and npm levels
    /**
     * Error convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public error(msg: any) {
        this.logger.error(msg);
    }

    /**
     * Send an email per the email options that were passed in
     *
     * @param {*} msg
     * @memberof Log
     */
    public email(msg: any) {
        // how does the node-mail/emailjs module call the email function?
        // this.logger.log({level: this.level, message: msg});
        this.logger.log({level: 'info', message: msg});
        const mailOptions = {
            to: 'email@gmail.com',
            from: 'email@gmail.com',
            subject: 'Test Email from Logger',
            text: msg,
        };

        const info = this.mailer.sendMail(mailOptions);

        console.log('Message sent: %s', info.messageId);
    }

    /**
     * Warn convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public warn(msg: any) {
        this.logger.warn(msg);
    }

    /**
     * Info convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public info(msg: any) {
        this.logger.info(msg);
    }

    /**
     * Debug convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public debug(msg: any) {
        this.logger.debug(msg);
    }

    /**
     * Http convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public http(msg: any) {
        this.logger.http(msg);
    }

    /**
     * Verbose convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public verbose(msg: any) {
        this.logger.verbose(msg);
    }

    /**
     * Silly convenience method
     *
     * @param {*} msg
     * @memberof Log
     */
    public silly(msg: any) {
        this.logger.silly(msg);
    }
}
