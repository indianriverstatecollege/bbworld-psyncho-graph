import * as Bluebird from 'bluebird';
import * as mongoStore from 'connect-mongo';
import * as session from 'express-session';
import * as mongoose from 'mongoose';
import * as graphqlQLSchema from 'swagger-to-graphql';
import express = require('express');
import graphqlHTTP = require('express-graphql');
import * as request from 'request-promise';
import { PsynchoSchema } from '../../graphql/schemas';
import { injectable, inject } from 'inversify';

import { IServerConfig } from '../types/server.types';
import { INJECTION_TYPES } from '../types/injection.types';

import { WorkdayActiveEnrollmentsLoader } from '../../graphql/schemas/workday/loaders/active-enrollments-loader';
import { WorkdayStudentLoader } from '../../graphql/schemas/workday/loaders/user-student-loader';
import { WorkdayAdvisorReportLoader } from '../../graphql/schemas/workday/loaders/advisor-reports-loader';
import { WorkdayCourseDefinitionLoader } from '../../graphql/schemas/workday/loaders/course-definition-loader';
import { WorkdayEducationalInstLoader } from '../../graphql/schemas/workday/loaders/educational-institutions-loader';
import { WorkdayCourseInfoLoader } from '../../graphql/schemas/workday/loaders/course-information-loader';
import { BbAnnouncementsLoader } from '../../graphql/schemas/blackboard/loaders/announcements-loader';
// import { BbDataSourcesLoader } from '../../graphql/schemas/blackboard/loaders/data-sources-loader';

import { cacheMapFactory } from '../../graphql/schemas/psyncho/cacheMapFactory';
import { loaderFactory } from '../../graphql/schemas/psyncho/loaderFactory';
// import { BbDataSourcesIntConfig, BbStagingConnConfig } from '../../configs';
import { BbDataSourcesIntConfig, BbConnection } from '../../configs';
import { Integration } from './integration';
export const ENV = process.env.NODE_ENV || 'development';

export const mongooseConfig: any = {
    database: 'mongodb://localhost:27017/psyncho',
    options: {
        useNewUrlParser: true
    }
};

/**
 * App is a convenience class to create and store an express application
 * and configuration object to be applied.
 * @export
 * @class App
 */
@injectable()
export class App {
    /**
     * The express application
     * @type {express.Application}
     * @memberof App
     */
    public app: express.Application;

    /**
     * The Server/App configuration object.
     * @type {*}
     * @memberof App
     */
    public config: IServerConfig;

    /**
     * Creates the application instance to be used. Sets the configutation object for the application.
     * @constructor
     * @memberof App
     */
    constructor(@inject(INJECTION_TYPES.ServerConfig) config: IServerConfig) {
        mongoStore(session);
        (mongoose as any).Promise = Bluebird;

        mongoose.connection.once('open', () => {
            mongoose.connection.on('error', (err: any) => {
                console.log(err);
            });
        });

        mongoose.connect(mongooseConfig.database, mongooseConfig.options);
        this.app = express();

        this.setConfig(config);
        this.setupApp();

        this.setupGraphQL();
    }

    public setupGraphQL() {
        /**
         * testing graphql
         */

        // The root provides a resolver function for each API endpoint
        const root = {
            hello: () => {
                return 'Hello world!';
            }
        };

        // const BbDataSourcesLoader = loaderFactory({
        //     cache: true,
        //     cacheCheckFn: key => JSON.stringify(key),
        //     errorMessage: 'Data Sources returned null or was empty',
        //     cacheMap: cacheMapFactory('BbDataSourceMap'),
        //     integration: new Integration(BbDataSourcesIntConfig)
        // });
        // TODO: Lookinto type-graphql: buildSchema to generate the schema file....
        request({
            uri: `${BbConnection.path}${BbConnection.oauthPath}`,
            method: 'post',
            headers: {
                Authorization: `Basic ${BbConnection.credentials.auth}`
            },
            form: {
                grant_type: 'client_credentials'
            },
            json: true
        })
            .then((token: any) => {
                console.log(token);
                graphqlQLSchema(
                    `${__dirname}/../../graphql/schemas/blackboard/learn-swagger-3200.0.8.json`,
                    'https://yourinstance.blackboard.com',
                    {
                        Authorization: `Bearer ${token}`
                    }
                ).then((schema: any) => {
                    console.log(schema);
                    this.app.use(
                        '/psynchoql',
                        graphqlHTTP((req: any, res: any) => {
                            const options = {
                                ...this.config.graphQLHttpOptions
                            };

                            options.schema = schema;
                            options.context = {
                                req,
                                res,
                                WorkdayActiveEnrollmentsLoader,
                                WorkdayStudentLoader,
                                WorkdayAdvisorReportLoader,
                                WorkdayCourseListingLoader: WorkdayCourseDefinitionLoader,
                                WorkdayEducationalInstLoader,
                                WorkdayCourseInfoLoader
                            };

                            return options;
                        })
                    );
                });
            })
            .catch(err => new Error(err));
    }

    /**
     * Sets the configutation object for the application.
     * @function setConfig
     * @param {*} config The Application Configuration object
     * @memberof App
     * @returns {void}
     */
    public setConfig(config: any): void {
        this.config = config;
    }

    /**
     * Sets up the application for middlewares based on the provided configuration object.
     * @function setupApp
     * @memberof App
     * @returns {void}
     */
    public setupApp(): void {
        /**
         * @argument middleware An express middleware function to be applied to the app.
         */
        if (this.config.middlewares && this.config.middlewares !== []) {
            this.config.middlewares.forEach((middleware: any) => {
                this.app.use(middleware);
            });
        }

        // add sets
        if (this.config.sets && this.config.sets !== []) {
            this.config.sets.forEach((appSet: any) => {
                this.app.set(appSet.key, appSet.value);
            });
        }

        // turn on express features
        if (this.config.enables && this.config.enables !== []) {
            this.config.enables.forEach((feature: any) => {
                this.app.enable(feature);
            });
        }

        // turn on express (app) local vars
        if (this.config.locals && this.config.locals !== []) {
            this.config.locals.forEach((localVar: any) => {
                this.app.locals[localVar.name] = localVar.value;
            });
        }
    }
}
