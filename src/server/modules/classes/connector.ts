import { connector } from '../decorators/connector.decorator';
import { IConnector, ICredentials } from '../types/connector.types';
/**
 * Connector Class that houses the connection details for Integrations.
 *
 * @export
 * @class Connector
 */
@connector()
export class Connector {
        /**
         * The credentials object.
         * @type {ICredentials}
         * @memberof Connector
         */
        public credentials: ICredentials;

        /**
         * Describes the connector connection details.
         * @type {string}
         * @memberof Connector
         */
        public description: string;

        /**
         * The friendly name for the Connector.
         * @type {string}
         * @memberof Connector
         */
        public name: string;

        /**
         * Any additional notes for the Connector.
         * @type {string}
         * @memberof Connector
         */
        public notes: string;

        /**
         * The connection path for the Connector.
         * @type {string}
         * @memberof Connector
         */
        public path: string;
        /**
         * The path to request for a access token
         *
         * @type {string}
         * @memberof IConnector
         */
        public oauthPath?: string;
        /**
         * The prefix that will be applied to the collection in the database.
         * @type {string}
         * @memberof Connector
         */
        public prefix: string;

        /**
         * The the type of connection that the Connector will using.
         * @type {*}
         * @memberof Connector
         */
        public type: any;
    constructor(private config: IConnector) {
        this.credentials = config.credentials;
        this.description = config.description;
        this.type = config.type;
        this.name = config.name;
        this.notes = config.notes;
        this.path = config.path;
        this.oauthPath = config.oauthPath;
        this.prefix = config.prefix;
    }
}
