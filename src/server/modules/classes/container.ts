import 'reflect-metadata';

export const Type = Function;
export interface Type<T> extends Function { new(...args: any[]): T; }
export function isType(v: any): v is Type<any> { return typeof v === 'function'; }
export type Mutable<T extends{[x: string]: any}, K extends string> = { [P in K]: T[P]; };
export type GenericClassDecorator<T> = (target: T) => void;

export class InjectionToken {
    constructor(public injectionIdentifier: string) {}
}
// Our combined Token type
export type Token<T> = Type<T> | InjectionToken;

// This class decorator adds a boolean property to the class
// metadata, marking it as 'injectable'. 
// It uses the reflect-metadata API.
const INJECTABLE_METADATA_KEY = Symbol('INJECTABLE_KEY');
export function Injectable() {
    return function(target: any) {
        // target in this case is the class being decorated.    
        Reflect.defineMetadata(INJECTABLE_METADATA_KEY, true, target);
        return target;
    };
}
// We also provide an easy way to query whether a class is
// injectable. Our container will reject classes which aren't
// marked as injectable.
export function isInjectable<T>(target: Type<T>) {
    return Reflect.getMetadata(INJECTABLE_METADATA_KEY, target) === true;
}

const INJECT_METADATA_KEY = Symbol('INJECT_KEY');
// This is a parameter decorator, it takes a token to map the parameter to.
export function Inject(token: Token<any>) {
    return function(target: any, _: string | symbol, index: number) {
        Reflect.defineMetadata(INJECT_METADATA_KEY, token, target, `index-${index}`);
        return target;
    };
}
export function getInjectionToken(target: any, index: number) {
    return Reflect.getMetadata(INJECT_METADATA_KEY, target, `index-${index}`) as Token<any> | undefined;
}


// Every provider maps to a token.
export interface BaseProvider<T> {
    provide: Token<T>;
}

export interface ClassProvider<T> extends BaseProvider<T> {
    useClass: Type<T>;
}

export interface ValueProvider<T> extends BaseProvider<T> {
    useValue: T;
}

// To keep things simple, a factory is just a function which creates the type.
export type Factory<T> = () => T;

export interface FactoryProvider<T> extends BaseProvider<T> {
    useFactory: Factory<T>;
}

export type Provider<T> = ClassProvider<T> | ValueProvider<T> | FactoryProvider<T>;

export function isClassProvider<T>(provider: BaseProvider<T>): provider is ClassProvider<T> {
    return (provider as any).useClass !== undefined;
}
export function isValueProvider<T>(provider: BaseProvider<T>): provider is ValueProvider<T> {
    return (provider as any).useValue !== undefined;
}
export function isFactoryProvider<T>(provider: BaseProvider<T>): provider is FactoryProvider<T> {
    return (provider as any).useFactory !== undefined;
}

type InjectableParam = Type<any>;
const REFLECT_PARAMS = "design:paramtypes";

export class Container {
    private providers = new Map<Token<any>, Provider<any>>();

    addProvider<T>(provider: Provider<T>) {
        this.assertInjectableIfClassProvider(provider);
        this.providers.set(provider.provide, provider);
    }
    
    // adding a convienence methods to add as an array
    addProviders<T>(providers: Provider<any>[]) { 
        for (const provider of providers) { 
            this.addProvider(provider);
        }
    }
    
    getProvider<T>(provider: any) {
        const instance = this.providers.get(provider).provide ||
            this.providers.get(InjectionToken).provide;
        // return new instance();
    }

    inject<T>(type: Token<T>): T {
        let provider = this.providers.get(type);
        if (provider === undefined && !(type instanceof InjectionToken)) {
            provider = { provide: type, useClass: type };
            this.assertInjectableIfClassProvider(provider);
        }
        return this.injectWithProvider(type, provider);
    }

    // Returns a printable name for the token.
    private getTokenName<T>(token: Token<T>) {
        return token instanceof InjectionToken ? token.injectionIdentifier : token.name;
    }

    private assertInjectableIfClassProvider<T>(provider: Provider<T>) {
        if (isClassProvider(provider) && !isInjectable(provider.useClass)) {
            throw new Error(
            `Cannot provide ${this.getTokenName(provider.provide)} using class ${this.getTokenName(
                provider.useClass
            )}, ${this.getTokenName(provider.useClass)} isn't injectable`
            );
        }
    }

    private injectWithProvider<T>(type: Token<T>, provider?: Provider<T>): T {
        if (provider === undefined) {
            throw new Error(`No provider for type ${this.getTokenName(type)}`);
        }
        if (isClassProvider(provider)) {
            return this.injectClass(provider as ClassProvider<T>);
        } else if (isValueProvider(provider)) {
            return this.injectValue(provider as ValueProvider<T>);
        } else {
            // Factory provider by process of elimination
            return this.injectFactory(provider as FactoryProvider<T>);
        }
    }

    private injectValue<T>(valueProvider: ValueProvider<T>): T {
        return valueProvider.useValue;
    }

    private injectFactory<T>(valueProvider: FactoryProvider<T>): T {
        return valueProvider.useFactory();
    }

    private injectClass<T>(classProvider: ClassProvider<T>): T {
        const target = classProvider.useClass;
        const params = this.getInjectedParams(target);
        return Reflect.construct(target, params);
    }

    private getInjectedParams<T>(target: Type<T>) {
        const argTypes = Reflect.getMetadata(REFLECT_PARAMS, target) as (InjectableParam | undefined)[];
        if (argTypes === undefined) {
            return [];
        }
        return argTypes.map((argType, index) => {
            // The reflect-metadata API fails on circular dependencies,
            // and will return undefined for the argument instead.
            // We could handle this better, but for now let's just throw an error.
            if (argType === undefined) {
                throw new Error(
                    `Injection error. Recursive dependency detected in constructor for type ${
                    target.name
                    } with parameter at index ${index}`
                );
            }
            // Check if a 'Inject(INJECTION_TOKEN)' was added to the parameter.
            // This always takes priority over the parameter type.
            const overrideToken = getInjectionToken(target, index);
            const actualToken = overrideToken === undefined ? argType : overrideToken;
            let provider = this.providers.get(actualToken);
            return this.injectWithProvider(actualToken, provider);
        });
    }
}
