import * as ts from 'typescript';
import * as vm from 'vm';
import { ITransformer } from '../types/transformer.types';

/**
 * Transformer is used to run data transformation
 *
 * @export
 * @class Transformer
 */
export class Transformer {

    /**
     * Data is the incoming data object
     *
     * @type {*}
     * @memberof Transformer
     */
    public data: any;
    /**
     * Code is the transformation function
     *
     * @type {*}
     * @memberof Transformer
     */
    public code: any;
    /**
     * Params exposes an object to pass parameters to code context
     *
     * @type {*}
     * @memberof Transformer
     */
    public params: any;
    /**
     * Context exposes the context to the code context for safe evaluation
     *
     * @type {*}
     * @memberof Transformer
     */
    public context: any;

    /**
     * Creates an instance of Transformer.
     * @param {*} data
     * @param {*} code
     * @param {*} params
     * @param {*} context
     * @memberof Transformer
     */
    constructor(config: ITransformer) {
        this.data = config.data;
        this.code = config.code;
        this.params = config.params;
        this.context = config.context;
    }

    /**
     * Run is the transpiling caller
     *
     * @param {*} data
     * @param {*} code
     * @param {*} params
     * @param {*} context
     * @returns
     * @memberof Transformer
     */
    public async run() {

        if (this.context.global) {
            delete this.context.global;
        }

        vm.createContext(this.context);
        const result = ts.transpile(`
            ({Run: (data:any, params:any): any => { ${this.code} return Promise.resolve(data); }})
        `);
        const runnable: any = vm.runInContext(result, this.context);

        return runnable.Run(this.data, this.params)
            .then(async (res: any) => res)
            .catch( /* istanbul ignore next */ (err: any) => err);
    }

}
