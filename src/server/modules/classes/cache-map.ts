export abstract class CacheMap {
    stash: Object;
    name: string;
    constructor() {
        this.stash = {};
        this.name = this.constructor.name;
    }

    get(key: any) {
        console.log(`${this.name} key:`, key);
        return this.stash[key];
    }

    set(key: any, value: any) {
        console.log(`${this.name} setting key:`, key);
        this.stash[key] = value;
    }

    delete(key: any) {
        delete this.stash[key];
    }

    clear() {
        this.stash = {};
    }
}
