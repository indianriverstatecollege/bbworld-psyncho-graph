import { loggerOptions } from '../../../../configs/app-logger.config';
import emailOptions from '../../../../configs/emailer.config';
import { ILog } from '../types/log.types';
import { Log } from './logger';

const iLogConfig: ILog = {
    config: loggerOptions,
    emailConfig: emailOptions,
};

const log = new Log(iLogConfig);

log.email('Test email');
