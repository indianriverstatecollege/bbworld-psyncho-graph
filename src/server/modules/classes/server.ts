import { App } from './app';
import { inject, injectable } from 'inversify';
import { INJECTION_TYPES } from '../types/injection.types';

/**
 * Server creates the actual application and applies the routes
 * @export
 * @class Server
 */
@injectable()
export class Server {
    /**
     * The Application Class that holds the configuration
     * @type {*}
     * @memberof Server
     */
    public application: App;

    /**
     * The actual express application instance.
     * @type {*}
     * @memberof Server
     */
    public app: any;

    public port = 3000;
    /**
     * Creates the server instance to be used.
     * @constructor
     * @param {*} _application The Application Class instance to be used for the Server.
     * @memberof Server
     */
    constructor(@inject(INJECTION_TYPES.App) app: App) {
        this.application = app;
        this.configRoutes();
    }

    /**
     * Sets us the routers for the application.
     * @function configRoutes Sets us the routers for the application.
     * @memberof Server
     * @returns {void}
     */
    public configRoutes(): void {
        if (this.application.config) {
            this.application.config.routers.forEach((router: any) => {
                this.application.app.use(router.path, router.router);
            });
            this.port = this.application.config.port || this.port;
        }
    }

    /**
     * Sets up the configuration for the application.
     * @function setConfig Sets up the configuration for the application.
     * @param {*} config The configuration object.
     * @memberof Server
     * @returns {void}
     */
    public setConfig(config: any): void {
        this.application.setConfig(config);
    }

    /**
     * Runs the actual application instance.
     * @function runServer
     * @memberof Server
     * @returns {*} Returns the application instance for convenience.
     */
    public runServer(): any {
        /* istanbul ignore if  */
        if (process.env.NODE_ENV !== 'test') {
            this.application.app.listen(this.port, () => {
                console.log(
                    `Server is running on http://localhost:${this.port}`
                );
            });
        }

        this.app = this.application.app;
        return this.application.app;
    }
}
