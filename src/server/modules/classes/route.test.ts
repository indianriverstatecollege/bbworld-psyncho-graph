import { Route } from './route';

describe('Testing the main Route class', () => {

    it('Should create a new Route instance', () => {
        const route = new Route({
            path: '/api',
            handlers: {
                get: (req: any, res: any) => {
                    res.status(200).json({ msg: 'this is only a test' });
                },
            },
        });

        expect(route).toBeInstanceOf(Route);
        expect(route.path).toEqual('/api');
    });

    it('Should create a new Route instance with a single middleware', () => {
        const route = new Route({
            path: '/api',
            middlewares: (req: any, res: any, next: any) => {
                console.log('Route Middleware test...');
                next();
            },
            handlers: {
                get: (req: any, res: any) => {
                    res.status(200).json({ msg: 'this is only a test' });
                },
            },
        });
        const middlewares = route.middlewares;
        expect(route).toBeInstanceOf(Route);
        expect(route.path).toEqual('/api');
        expect(middlewares).toBeInstanceOf(Function);
    });

});
