import * as request from 'request-promise';
import { integration } from '../decorators/integration.decorator';
import { enumMode, IIntegration, IParam } from '../types/integration.types';
import { Connector } from './connector';
import { enumType } from '../types/connector.types';
// import { DataResolver } from '../decorators/get-data.docorator';

/**
 * Integration Class that holds the details for the connection endpoint.
 * @export
 * @class Integration
 */
@integration()
export class Integration {
    /**
     * A connector to use in retrieving data from a system or internal mapping.
     * @type {Connector}
     * @memberof Integration
     */
    public connector: Connector;

    /**
     * The data (payload) that is stored coming from the integration endpoint.
     * @type {*}
     * @memberof Integration
     */
    public data: any;

    /**
     * The index to be applied to the model.
     * @type {string}
     * @memberof Integration
     */
    public index: string;

    /**
     * The endpoint path for the connection type to used after connecting.
     * @type {string}
     * @memberof Integration
     */
    public endpoint: string;

    /**
     * Error Handlers to be used for this integration.
     * @type {*}
     * @memberof Integration
     */
    public errorHandlers: any;

    /**
     * The mode to describe the state of the integration.
     * @type {enumMode}
     * @memberof Integration
     */
    public mode: enumMode;

    /**
     * A list of models that will used to import/validate data coming from the endpoint payload.
     * @type {*}
     * @memberof Integration
     */
    public model: any;
    /**
     * An object containig the key and or sub object to obtain the next page
     *
     * @type {*}
     * @memberof Integration
     */
    public modelPagination?: any;
    /**
     * An object of key:value pairs to be injected into the model upon saving with defaults.
     * @type {*}
     * @memberof Integration
     */
    public modelInjectionFields: any;
    /**
     * A list of model include fields that will be injected into the retreiving or comparing process.
     * @type {*}
     * @memberof Integration
     */
    public modelIncludeFields: any;
    /**
     * A list of model exclude fields that will be removed from the retreiving or comparing process.
     * @type {*}
     * @memberof Integration
     */
    public modelExcludeFields: any;

    /**
     * A string that represents the many payload response incase that the returned payload
     * does not match: single (object) vs. many (array of objects)
     *
     * @type {string}
     * @memberof Integration
     */
    public modelManyKey: string;
    /**
     * The name of the collection that will be used for the payloads from the integration.
     *
     * @type {string}
     * @memberof IIntegration
     */
    public collectionName: string;
    /**
     * A key:value pair object that will be used in the endpoint or data payload to the connection integration.
     * @type {IParam}
     * @memberof Integration
     */
    public params: IParam;
    /**
     * The default http options
     *
     * @type {any}
     * @memberof Integration
     */
    public defaultHttpOptions: any;
    /**
     * If available then adds the http methods for the crud call
     *
     * @type {any}
     * @memberof Integration
     */
    public crudOps: any;
    /**
     * The and option to refresh the current data context
     *
     * @type {boolean}
     * @memberof Integration
     */
    public refresh: boolean;
    /**
     * The httpOptions to use for http operations
     *
     * @type {boolean}
     * @memberof Integration
     */
    public httpOptions: any;

    /* Creates an instance of Integration.
     * @constructor
     * @param {IIntegration} config
     * @memberof Integration
     */
    constructor(private config: IIntegration) {
        this.config = config;
        this.connector = config.connector;
        this.mode = config.mode;
        this.params = config.params;
        this.data = config.data;
        this.index = config.index;
        this.endpoint = config.endpoint;
        this.defaultHttpOptions = config.defaultHttpOptions;
        this.crudOps = config.crudOps;
        this.errorHandlers = config.errorHandlers;
        this.collectionName = config.collectionName;
        this.model = config.model;
        this.modelManyKey = config.modelManyKey;
        this.modelIncludeFields = config.modelIncludeFields;
        this.modelExcludeFields = config.modelExcludeFields;
        this.modelInjectionFields = config.modelInjectionFields;
        this.modelPagination = config.modelPagination;
        this.refresh = false;
        this.httpOptions = null;
    }

    private async setupHttpOptions() {
        // console.log('[setupHttpOptions()]');
        this.httpOptions = { ...this.defaultHttpOptions };
        // if oauth then we need to pre auth our request
        if (this.connector.type === enumType.oauth) {
            // httpOptions = { ...this.defaultHttpOptions };
            const oauthEndpoint = `${this.connector.path}${
                this.connector.oauthPath
            }`;
            this.httpOptions.uri = oauthEndpoint;
            this.httpOptions.method = 'post';
            this.httpOptions.form = {
                grant_type: this.connector.credentials.grantType
            };

            const oauthReq = await request(this.httpOptions).catch(
                (err: any) => {
                    this.handleErrors(err);
                }
            );

            this.connector.credentials.token =
                oauthReq[this.connector.credentials.tokenKey];

            // Bearer
            this.httpOptions.headers = {
                Authorization: `Bearer ${this.connector.credentials.token}`
            };
            // reset back to the default options
            this.httpOptions.method = this.defaultHttpOptions.method;
            delete this.httpOptions.form;
        }
        return this.httpOptions;
    }

    private setupEndpoint(endpoint: string = null) {
        endpoint = endpoint || this.endpoint;
        console.log(endpoint);

        if (this.params && this.params !== {}) {
            for (const key of Object.keys(this.params)) {
                endpoint = endpoint.replace(
                    new RegExp(`:${key}`),
                    this.params[key] || ''
                );
            }
        }

        return endpoint;
    }

    // @DataResolver()
    public async fetchData(endpoint: string = null) {
        const url = `${this.connector.path}${this.setupEndpoint(endpoint)}`;
        const options = this.httpOptions || (await this.setupHttpOptions());

        options.uri = url;

        return this.saveData(options);
    }

    private compareKeys(a: any, b: any): boolean {
        const aProps = Object.keys(a),
            bProps = Object.keys(b);
        if (aProps.length != bProps.length) {
            return false;
        }

        for (let i = 0; i < aProps.length; i++) {
            var propName = aProps[i];
            if (a[propName] !== b[propName]) {
                return false;
            }
        }
        return true;
    }

    public async paginationData(options: any) {
        return request(options).catch((err: any) => {
            this.handleErrors(err);
        });
    }

    public setModelManyKey(data: any) {
        let _data = {};
        if (Array.isArray(data)) {
            _data[this.modelManyKey] = data;
            return _data;
        }
        if (!data.hasOwnProperty(this.modelManyKey)) {
            _data[this.modelManyKey] = [data];
            data = _data;
        }
        return data;
    }

    public async saveData(options: any) {
        this.data = await request(options)
            .then(async (data: any) => {
                // ensure many model type
                data = this.setModelManyKey(data);
                let results = [];
                results = [].concat(...data[this.modelManyKey]);

                // console.log(options.uri);
                let pagination = false;

                if (
                    this.modelPagination &&
                    (data[this.modelPagination.key] ||
                        data[this.modelPagination.page])
                ) {
                    pagination = true;
                }

                if (pagination) {
                    let _data: any;
                    let _dataCopy = JSON.parse(JSON.stringify(data));
                    while (pagination) {
                        if (this.modelPagination.isSubObject) {
                            options.uri = `${this.connector.path}${
                                _dataCopy[this.modelPagination.key][
                                    this.modelPagination.page
                                ]
                            }`;
                            _data = await this.paginationData(options);

                            if (
                                !_data.hasOwnProperty(this.modelPagination.key)
                            ) {
                                pagination = false;
                            } else {
                                options.uri = `${this.connector.path}${
                                    _data[this.modelPagination.key][
                                        this.modelPagination.page
                                    ]
                                }`;
                            }
                        } else {
                            options.uri = `${this.connector.path}${
                                _dataCopy[this.modelPagination.page]
                            }`;
                            _data = await this.paginationData(options);
                            if (
                                !_data.hasOwnProperty(this.modelPagination.page)
                            ) {
                                pagination = false;
                            } else {
                                options.uri = `${this.connector.path}${
                                    _data[this.modelPagination.page]
                                }`;
                            }
                        }

                        _data = this.setModelManyKey(_data);
                        _dataCopy = JSON.parse(JSON.stringify(_data));
                        // console.log(options.uri);
                        results = results.concat(..._data[this.modelManyKey]);
                    }
                }

                return results;
            })
            .catch((err: any) => {
                this.handleErrors(err);
            });

        this.data = this.setModelManyKey(this.data);

        const _data = [];
        for (let data of this.data[this.modelManyKey]) {
            // let modelData = this.model(data);
            let criteria = {};
            criteria[this.index] = data[this.index];
            let fields = '';

            if (this.modelIncludeFields) {
                for (const key of this.modelIncludeFields) {
                    fields += ` ${key}`;
                }
            }

            if (this.modelExcludeFields) {
                for (const key of this.modelExcludeFields) {
                    fields += ` -${key}`;
                }
            }

            if (this.modelInjectionFields) {
                for (let [key, value] of Object.entries(
                    this.modelInjectionFields
                )) {
                    // console.log('====== INJECTION ======');
                    if (!data.hasOwnProperty(key)) {
                        if (typeof value === 'object') {
                            // console.log(value);
                            let args = (value as any).args || null;
                            if (args) {
                                args = args.map((arg: any) => data[arg]);
                                data[key] = (value as any).fn(...args);
                            } else {
                                data[key] = (value as any).fn();
                            }
                        } else {
                            data[key] = value;
                        }
                    }
                }
            }

            let modelData = await this.model
                .findOne(criteria)
                .select(fields)
                .lean()
                .exec();
            // console.log('='.repeat(80));
            // console.log('criteria');
            // console.log(criteria);
            // console.log('='.repeat(80));
            // console.log('='.repeat(80));
            // console.log('data');
            // console.log(data);
            // console.log('='.repeat(80));
            if (
                this.refresh ||
                !modelData ||
                typeof modelData === undefined ||
                typeof modelData === 'undefined' ||
                !this.compareKeys(data, modelData)
            ) {
                modelData = await this.model
                    .findOneAndUpdate(
                        { externalId: 'DevCon19_TERM' },
                        data,
                        {
                            upsert: true,
                            new: true
                        },
                        console.log
                    )
                    .exec()
                    // .then((err: any, doc: any) => {
                    //     console.log('='.repeat(80));
                    //     console.log('INSIDE EXEC FOR findOneAndUpdate()');
                    //     console.log(doc);
                    //     console.log('='.repeat(80));
                    //     return doc;
                    // })
                    .catch((err: any) => {
                        console.log(err);
                        this.handleErrors(err);
                    });
                // console.log('='.repeat(80));
                // console.log('findOneAndUpdate:modelData');
                // console.log(modelData);
                // console.log('='.repeat(80));
                // console.log('data');
                // console.log(data);
                // console.log('='.repeat(80));
            }
            _data.push(modelData);
        }
        // console.log('='.repeat(80));
        // console.log(_data);
        // console.log('='.repeat(80));

        this.data = _data;
        // console.log('='.repeat(80));
        // console.log(this.data);
        // console.log('='.repeat(80));
        return this.data;
    }

    // TODO: Create a method to send new data to the third party system
    public async createData(data: any) {
        // console.log(`

        //     BEFORE URL UPDATE
        // `);
        // console.log(`${this.connector.path}${this.setupEndpoint()}`);
        const options = this.httpOptions || (await this.setupHttpOptions());
        const url = `${this.connector.path}${this.setupEndpoint()}`;
        // console.log(url);
        options.uri = url;
        options.method = this.crudOps.create;
        options.body = data;
        // console.log(options);
        // console.log(data);
        // console.log(`
        //
        // AFTER URL UPDATE
        // `);

        return this.saveData(options);
    }
    // TODO: Create a method to update data in the third party system
    public async updateData(id: string, data: any) {
        const options = this.httpOptions || (await this.setupHttpOptions());
        const url = `${this.connector.path}${this.setupEndpoint()}`;
        options.uri = url;
        options.method = this.crudOps.update;
        options.body = data;
        // console.log(options);
        // console.log(id);
        // console.log(data);
        return this.saveData(options);
    }
    // TODO: Create a method to remove data from the third party system
    public async removeData(id: string) {
        const options = this.httpOptions || (await this.setupHttpOptions());
        const url = `${this.connector.path}${this.setupEndpoint()}`;
        options.uri = url;
        options.method = this.crudOps.delete;
        options.body = null;
        // console.log(options);
        // console.log(id);

        const req = await request(options);
        // console.log(req);

        let criteria: any = { id };

        if (/:/i.test(id)) {
            let parts = id.split(':');
            criteria = {};
            criteria[parts[0]] = parts[1];
        }

        await this.model
            .findOneAndRemove(criteria)
            .lean()
            .exec();

        return `${id} has been deleted.`;
    }

    private handleErrors(err: any) {
        // console.log(this.config);
        console.log(err.error);
        console.log(err.options);
        for (const eh of this.errorHandlers) {
            eh.runHandler();
        }
    }
}
