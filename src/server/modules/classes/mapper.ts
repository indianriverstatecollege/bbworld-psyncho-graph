import { ErrorHandler } from '../services/error.service';
import { enumMode, IParam } from '../types/integration.types';
import { IMapper } from '../types/mapper.types';
import { Integration } from './integration';
import { Transformer } from './transformer';

/**
 * Mapper "Psyncho Mapper" is the main class object in which
 * uses the integrations to connect to third party systems
 * and fetch data and convert the payloads into a standard
 * format. This format can be used to transform the data to
 * other systems acting as middleware concept.
 *
 * @export
 * @class Mapper
 */
export class Mapper {
    /**
     * A list of integration class objects.
     *
     * @type {Integration[]}
     * @memberof Mapper
     */
    public intergrations: Integration[];
    /**
     * A list of transformations that will applied to the incoming
     * data properties.
     *
     * @type {Transformer[]}
     * @memberof Mapper
     */
    public tranformations: Transformer[];
    /**
     * A list of error handler class objects.
     *
     * @type {ErrorHandler[]}
     * @memberof Mapper
     */
    public errorHandlers: ErrorHandler[];
    /**
     * Describes the state of the Mapper Object.
     *
     * @type {enumMode}
     * @memberof Mapper
     */
    public mode: enumMode;
    /**
     * A unique string identifier to use as database collection prefix
     * for incoming/outgoing payloads.
     *
     * @type {string}
     * @memberof Mapper
     */
    public prefix: string;
    /**
     * A list of parameter objects that will be used by the integration
     * and connections instances.
     *
     * @type {IParam[]}
     * @memberof Mapper
     */
    public parameters: IParam[];
    /**
     * Determines is the Mapper is requied for future sequenced Mappers.
     * Essentially: if required, halt and submit error, else continue.
     *
     * @type {boolean}
     * @memberof Mapper
     */
    public required: boolean;

    /**
     * Psyncho Data is the internal standardized meta data mapping object
     * to be used to help transform the data from one system schema to
     * another.
     *
     * @type {*}
     * @memberof Mapper
     */
    public psynchoData: any;

    /**
     * Creates an instance of Mapper.
     * @param {IMapper} config
     * @memberof Mapper
     */
    constructor(private config: IMapper) {
        this.intergrations = config.integrations;
        this.tranformations = config.transformations;
        this.errorHandlers = config.errorHandlers;
        this.mode = config.mode;
        this.prefix = config.prefix;
        this.parameters = config.parameters;
        this.required = config.required;
    }

    /**
     * runIntegrations iterates over each integration and connects to
     * the third party system and retrieves the payload(s) then caches
     * the data into the meta 'zero copy' psynchoData object.
     *
     * @returns {*}
     * @memberof Mapper
     */
    public runIntegrations(): any {
        switch (this.mode) {
            case enumMode.test:
                console.log('runIntegrations completed for test');
                return {};
            case enumMode.active:
                console.log('runIntegrations completed for active');
                return {};
            case enumMode.disabled:
            case enumMode.draft:
            case enumMode.deprecated:
                return;
        }
    }

    /**
     * rubTransformations iterates over each tranformer and applies the property
     * transformation and returns an object to be stored into psynchoData.
     *
     * @memberof Mapper
     */
    public runTransformations() {}
}
