import { loggerOptions } from '../../configs/app-logger.config';
import emailOptions from '../../configs/emailer.config';
import { ILog } from '../types/log.types';
import { Log } from './logger';

const iLogConfig: ILog = {
    config: loggerOptions,
    emailConfig: undefined
};

const log = new Log(iLogConfig);

describe('Test suite for Logs', () => {
    it('Should create an instance of the Log class', () => {
        expect(log).toBeInstanceOf(Log);
    });

    it('Should test convenience method: console', () => {
        log.console('Test console');
        log.console = jest.fn();
        log.console('Test console');
        expect(log.console).toHaveBeenCalledTimes(1);
        expect(log.console).toHaveBeenCalledWith('Test console');
    });

    it('Should test convenience method: error', () => {
        log.error('Test error');
        log.error = jest.fn();
        log.error('Test error');
        expect(log.error).toHaveBeenCalledTimes(1);
        expect(log.error).toHaveBeenCalledWith('Test error');
    });

    it('Should test convenience method: warn', () => {
        log.warn('Test warn');
        log.warn = jest.fn();
        log.warn('Test warn');
        expect(log.warn).toHaveBeenCalledTimes(1);
        expect(log.warn).toHaveBeenCalledWith('Test warn');
    });

    it('Should test convenience method: info', () => {
        log.info('Test info');
        log.info = jest.fn();
        log.info('Test info');
        expect(log.info).toHaveBeenCalledTimes(1);
        expect(log.info).toHaveBeenCalledWith('Test info');
    });

    it('Should test convenience method: debug', () => {
        log.debug('Test debug');
        log.debug = jest.fn();
        log.debug('Test debug');
        expect(log.debug).toHaveBeenCalledTimes(1);
        expect(log.debug).toHaveBeenCalledWith('Test debug');
    });

    it('Should test convenience method: http', () => {
        log.http('Test http');
        log.http = jest.fn();
        log.http('Test http');
        expect(log.http).toHaveBeenCalledTimes(1);
        expect(log.http).toHaveBeenCalledWith('Test http');
    });

    it('Should test convenience method: verbose', () => {
        log.verbose('Test verbose');
        log.verbose = jest.fn();
        log.verbose('Test verbose');
        expect(log.verbose).toHaveBeenCalledTimes(1);
        expect(log.verbose).toHaveBeenCalledWith('Test verbose');
    });

    it('Should test convenience method: silly', () => {
        log.silly('Test silly');
        log.silly = jest.fn();
        log.silly('Test silly');
        expect(log.silly).toHaveBeenCalledTimes(1);
        expect(log.silly).toHaveBeenCalledWith('Test silly');
    });

    it('Should test convenience method: email', () => {
        const iLogConfig: ILog = {
            config: loggerOptions,
            emailConfig: emailOptions
        };

        const log = new Log(iLogConfig);
        log.email('Test email');
        log.email = jest.fn();
        log.email('Test email');
        expect(log.email).toHaveBeenCalledTimes(1);
        expect(log.email).toHaveBeenCalledWith('Test email');
    });
});
