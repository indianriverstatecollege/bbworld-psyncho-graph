import * as moment from 'moment';
import { Connector } from './connector';
import { enumErrorAction, enumErrorLevel, IErrorHandler } from '../types/error.types';
import { enumType } from '../types/connector.types';
import { enumMode, IIntegration } from '../types/integration.types';
import { ErrorHandler } from '../services/error.service';
import { enumRunnerMode, IRunner } from '../types/runner.types';
import { IMapper } from '../types/mapper.types';
import { Integration } from './integration';
import { Mapper } from './mapper';
import { Runner } from './runner';

const errorConfig: IErrorHandler = {
    name: 'Test Error',
    description: 'A simple test for unit testing.',
    error: Error('Test Error'),
    service: { name: 'unit', caller: 'test'},
    handler: (err: any) => {
        if (this.action === enumErrorAction.console) {
            console.log(err);
        }
        return err;
    },
    action: enumErrorAction.console,
    level: enumErrorLevel.debug,
};

const integrationConfig: IIntegration = {
    connector: new Connector({
        type: enumType.httpx,
        description: 'This is a test description',
        name: 'Main Connector',
        path: 'https://username:password@google.com',
        prefix: 'main_connector',
    }),
    data: {},
    endpoint: '/workday/report',
    errorHandlers: [new ErrorHandler(errorConfig)],
    mode: enumMode.active,
    model: null,
    collectionName: null,
    params: [{
        student: '1234',
    }],
};

const mapperConfig: IMapper = {
    errorHandlers: [new ErrorHandler(errorConfig)],
    integrations: [new Integration(integrationConfig)],
    mode: enumMode.active,
    parameters: [],
    prefix: 'workday',
    required: true,
};

const runnerConfig: IRunner = {
    iterable: true,
    mapper: new Mapper(mapperConfig),
    mode: enumRunnerMode.ready,
    occurrances: null,
    when: moment.utc(),
    parameters: null,
    repeatable: false,
};

describe('Test Suite for Runner', () => {
    it('Should create a new instance of Runner', () => {
        const runner = new Runner(runnerConfig);
        expect(runner).toBeInstanceOf(Runner);
    });
});
