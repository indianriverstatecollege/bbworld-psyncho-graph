import { enumType } from '../types/connector.types';
import { Connector } from './connector';

describe('Testing the main Connector class', () => {

    it('Should create a new Connector instance', () => {
        const connector: Connector = new Connector({
            type: enumType.httpx,
            description: 'This is a test description',
            name: 'Base Connector',
            path: 'https://username:password@google.com',
            prefix: 'base_connector',
        });

        console.log(connector);

        expect(connector).toBeInstanceOf(Connector);
        expect(connector.name).toBe('Base Connector');
    });
});
