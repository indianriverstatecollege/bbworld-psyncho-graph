import { Moment } from 'moment';
import { Mapper } from './mapper';
import { IParam } from '../types/integration.types';
import { enumRunnerMode, IOccurrance, IRunner } from '../types/runner.types';

/**
 * Runner "task" runs a Psyncho Mapper object based
 * on a configuration.
 *
 * @export
 * @class Runner
 */
export class Runner {

    /**
     * Holds the Mapper object to perform the tasks on.
     *
     * @type {Mapper}
     * @memberof Runner
     */
    public mapper: Mapper;
    /**
     * Whether this runner is to be repeatable or
     * a single run instance.
     *
     * @type {boolean}
     * @memberof Runner
     */
    public repeatable: boolean;
    /**
     * A scheduler if this runner is to be repeated.
     *
     * @type {IOccurrance}
     * @memberof Runner
     */
    public occurrances: IOccurrance;
    /**
     * When the runner is to be performed.
     *
     * @type {Moment}
     * @memberof Runner
     */
    public when: Moment;
    /**
     * Parameters to pass to the integration / connection.
     *
     * @type {IParam[]}
     * @memberof Runner
     */
    public parameters: IParam[];
    /**
     * Whether this runner can be self repeating /
     * stackable to apply or re-apply further processing.
     *
     * @type {boolean}
     * @memberof Runner
     */
    public iterable: boolean;
    /**
     * The state of the runner.
     *
     * @type {enumMode}
     * @memberof Runner
     */
    public mode: enumRunnerMode;

    /**
     * Creates an instance of Runner.
     * @param {IRunner} config
     * @memberof Runner
     */
    constructor(private config: IRunner) {
        this.mapper = config.mapper;
        this.repeatable = config.repeatable;
        this.occurrances = config.occurrances;
        this.when = config.when;
        this.parameters = config.parameters;
        this.iterable = config.iterable;
        this.mode = config.mode;
    }

    /**
     * Runs the actual mapper and monitors for any
     * errors or until it's completed.
     *
     * @memberof Runner
     * @function
     */
    public run() {
        // check mode
        // check when
        // check occurrances
        // does it meet when and if occurrances, those as well
        // check for parameters
        // check to see if iterable: does it allow for skipping?
        // connection via the integration w/ parameters if any
        // if no errors, mark as complete
    }

    /**
     * Uses the integrations connections to connect and retreive
     * data. Integrations saved the payload and returns.
     *
     * @memberof Runner
     */
    public connect() {}

    /**
     * If there any errors, runErrors trigger the integration errorHanlders to run.
     *
     * @memberof Runner
     */
    public runErrors() {}
}
