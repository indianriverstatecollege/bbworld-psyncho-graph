import { writeFileSync } from 'fs';
import { join } from 'path';

export default (gql: any) => {
    // clean up Blackboard Type Defs.
    let inType = false;
    let currentDescription = '';
    const typeDefs = gql
        .split('\n')
        .filter((line: string) => line.trim() != '')
        .map((line: string) => {
            // ensure backticks are being picked up and interpreted
            line = line.trim().replace(/`/gi, '');
            // check for type with a space as some fields maybe called type
            if (/^type\s+/gi.test(line)) {
                inType = true;
                return `${line}\n`;
            } else if (line === '}') {
                inType = false;
                return `${line}\n\n`;
            }

            line = `${inType ? '  ' : ''}${line}\n`;

            // test if the line a normal comment
            // liftoff returns them usually as #
            if (/#/gi.test(line)) {
                // normal single line comment
                // remove the # and store until we know its not
                // apart of a multi-line comment
                currentDescription = `${currentDescription}\n${line.replace(
                    /#/gi,
                    ''
                )}`;
                // store the comment and return nothing
                // we will add this back if the next line isn't
                // a comment for a description field
                return;
            }

            // check if next line is another comment or
            // expansion on the comment | or *
            // malformed comments
            if (/\||\*/gi.test(line)) {
                currentDescription = `${currentDescription}\n${line.replace(
                    /\|/gi,
                    ''
                )}`;
                return;
            }

            // specific Blackboard malformed comments
            if (
                /^\s+?(Publicly|For|Shown|Only|These|Custom|Associated|This|Both|Cannot|Formerly|The) /gi.test(
                    line
                )
            ) {
                currentDescription = `${currentDescription}\n${line}`;
                return;
            }

            // not a comment, add the comment back in, if any
            if (currentDescription != '') {
                currentDescription = `"""${currentDescription.replace(
                    /\n\n/gim,
                    '\n'
                )}"""`;
                line = `${currentDescription}\n${line}`;
                // reset currentDescription
                currentDescription = '';
            }

            return line;
        })
        .join('')
        .replace(/BYDAYEnum/gi, 'String')
        .replace(/aliases: Aliases/gi, 'aliases: JSON')
        .replace(
            /customParameters: CustomParameters/gi,
            'customParameters: JSON'
        )
        .replace(
            /assessmentSettingsCustomParameters: AssessmentSettingsCustomParameters/gi,
            'assessmentSettingsCustomParameters: JSON'
        )
        .replace(/SystemRoleEnum/gi, 'String');

    writeFileSync(
        join(
            __dirname,
            '../',
            'graphql',
            'schemas',
            'blackboard',
            'typeDefs.gql'
        ),
        typeDefs
    );
};
