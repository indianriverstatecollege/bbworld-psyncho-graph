import { Integration } from './classes/integration';
import {
    BbDataSourcesIntConfig,
    BbTermsIntConfig,
    BbUsersIntConfig,
    BbCoursesIntConfig,
    BbMembershipsIntConfig
} from '../configs/blackboard/blackboard.config';

const bbDSIntegration: Integration = new Integration(BbDataSourcesIntConfig);
const bbUserIntegration: Integration = new Integration(BbUsersIntConfig);
const bbCourseIntegration: Integration = new Integration(BbCoursesIntConfig);
const bbTermIntegration: Integration = new Integration(BbTermsIntConfig);
const bbMembershipIntegration: Integration = new Integration(
    BbMembershipsIntConfig
);

export async function getBbDataSource(dataSourceId: string, refresh: boolean) {
    bbDSIntegration.params = { dataSourceId };
    bbDSIntegration.refresh = refresh;

    return bbDSIntegration.fetchData();
}

export async function getAllBbDataSources(refresh: boolean) {
    bbDSIntegration.params = { dataSourceId: '' };
    bbDSIntegration.refresh = refresh;

    return bbDSIntegration.fetchData();
}

export async function createBbDataSource(dataSource: any) {
    bbDSIntegration.params = { dataSourceId: '' };
    if (/:/i.test(dataSource.externalId)) {
        const parts = dataSource.externalId.split(':');
        dataSource.externalId = parts[1];
    }
    return bbDSIntegration.createData(dataSource);
}

export async function updateBbDataSource(
    dataSourceId: string,
    dataSource: any
) {
    bbDSIntegration.params = { dataSourceId };

    if (/:/i.test(dataSource.externalId)) {
        const parts = dataSource.externalId.split(':');
        dataSource.externalId = parts[1];
    }
    return bbDSIntegration.updateData(dataSourceId, dataSource);
}

export async function deleteBbDataSource(dataSourceId: any) {
    bbDSIntegration.params = { dataSourceId };
    return bbDSIntegration.removeData(dataSourceId);
}

export async function getBbTerm(termId: string, refresh: boolean) {
    bbTermIntegration.params = { termId };
    bbTermIntegration.refresh = refresh;

    return bbTermIntegration.fetchData();
}

export async function getAllBbTerms(refresh: boolean) {
    // cleanEndpoint(bbTermIntegration, 'terms', 'termId');
    bbTermIntegration.params = { termId: '' };

    bbTermIntegration.refresh = refresh;

    return bbTermIntegration.fetchData();
}

export async function createBbTerm(term: any) {
    bbTermIntegration.params = { termId: '' };
    if (/:/i.test(term.externalId)) {
        const parts = term.externalId.split(':');
        term.externalId = parts[1];
    }
    return bbTermIntegration.createData(term);
}

export async function updateBbTerm(termId: string, term: any) {
    bbTermIntegration.params = { termId };
    if (/:/i.test(term.externalId)) {
        const parts = term.externalId.split(':');
        term.externalId = parts[1];
    }
    return bbTermIntegration.updateData(termId, term);
}

export async function deleteBbTerm(termId: any) {
    bbTermIntegration.params = { termId };
    return bbTermIntegration.removeData(termId);
}

export async function getBbUser(userId: string, refresh: boolean) {
    bbUserIntegration.params = { userId };
    bbUserIntegration.refresh = refresh;

    return bbUserIntegration.fetchData();
}

export async function getAllBbUsers(refresh: boolean) {
    bbUserIntegration.params = { userId: '' };

    bbUserIntegration.refresh = refresh;

    return bbUserIntegration.fetchData();
}

export async function createBbUser(user: any) {
    bbUserIntegration.params = { userId: '' };

    if (/:/i.test(user.externalId)) {
        const parts = user.externalId.split(':');
        user.externalId = parts[1];
    }
    return bbUserIntegration.createData(user);
}

export async function updateBbUser(userId: string, user: any) {
    bbUserIntegration.params = { userId };

    if (/:/i.test(user.externalId)) {
        const parts = user.externalId.split(':');
        user.externalId = parts[1];
    }
    return bbUserIntegration.updateData(userId, user);
}

export async function deleteBbUser(userId: any) {
    bbUserIntegration.params = { userId };
    return bbUserIntegration.removeData(userId);
}

export async function getBbCourse(courseId: string, refresh: boolean) {
    bbCourseIntegration.params = { courseId };
    bbCourseIntegration.refresh = refresh;

    return bbCourseIntegration.fetchData();
}

export async function getAllBbCourses(refresh: boolean) {
    bbCourseIntegration.params = { courseId: '' };
    bbCourseIntegration.refresh = refresh;

    return bbCourseIntegration.fetchData();
}

export async function createBbCourse(course: any) {
    bbCourseIntegration.params = { courseId: '' };
    if (/:/i.test(course.externalId)) {
        const parts = course.externalId.split(':');
        course.externalId = parts[1];
    }
    return bbCourseIntegration.createData(course);
}

export async function updateBbCourse(courseId: string, course: any) {
    bbCourseIntegration.params = { courseId };
    if (/:/i.test(course.externalId)) {
        const parts = course.externalId.split(':');
        course.externalId = parts[1];
    }
    return bbCourseIntegration.updateData(courseId, course);
}

export async function deleteBbCourse(courseId: any) {
    bbCourseIntegration.params = { courseId };
    return bbCourseIntegration.removeData(courseId);
}

export async function getBbMembership(
    courseId: string,
    userId: string,
    refresh: boolean
) {
    bbMembershipIntegration.params = { courseId, userId };
    bbMembershipIntegration.refresh = refresh;

    return bbMembershipIntegration.fetchData();
}

export async function getAllBbMemberships(courseId: string, refresh: boolean) {
    bbMembershipIntegration.params = { courseId, userId: '' };
    bbMembershipIntegration.refresh = refresh;

    return bbMembershipIntegration.fetchData();
}

export async function createBbMembership(
    courseId: string,
    userId: string,
    membership: any
) {
    bbMembershipIntegration.params = { courseId, userId };
    if (/:/i.test(membership.courseId)) {
        const parts = membership.courseId.split(':');
        membership.courseId = parts[1];
    }
    return bbMembershipIntegration.createData(membership);
}

export async function updateBbMembership(
    courseId: string,
    userId: string,
    membership: any
) {
    bbMembershipIntegration.params = { courseId, userId };
    if (/:/i.test(membership.courseId)) {
        const parts = membership.courseId.split(':');
        membership.courseId = parts[1];
    }
    return bbMembershipIntegration.updateData(courseId, membership);
}

export async function deleteBbMembership(courseId: string, userId: string) {
    bbMembershipIntegration.params = { courseId, userId };
    return bbMembershipIntegration.removeData(courseId);
}
