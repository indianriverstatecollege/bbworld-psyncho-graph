import { enumErrorAction, enumErrorLevel, IErrorHandler } from '../types/error.types';
import { PsynchoEmitter } from './emitter.service';
import { ErrorHandler } from './error.service';

const testEmitter = new PsynchoEmitter();
testEmitter.on('psyncho-error', console.log);

const errorConfig: IErrorHandler = {
    name: 'Test Error',
    description: 'A simple test for unit testing.',
    error: Error('Test Error'),
    service: { name: 'unit', caller: 'test'},
    handler: (err: any) => {
        if (this.action === enumErrorAction.console) {
            console.log(err);
        }
        return err;
    },
    action: enumErrorAction.console,
    level: enumErrorLevel.debug,
    emitter: testEmitter,
};

describe('Test Suite for ErrorHandler', () => {
    it('Should create and instance of ErrorHandler', () => {
        const errorHandler = new ErrorHandler(errorConfig);
        expect(errorHandler).toBeInstanceOf(ErrorHandler);
        expect(errorHandler.service).toMatchObject({ name: 'unit', caller: 'test'});
    });

    it('Should set the logPath', () => {
        const eh: IErrorHandler = {...errorConfig};
        eh.action = enumErrorAction.systemLog;
        eh.logPath = './error.log';
        const errorHandler = new ErrorHandler(eh);
        expect(errorHandler).toBeInstanceOf(ErrorHandler);
        expect(errorHandler.service).toMatchObject({ name: 'unit', caller: 'test'});
        expect(errorHandler.logPath).toBe('./error.log');
    });

    it('Should set the logPath to default', () => {
        const eh: IErrorHandler = {...errorConfig};
        eh.action = enumErrorAction.systemLog;
        const errorHandler = new ErrorHandler(eh);
        expect(errorHandler).toBeInstanceOf(ErrorHandler);
        expect(errorHandler.service).toMatchObject({ name: 'unit', caller: 'test'});
    });

    it('Should set the action to email', () => {
        const eh: IErrorHandler = {...errorConfig};
        eh.action = enumErrorAction.email;
        const errorHandler = new ErrorHandler(eh);
        expect(errorHandler).toBeInstanceOf(ErrorHandler);
        expect(errorHandler.service).toMatchObject({ name: 'unit', caller: 'test'});
    });

    it('Should fire and emission to the default emitter event handler', async () => {
        const eh: IErrorHandler = {...errorConfig};
        delete eh.emitter;
        eh.action = enumErrorAction.console;
        jest.mock('./error.service.ts');
        const errorHandler = new ErrorHandler(eh);
        errorHandler.emitter.emit('psyncho-error', 'hello');
        // we only need to know if the emit function is truthy because the output
        // should be enough for this use case, most of the time, a custom
        // error handler with be supplied.
        expect(errorHandler.emitter.emit).toBeTruthy();
    });
});
