import { bootstrap } from '../../bootstrap';
import { PsynchoEmitter } from './emitter.service';

describe('Test Suite for for Psyncho Event Emitter', () => {
    it('Create a new instance of PsynchoEmitter', () => {
        const [emitter, release] = bootstrap<PsynchoEmitter>(PsynchoEmitter);
        console.log(emitter);
        emitter.on('psyncho-error', console.log);
        emitter.emit('psyncho-error', 'Test from PsynchoEmitter');
        expect(emitter).toBeInstanceOf(PsynchoEmitter);
        release();
    });

    it('Should create and listen to a custom event', () => {
        const eMock = jest.mock('./emitter.service.ts');
        const emit = eMock.fn().mockName('emit');
        const on = eMock.fn().mockName('on').mockReturnValue('Psyncho Error!');
        emit('psyncho-error');
        on();
        expect(emit).toBeCalledWith('psyncho-error');
        expect(on).toReturnWith('Psyncho Error!');
    });
});
