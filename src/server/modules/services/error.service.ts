import { format, LoggerOptions, transports } from 'winston';
import { Service } from '../decorators/service.decorator';
import { Log } from '../classes/logger';
import {
    enumErrorAction,
    enumErrorLevel,
    errorLevelMapping,
    IErrorHandler,
    IErrorService,
} from '../types/error.types';
import { PsynchoEmitter } from './emitter.service';

/**
 * Error Handler is used to inform the adminstrator(s) issues that occur.
 *
 * @export
 * @class ErrorHandler
 */
@Service()
export class ErrorHandler {

    /**
     * The friendly name of the error.
     *
     * @type {string}
     * @memberof ErrorHandler
     */
    public name: string;
    /**
     * Describes the error handler object.
     *
     * @type {string}
     * @memberof ErrorHandler
     */
    public description: string;
    /**
     * The actual error that is being passed into the ErrorHandler class.
     *
     * @type {*}
     * @memberof ErrorHandler
     */
    public error: any;
    /**
     * Service Identifer from caller.
     *
     * @type {IErrorService}
     * @memberof ErrorHandler
     */
    public service: IErrorService;
    /**
     * The actual closure function that will be ran.
     *
     * @type {*}
     * @memberof ErrorHandler
     */
    public handler: any;
    /**
     * Describes that state of what type of Error the class is.
     *
     * @type {enumErrorAction}
     * @memberof ErrorHandler
     */
    public action: enumErrorAction;
    /**
     * The path to where the error output should be.
     *
     * @type {string}
     * @memberof ErrorHandler
     */
    public logPath: string;
    /**
     * Desribces the level of severity of the error.
     *
     * @type {enumErrorLevel}
     * @memberof ErrorHandler
     */
    public level: enumErrorLevel;

    /**
     * The event listener that will have error events. If a custom event emitter
     * is not set in the config, then a default one is created.
     *
     * @public
     * @type {PsynchoEmitter}
     * @memberof ErrorHandler
     */
    public emitter: PsynchoEmitter;

    /**
     * Creates an instance of ErrorHandler.
     * @param {IErrorHandler} config
     * @memberof ErrorHandler
     */
    constructor(private config: IErrorHandler) {
        this.name = config.name;
        this.description = config.description;
        this.error = config.error;
        this.service = config.service;
        this.handler = config.handler;
        this.action = config.action;
        this.level = config.level;

        if (config.logPath) {
            this.logPath = config.logPath;
        }

        this.emitter = config.emitter || new PsynchoEmitter();

        // create on listener if config.emitter is undefined
        if (!config.emitter) {
            this.emitter.on('psyncho-error', this.handleEvent);
        }
        this.runHandler();
    }

    /**
     * Runs the actual handler that was injected.
     * @function
     * @memberof ErrorHandler
     */
    public runHandler(): void {
        let loggerOptions: LoggerOptions;
        let emailOptions: any;

        switch (this.action) {
            case enumErrorAction.console:
                loggerOptions = {
                    level: errorLevelMapping[this.level],
                    format: format.json(),
                    defaultMeta: { service: `${this.service.name}-${this.service.caller}` },
                    transports: [
                        new transports.Console(),
                    ],
                };
                break;

            case enumErrorAction.systemLog:
                loggerOptions = {
                    level: errorLevelMapping[this.level],
                    format: format.json(),
                    defaultMeta: { service: `${this.service.name}-${this.service.caller}` },
                    transports: [
                        new transports.File({
                            filename: this.logPath || 'src/server/express/logs/error.log',
                            level: errorLevelMapping[this.level],
                            handleExceptions: true,
                            maxsize: 5242880, // 5MB
                            maxFiles: 5,
                        }),
                    ],
                };
                break;

            case enumErrorAction.email:
                emailOptions = {
                    host: 'smtp.address.edu',
                    to: ['email@address.edu'],
                };

                loggerOptions = {
                    level: errorLevelMapping[this.level],
                    format: format.json(),
                    defaultMeta: { service: `${this.service.name}-${this.service.caller}` },
                };
                break;
        }

        // tslint:disable-next-line:no-unused-expression
        new Log({
            config: loggerOptions,
            emailConfig: emailOptions,
        });
    }

    /**
     * A internal logging error handler for events
     *
     * @private
     * @param {*} event
     * @memberof ErrorHandler
     */
    /* istanbul ignore next */
    private handleEvent(event: any) {
        /* istanbul ignore next */
        console.log('should fire');
        /* istanbul ignore next */
        console.log(event);
    }
}
