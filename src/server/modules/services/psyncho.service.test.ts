import { bootstrap } from '../../bootstrap';
import { PsynchoService } from './psyncho.service';

describe('Test Suite for PsynchoService', () => {
    it('Should create a new instance of PsynchoService', () => {
        const [psynchoService, release] = bootstrap<PsynchoService>(PsynchoService);
        psynchoService.psynchoEmitter.emit('psyncho-error', 'hello from psyncho service emitter');
        psynchoService.test();
        expect(psynchoService).toBeInstanceOf(PsynchoService);
        release();
    });
});
