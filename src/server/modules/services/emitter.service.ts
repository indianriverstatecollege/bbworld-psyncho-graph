import { EventEmitter } from 'events';
import { Service } from '../decorators/service.decorator';

/**
 * A custom class extension of Nodes EventEmitter class.
 * Allow for ease of use for dependency injection for
 * custom events.
 *
 * @export
 * @class PsynchoEmitter
 * @extends {EventEmitter}
 */
@Service()
export class PsynchoEmitter extends EventEmitter {}
