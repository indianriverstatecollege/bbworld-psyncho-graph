import { Service } from '../decorators/service.decorator';
import { PsynchoEmitter } from './emitter.service';
import { ErrorHandler } from './error.service';

@Service()
export class PsynchoService {
    constructor(
        private errorHandler: ErrorHandler,
        public psynchoEmitter: PsynchoEmitter,
    ) {

    }

    public test() {
        this.errorHandler.runHandler();
    }
}
