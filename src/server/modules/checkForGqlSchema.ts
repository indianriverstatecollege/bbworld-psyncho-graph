import { genSchemaFromSwagger } from './generators/generateSchema';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
export default (exists: any) => {
    if (!exists) {
        return genSchemaFromSwagger(
            'https://developer.blackboard.com/portal/docs/apis/learn-swagger.json',
            'Bb'
        ).then((gql: any) => {
            writeFileSync(
                join(
                    __dirname,
                    '../',
                    'graphql',
                    'schemas',
                    'blackboard',
                    'typeDefs.raw'
                ),
                gql
            );
            return gql;
        });
    }
    const data = readFileSync(
        join(__dirname, '../graphql/schemas/blackboard/typeDefs.raw'),
        'utf8'
    );

    const gql = data.toString();
    return gql;
};
