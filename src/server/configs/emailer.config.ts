export default {
    from: 'Psyncho <noreply@address.edu>',
    host: 'your.smtp.url',
    port: 25,
    secure: false,
    to: 'email@address.edu',
};
