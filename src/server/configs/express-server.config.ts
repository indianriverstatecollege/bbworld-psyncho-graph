import * as bodyParser from 'body-parser';
import * as methodOverride from 'method-override';
import * as morgan from 'morgan';
import { IServerConfig } from '../modules/types/server.types';
import mainRoutes from './routes.config';
import { InjectionToken } from '../modules/classes/container';

export const AuthConfig = {
    key: 'key',
    secret: 'secret',
    auth: ''
};
AuthConfig.auth = Buffer.from(
    AuthConfig.key + ':' + AuthConfig.secret
).toString('base64');

export const AppConfig = {
    // refresh data every 15 minutes from the backend, and 5 minutes from the frontend
    // intRefreshRate: 15 * (60 * 1000),
    intRefreshRate: 1000,
    uiRefreshRate: 5 * (60 * 1000)
};

// Mongo DB Import
// mongoimport --db wd-2-bb --collection users --file ~/Downloads/BBLEARN_USERS_EXPORT_201808241642.json --jsonArray

export const testServerConfig: IServerConfig = {
    middlewares: [
        morgan('dev'),
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true }),
        methodOverride((req: any) => {
            if (
                req.body &&
                typeof req.body === 'object' &&
                '_method' in req.body
            ) {
                // look in urlencoded POST bodies and delete it
                const method = req.body._method;
                delete req.body._method;
                return method;
            }
        })
    ],
    port: 3000,
    routers: mainRoutes,
    sets: [{ key: 'x-powered-by', value: false }],
    enables: ['trust proxy'],
    locals: [],
    staticPaths: [],
    graphQLHttpOptions: {
        // schema: PsynchoSchema,
        schema: null,
        rootValue: null,
        graphiql: true,
        cacheControl: true,
        context: {}
    }
};
export const serverToken = new InjectionToken('server-configuration');
