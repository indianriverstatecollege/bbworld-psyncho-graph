import { Route } from '../modules/classes/route';
import helloHandlers from './workday/routes/hello.handlers';

export default [
    new Route({
        path: '/api',
        middlewares: [
            (req: any, res: any, next: any) => {
                console.log('API Middleware!!');
                next();
            }
        ],
        handlers: {
            get: (req: any, res: any) => {
                res.status(200).json({ msg: 'Hello from api route' });
            }
        },
        childRoutes: [
            {
                path: '/queue',
                handlers: {
                    get: (req: any, res: any) => {
                        res.status(200).json({ msg: 'Hello from queue route' });
                    }
                },
                childRoutes: [
                    {
                        path: '/running',
                        handlers: {
                            get: (req: any, res: any) => {
                                res.status(200).json({
                                    msg: 'Hello from queue/running route'
                                });
                            }
                        }
                    },
                    {
                        path: '/status',
                        handlers: {
                            get: (req: any, res: any) => {
                                res.status(200).json({
                                    msg: 'Hello from queue/status route'
                                });
                            }
                        },
                        childRoutes: [
                            {
                                path: '/:id',
                                middlewares: [
                                    (req: any, res: any, next: any) => {
                                        console.log(`Id: ${req.params.id++}`);
                                        next();
                                    }
                                ],
                                handlers: {
                                    get: (req: any, res: any) => {
                                        res.status(200).json({
                                            id: req.params.id
                                        });
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }),
    new Route({
        path: '/workday',
        handlers: helloHandlers
    })
];
