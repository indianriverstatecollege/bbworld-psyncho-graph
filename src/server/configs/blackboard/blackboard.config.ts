import { Connector } from '../../modules/classes/connector';
import { BbStagingConnConfig } from './connectors/staging.connector';

export { BbStagingConnConfig } from './connectors/staging.connector';
export const BbConnection = new Connector(BbStagingConnConfig);

export {
    BbAnnouncementsIntConfig
} from './integrations/announcements.integration';

export {
    BbDataSourcesIntConfig
} from './integrations/data-sources.integration';

export { BbTermsIntConfig } from './integrations/terms.integration';
export { BbUsersIntConfig } from './integrations/users.integration';
export { BbCoursesIntConfig } from './integrations/courses.integration';
export { BbMembershipsIntConfig } from './integrations/memberships.integration';
