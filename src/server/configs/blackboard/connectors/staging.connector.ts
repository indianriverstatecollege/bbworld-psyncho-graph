import { IConnector, enumType } from '../../../modules/types/connector.types';

export const BbStagingConnConfig: IConnector = {
    name: 'Blackboard Staging REST API Connector',
    description: 'Connection to communicate with Bb Learn APIs',
    notes: 'This is used to set/get data in/out of Bb Learn',
    path: 'https://your.bb.url',
    oauthPath: '/learn/api/public/v1/oauth2/token',
    credentials: {
        auth: '',
        token: null,
        grantType: 'client_credentials',
        key: 'your-bb-key',
        secret: 'your-bb-secret',
        tokenKey: 'access_token'
    },
    type: enumType.oauth,
    prefix: 'bb_staging'
};
BbStagingConnConfig.credentials.auth = Buffer.from(
    `${BbStagingConnConfig.credentials.key}:${
        BbStagingConnConfig.credentials.secret
    }`
).toString('base64');
