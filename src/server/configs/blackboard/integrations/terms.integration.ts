import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';
import { Terms } from '../../../database/models/blackboard/terms';
import { BbConnection } from '../blackboard.config';

export const BbTermsIntConfig: IIntegration = {
    index: 'externalId',
    connector: BbConnection,
    collectionName: 'terms',
    endpoint: '/learn/api/public/v1/terms/:termId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelPagination: {
        key: 'paging',
        page: 'nextPage',
        isSubObject: true,
        limit: 100
    },
    modelManyKey: 'results',
    modelInjectionFields: {
        description: ''
    },
    modelIncludeFields: [],
    modelExcludeFields: ['_id', '__v', 'createdAt', 'updatedAt'],
    params: {
        termId: ''
    },
    defaultHttpOptions: {
        method: 'get',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        json: true
    },
    crudOps: {
        create: 'post',
        delete: 'delete',
        update: 'patch'
    }
};

BbTermsIntConfig.model = new Terms().setModelForClass(Terms, {
    schemaOptions: {
        collection: `${BbTermsIntConfig.connector.prefix}_${
            BbTermsIntConfig.collectionName
        }`,
        timestamps: true,
        strict: false
    }
});
