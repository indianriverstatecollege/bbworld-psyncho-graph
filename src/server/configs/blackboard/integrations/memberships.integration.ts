import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';
import { BbMemberships } from '../../../database/models/blackboard/membership';
import { BbConnection } from '../blackboard.config';

export const BbMembershipsIntConfig: IIntegration = {
    index: 'externalId',
    connector: BbConnection,
    collectionName: 'memberships',
    endpoint: '/learn/api/public/v1/courses/:courseId/users/:userId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelPagination: {
        key: 'paging',
        page: 'nextPage',
        isSubObject: true,
        limit: 100
    },
    modelManyKey: 'results',
    modelInjectionFields: {},
    modelIncludeFields: [],
    modelExcludeFields: ['_id', '__v', 'createdAt', 'updatedAt'],
    params: {
        courseId: '',
        userId: ''
    },
    defaultHttpOptions: {
        method: 'get',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        json: true
    },
    crudOps: {
        create: 'put',
        delete: 'delete',
        update: 'patch'
    }
};

BbMembershipsIntConfig.model = new BbMemberships().setModelForClass(
    BbMemberships,
    {
        schemaOptions: {
            collection: `${BbMembershipsIntConfig.connector.prefix}_${
                BbMembershipsIntConfig.collectionName
            }`,
            timestamps: true,
            strict: false
        }
    }
);
