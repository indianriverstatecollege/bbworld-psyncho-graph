import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';

import { Announcements } from '../../../database/models/blackboard/announcements';
import { BbConnection } from '../blackboard.config';
export const BbAnnouncementsIntConfig: IIntegration = {
    connector: BbConnection,
    collectionName: 'announcements',
    endpoint: '/announcements/:announcementId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'results',
    params: {
        announcementId: ''
    },
    defaultHttpOptions: {
        method: 'get',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        json: true
    }
};

BbAnnouncementsIntConfig.model = new Announcements().setModelForClass(
    Announcements,
    {
        schemaOptions: {
            collection: `${BbAnnouncementsIntConfig.connector.prefix}_${
                BbAnnouncementsIntConfig.collectionName
            }`,
            timestamps: true
        }
    }
);
