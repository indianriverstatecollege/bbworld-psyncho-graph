import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';
import { BbCourses } from '../../../database/models/blackboard/courses';
import { BbConnection } from '../blackboard.config';

export const BbCoursesIntConfig: IIntegration = {
    index: 'externalId',
    connector: BbConnection,
    collectionName: 'courses',
    endpoint: '/learn/api/public/v1/courses/:courseId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelPagination: {
        key: 'paging',
        page: 'nextPage',
        isSubObject: true,
        limit: 100
    },
    modelManyKey: 'results',
    modelInjectionFields: {},
    modelIncludeFields: [],
    modelExcludeFields: ['_id', '__v', 'createdAt', 'updatedAt'],
    params: {
        courseId: ''
    },
    defaultHttpOptions: {
        method: 'get',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        json: true
    },
    crudOps: {
        create: 'post',
        delete: 'delete',
        update: 'patch'
    }
};

BbCoursesIntConfig.model = new BbCourses().setModelForClass(BbCourses, {
    schemaOptions: {
        collection: `${BbCoursesIntConfig.connector.prefix}_${
            BbCoursesIntConfig.collectionName
        }`,
        timestamps: true,
        strict: false
    }
});
