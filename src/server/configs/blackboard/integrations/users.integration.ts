import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';
import { BbUsers } from '../../../database/models/blackboard/users';
import { BbConnection } from '../blackboard.config';

export const BbUsersIntConfig: IIntegration = {
    index: 'externalId',
    connector: BbConnection,
    collectionName: 'users',
    endpoint: '/learn/api/public/v1/users/:userId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelPagination: {
        key: 'paging',
        page: 'nextPage',
        isSubObject: true,
        limit: 100
    },
    modelManyKey: 'results',
    modelInjectionFields: {
        description: ''
    },
    modelIncludeFields: [],
    modelExcludeFields: ['_id', '__v', 'createdAt', 'updatedAt'],
    params: {
        userId: ''
    },
    defaultHttpOptions: {
        method: 'get',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        json: true
    },
    crudOps: {
        create: 'post',
        delete: 'delete',
        update: 'patch'
    }
};

BbUsersIntConfig.model = new BbUsers().setModelForClass(BbUsers, {
    schemaOptions: {
        collection: `${BbUsersIntConfig.connector.prefix}_${
            BbUsersIntConfig.collectionName
        }`,
        timestamps: true,
        strict: false
    }
});
