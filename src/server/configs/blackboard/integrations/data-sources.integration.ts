import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';

import { DataSources } from '../../../database/models/blackboard/dataSource';
import { BbConnection } from '../blackboard.config';
export const BbDataSourcesIntConfig: IIntegration = {
    index: 'externalId',
    connector: BbConnection,
    collectionName: 'datasources',
    endpoint: '/learn/api/public/v1/dataSources/:dataSourceId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelPagination: {
        key: 'paging',
        page: 'nextPage',
        isSubObject: true,
        limit: 100
    },
    modelManyKey: 'results',
    modelInjectionFields: {
        description: ''
    },
    modelIncludeFields: [],
    modelExcludeFields: ['_id', '__v', 'createdAt', 'updatedAt'],
    params: {
        dataSourceId: ''
    },
    defaultHttpOptions: {
        method: 'get',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        json: true
    },
    crudOps: {
        create: 'post',
        delete: 'delete',
        update: 'patch'
    }
};

BbDataSourcesIntConfig.model = new DataSources().setModelForClass(DataSources, {
    schemaOptions: {
        collection: `${BbDataSourcesIntConfig.connector.prefix}_${
            BbDataSourcesIntConfig.collectionName
        }`,
        timestamps: true,
        strict: false
    }
});
