import emailerConfig from './emailer.config';
import expressRoutes from './routes.config';
import { buildSchema } from 'graphql';
import PsynchoSchema from '../graphql/psyncho-schema';
import { WorkdayCourseSectionLoader } from '../graphql/schemas/workday/loaders/course-section-loader';
import { WorkdayCourseDefinitionLoader } from '../graphql/schemas/workday/loaders/course-definition-loader';
import { WorkdayEmployeeLoader } from '../graphql/schemas/workday/loaders/user-employee-loader';
import { WorkdayStudentLoader } from '../graphql/schemas/workday/loaders/user-student-loader';
import { BbDataSourcesLoader } from '../graphql/schemas/blackboard/loaders/data-sources-loader';
import { BbTermsLoader } from '../graphql/schemas/blackboard/loaders/terms-loader';
import { BbUsersLoader } from '../graphql/schemas/blackboard/loaders/users-loader';
import { BbCoursesLoader } from '../graphql/schemas/blackboard/loaders/courses-loader';
import { BbMembershipsLoader } from '../graphql/schemas/blackboard/loaders/memberships-loader';
import { BehaviorSubject } from 'rxjs';
export { appLogger, loggerOptions } from './app-logger.config';

export {
    BbAnnouncementsIntConfig,
    BbConnection,
    BbDataSourcesIntConfig,
    BbStagingConnConfig
} from './blackboard/blackboard.config';

export { emailerConfig };

export {
    AppConfig,
    AuthConfig,
    serverToken,
    testServerConfig
} from './express-server.config';

export { expressRoutes };

export {
    WdIntAdvisorReportConfig,
    WdIntCourseInformationConfig,
    WdIntCourseDefinitionConfig,
    WdIntEducationalInstConfig,
    WdIntRegStudentCSConfig,
    WdIntStudentReportConfig,
    WdIntCourseSectionsConfig
} from './workday/workday.config';

export const TERMS = ['20193', '20193A', '20193B'];
export const PORT = process.env.PORT || 4000;
export const GRAPHQL_CONFIG = {
    schema: buildSchema(PsynchoSchema),
    // pre-defined resolverMap or pass null to let it load from code
    rootValue: null,
    endpoint: '/psynchoql',
    apis: [],
    // apis: [
    //     {
    //         url:
    //             'https://developer.blackboard.com/portal/docs/apis/learn-swagger.json',
    //         prefix: 'Bb',
    //         path: join(
    //             __dirname,
    //             './graphql/schemas/blackboard/typeDefs.gql'
    //         )
    //     }
    // ],
    graphiql: true,
    context: {
        WorkdayCourseSectionLoader,
        WorkdayCourseDefinitionLoader,
        WorkdayEmployeeLoader,
        WorkdayStudentLoader,
        BbDataSourcesLoader,
        BbTermsLoader,
        BbUsersLoader,
        BbCoursesLoader,
        BbMembershipsLoader
    }
};

export const APP_CONFIG = {
    port: PORT,
    sets: [{ key: 'x-powered-by', value: false }],
    enables: ['trust proxy'],
    locals: {
        router: new BehaviorSubject(null),
        routes: {},
        blackboard: {
            token: new BehaviorSubject({}),
            tokenTimer: new BehaviorSubject(0)
        }
    }
};

export const ACTIVE_COURSES_REFRESH = 60000;
export const COURSE_DEFINITIONS_REFRESH = 5000;
export const EMPLOYEE_REFRESH = 60000;
export const STUDENT_REFRESH = 60000;
