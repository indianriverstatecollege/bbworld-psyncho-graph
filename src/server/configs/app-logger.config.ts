import stream from 'stream';
import { createLogger, format, transports } from 'winston';
import emailerConfig from './emailer.config';

// tslint:disable-next-line:no-var-requires
const { Mail } = require('winston-mail');

export const loggerOptions = {
    defaultMeta: { service: 'psyncho-service' },
    format: format.json(),
    level: 'info',
    transports: [
        new transports.Console(),
        new transports.File({
            filename: `src/server/express/logs/error.log`,
            handleExceptions: true,
            level: 'error',
            maxFiles: 5,
            maxsize: 5242880, // 5MB
        }),
        new transports.File({
            filename: `src/server/express/logs/combined.log`,
        }),
    ],
};

export const appLogger = createLogger(loggerOptions);

appLogger.stream = (options?: any) => new stream.Duplex({
    write(message: string, encoding: any) {
        appLogger.info(message);
    },
});

appLogger.add(new Mail(emailerConfig));
