import { Route } from '../database/models/psyncho/routes';
import { Token } from '../database/models/blackboard/token';
import * as request from 'request-promise';
import { BbConnection } from './blackboard/blackboard.config';
// import { BbStagingConnConfig } from './blackboard/blackboard.config';

export const RouteModel = new Route().setModelForClass(Route, {
    schemaOptions: {
        collection: 'routes',
        timestamps: true
    }
});

export const TokenModel = new Token().setModelForClass(Token, {
    schemaOptions: {
        capped: 1,
        collection: 'bb_staging_tokens',
        timestamps: true
    }
});

export const ENV = process.env.NODE_ENV || 'development';

export const mongooseConfig: any = {
    // TODO: find a way to create the same replicaSet across systems.
    database: 'mongodb://localhost:27017/psyncho?replicaSet=rs01',
    options: {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    }
};

export const getBbToken = async () => {
    return await request({
        uri: `${BbConnection.path}${BbConnection.oauthPath}`,
        method: 'post',
        headers: {
            Authorization: `Basic ${BbConnection.credentials.auth}`
        },
        form: {
            grant_type: 'client_credentials'
        },
        json: true
    }).catch(err => new Error(err));
};

export const setToken = async (app: any, token: any) => {
    let _token: any;
    // first load the token
    const currentToken = await TokenModel.findOne().exec();

    // compare the new token with the currently saved token
    // if new token is the same update saved
    if (
        currentToken &&
        currentToken.access_token &&
        token.access_token === currentToken.access_token
    ) {
        _token = await TokenModel.findOneAndUpdate(
            currentToken.access_token,
            token
        ).exec();
    } else {
        // if not the same token, replace the token
        _token = await TokenModel.create(token);
    }

    // set the token to the app.locals.blackboard token
    app.locals.blackboard.token.next(_token);
    app.locals.blackboard.tokenTimer.next(_token.expires_in);
};
const activeCoursesFragment = `
    availabilty
    endDate
    courseId
    termId
    campus
    session
    externalId
    sectionId
    externalTermId
    startDate
`;
export const activeCoursesQuery = `
query getDbActiveCourses($terms: [String]!, $refresh: Boolean){
    activeCourses: getDbActiveCourses(terms: $terms, refresh:$refresh) {
        ${activeCoursesFragment}
    }
  }
`;

export const activeCoursesQueryByTerm = `
query getDbActiveCoursesByTerm($term: String!, $refresh: Boolean){
    activeCourses: getDbActiveCoursesByTerm(term: $term, refresh:$refresh) {
        ${activeCoursesFragment}
    }
  }
`;
const courseDefinitionsFragment = `
    endDate
    enrolledCapacity
    section
    campusMeetingPattern
    shortTitle
    title
    capacity
    instructors
    academicYear
    term
    courseId
    delivery
    meetingDays
    courseTags
    courseSectionDefinition
    campus
    externalId
    externalTermId
    contactHours
    maximumUnits
    courseDescription
    academicLevel
    termDates
    courseSubject
    instructor
    publicNotes
    startDate
    status
`;

export const courseDefinitionsQuery = `
query getDbCourseDefinitions($externalIds: [String]!, $term: String!, $refresh: Boolean){
    courses: getDbCourseDefinitions(externalIds: $externalIds, term: $term, refresh:$refresh) {
        ${courseDefinitionsFragment}
    }
  }
`;

export const courseDefinitionsQueryByExternalId = `
query getDbCourseDefinitionByExternalId($externalId: String!, $term: String!, $refresh: Boolean){
    course: getDbCourseDefinitionByExternalId(externalId: $externalId, term: $term, refresh: $refresh) {
        ${courseDefinitionsFragment}
    }
  }
`;

export const courseDefinitionsQueryByTerms = `
query getDbCourseDefinitionsByTerms($terms: [String]!, $refresh: Boolean) {
    courses: getDbCourseDefinitionsByTerms(terms: $terms, refresh: $refresh) {
        ${courseDefinitionsFragment}
    }
  }
`;

export const userEmployeesQuery = `
query getDbEmployees($users: [String], $by: String, $refresh: Boolean) {
    users: getDbEmployees(users: $users, by: $by, refresh: $refresh){
      given
      jobTitle
      externalId
      jobFamily
      company
      userName
      family
      department
      email
    }
  }
`;

export const userEmployeeQuery = `
query getDbEmployee($user: String, $by: String, $refresh: Boolean) {
    user: getDbEmployee(user: $user, by: $by, refresh: $refresh){
      given
      jobTitle
      externalId
      jobFamily
      company
      userName
      family
      department
      email
    }
  }
`;
export const QUERY_REFRESH = true;
