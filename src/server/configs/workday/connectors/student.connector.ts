import { IConnector, enumType } from '../../../modules/types/connector.types';
import { Connector } from '../../../modules/classes/connector';

export const WdStudentConnectionConfig: IConnector = {
    credentials: {
        key: 'some_workday_key',
        secret: 'some_workday_secret',
        token: '',
        username: 'some_workday_username'
    },
    description: 'Main Workday Conncetor for Student Info',
    name: 'Workday - Student Info',
    notes:
        'This connector utilizes the reportst for student data for Blackboard',
    path: 'https://your-workday-url/ccx/service/customreport2/yourinstitution',
    prefix: 'workday',
    type: enumType.httpx
};

WdStudentConnectionConfig.credentials.token = Buffer.from(
    `${WdStudentConnectionConfig.credentials.key}:${
        WdStudentConnectionConfig.credentials.secret
    }`
).toString('base64');
WdStudentConnectionConfig.path = `${WdStudentConnectionConfig.path}/${
    WdStudentConnectionConfig.credentials.username
}`;
export const WdStudentConnector: Connector = new Connector(
    WdStudentConnectionConfig
);
