export {
    WdIntRegStudentCSConfig
} from './integrations/registered-student-cs.integration';

export {
    WdIntStudentReportConfig
} from './integrations/student-report.integration';

export {
    WdIntAdvisorReportConfig
} from './integrations/advisor-report.integration';

export {
    WdIntCourseDefinitionConfig
} from './integrations/course-definition.integration';
export {
    WdIntCourseDefinitionTermConfig
} from './integrations/course-definition-term.integration';

export {
    WdIntEducationalInstConfig
} from './integrations/educational-institutions.integration';

export {
    WdIntCourseInformationConfig
} from './integrations/course-information.integration';

export {
    WdIntCourseSectionsConfig
} from './integrations/course-sections.integration';

export {
    WdIntEmployeeReportConfig
} from './integrations/employee-report.integration';

