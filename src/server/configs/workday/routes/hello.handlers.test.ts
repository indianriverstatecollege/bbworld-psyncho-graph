import helloHandlers from './hello.handlers';
const req = {};
const res = {
    json: (data: any) => data,
};
describe('Test suite for hello.handlers', () => {
    it('Should test return data from get', () => {
        // tslint:disable-next-line: no-use-of-empty-return-value
        const result = helloHandlers.get(req, res);
        expect(result).toBe(undefined);
    });

    it('Should test return data from post', () => {
        // tslint:disable-next-line: no-use-of-empty-return-value
        const result = helloHandlers.post(req, res);
        expect(result).toBe(undefined);
    });

    it('Should test return data from put', () => {
        // tslint:disable-next-line: no-use-of-empty-return-value
        const result = helloHandlers.put(req, res);
        expect(result).toBe(undefined);
    });

    it('Should test return data from delete', () => {
        // tslint:disable-next-line: no-use-of-empty-return-value
        const result = helloHandlers.delete(req, res);
        expect(result).toBe(undefined);
    });

});
