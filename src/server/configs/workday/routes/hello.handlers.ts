export default {
    get: (req: any, res: any) => {
        res.json({msg: 'Hello From Workday'});
    },
    post: (req: any, res: any) => {
        const data = req.body;
        res.json({
            msg: 'You posted to Workday',
            data,
        });
    },
    // tslint:disable-next-line:no-identical-functions
    put: (req: any, res: any) => {
        const data = req.body;
        res.json({
            msg: 'You put data into Workday',
            data,
        });
    },
    delete: (req: any, res: any) => {
        res.json({msg: 'You deleted something from Workday'});
    },
};
