import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { EmployeeReports } from '../../../database/models/workday/employeeReport';

export const WdIntEmployeeReportConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint:
        '/REST_RPT_Employee_Report_for_Blackboard?format=json&externalId=:externalId',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    collectionName: 'employees',
    params: {
        externalId: ''
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntEmployeeReportConfig.model = new EmployeeReports().setModelForClass(
    EmployeeReports,
    {
        schemaOptions: {
            collection: `${WdIntEmployeeReportConfig.connector.prefix}_${
                WdIntEmployeeReportConfig.collectionName
            }`,
            timestamps: true,
            strict: false
        }
    }
);
