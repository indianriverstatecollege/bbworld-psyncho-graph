import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { CourseDefinition } from '../../../database/models/workday/courseDefinition';

export const WdIntCourseDefinitionTermConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint:
        '/REST_RPT_Course_List?format=json&academicPeriod!Academic_Period_ID=:academicPeriods',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    modelInjectionFields: {
        externalTermId: {
            args: ['externalId'],
            fn: (externalId: string) => {
                const parts = externalId.split('-');
                return `${parts[1]}${parts[3] === 'M' ? '' : parts[3]}`;
            }
        }
    },
    collectionName: 'course_definitions',
    params: {
        academicPeriods: []
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

// WdIntCourseDefinitionTermConfig.model = new CourseDefinition().setModelForClass(
//     CourseDefinition,
//     {
//         schemaOptions: {
//             collection: `${WdIntCourseDefinitionTermConfig.connector.prefix}_${
//                 WdIntCourseDefinitionTermConfig.collectionName
//             }`,
//             timestamps: true,
//             strict: false
//         }
//     }
// );
WdIntCourseDefinitionTermConfig.model = new CourseDefinition().getModelForClass(
    CourseDefinition
);
