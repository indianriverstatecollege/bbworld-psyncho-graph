import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { EducationalInstitutions } from '../../../database/models/workday/educationalInstitutions';

export const WdIntEducationalInstConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint: '/REST_RPT_Educational_Institutions?format=json',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    collectionName: 'educational_institutions',
    params: {},
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntEducationalInstConfig.model = new EducationalInstitutions().setModelForClass(
    EducationalInstitutions,
    {
        schemaOptions: {
            collection: `${WdIntEducationalInstConfig.connector.prefix}_${
                WdIntEducationalInstConfig.collectionName
            }`,
            timestamps: true
        }
    }
);
