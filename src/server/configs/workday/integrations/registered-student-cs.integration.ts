import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { ActiveCourseEnrollments } from '../../../database/models/workday/activeCourseEnrollments';

export const WdIntRegStudentCSConfig: IIntegration = {
    connector: WdStudentConnector,
    // tslint:disable-next-line:max-line-length
    endpoint:
        '/REST_RPT_Registered_Students_in_Course_Section?Section=:courseId&Academic_Period!Academic_Period_ID=ACADEMIC_PERIOD_:academicPeriod&format=json',
    errorHandlers: [],
    mode: enumMode.test,
    modelManyKey: 'Report_Entry',
    collectionName: 'active_course_enrollments',
    model: null,
    params: {
        courseId: '',
        academicPeriod: ''
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntRegStudentCSConfig.model = new ActiveCourseEnrollments().setModelForClass(
    ActiveCourseEnrollments,
    {
        schemaOptions: {
            collection: `${WdIntRegStudentCSConfig.connector.prefix}_${
                WdIntRegStudentCSConfig.collectionName
            }`,
            timestamps: true
        }
    }
);
