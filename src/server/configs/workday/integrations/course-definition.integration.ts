import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { CourseDefinition } from '../../../database/models/workday/courseDefinition';

export const WdIntCourseDefinitionConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint:
        '/REST_RPT_Course_List?format=json&externalId=:externalId&academicPeriod!Academic_Period_ID=ACADEMIC_PERIOD_:term',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    modelInjectionFields: {
        externalTermId: {
            args: ['externalId'],
            fn: (externalId: string) => {
                const parts = externalId.split('-');
                return `${parts[1]}${parts[3] === 'M' ? '' : parts[3]}`;
            }
        }
    },
    collectionName: 'course_definitions',
    params: {
        externalId: '',
        term: ''
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntCourseDefinitionConfig.model = new CourseDefinition().setModelForClass(
    CourseDefinition,
    {
        schemaOptions: {
            collection: `${WdIntCourseDefinitionConfig.connector.prefix}_${
                WdIntCourseDefinitionConfig.collectionName
            }`,
            timestamps: true,
            strict: false
        }
    }
);
