import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { CourseInformation } from '../../../database/models/workday/courseInformation';

export const WdIntCourseInformationConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint: '/REST_RPT_Course_Information?format=json',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    collectionName: 'course_information',
    params: {},
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntCourseInformationConfig.model = new CourseInformation().setModelForClass(
    CourseInformation,
    {
        schemaOptions: {
            collection: `${WdIntCourseInformationConfig.connector.prefix}_${
                WdIntCourseInformationConfig.collectionName
            }`,
            timestamps: true
        }
    }
);
