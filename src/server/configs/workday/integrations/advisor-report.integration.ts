import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { AdivsorReports } from '../../../database/models/workday/advisorReport';

export const WdIntAdvisorReportConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint: '/REST_RPT_Student_Advisor?studentId=:studentId&format=json',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    collectionName: 'advisor_reports',
    params: {
        studentId: ''
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntAdvisorReportConfig.model = new AdivsorReports().setModelForClass(
    AdivsorReports,
    {
        schemaOptions: {
            collection: `${WdIntAdvisorReportConfig.connector.prefix}_${
                WdIntAdvisorReportConfig.collectionName
            }`,
            timestamps: true
        }
    }
);
