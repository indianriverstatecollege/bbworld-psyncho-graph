import { IIntegration, enumMode } from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { CourseSection } from '../../../database/models/workday/courseSection';

export const WdIntCourseSectionsConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint:
        '/REST_RPT_Course_Sections?format=json&academicPeriod!Academic_Period_ID=:academicPeriods',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    modelInjectionFields: {
        courseId: '',
        termId: ''
    },
    modelIncludeFields: [],
    modelExcludeFields: [
        '_id',
        '__v',
        'createdAt',
        'updatedAt',
        'courseId',
        'termId'
    ],
    collectionName: 'active_courses',
    params: {
        sectionDefinition: '',
        term: ''
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntCourseSectionsConfig.model = new CourseSection().setModelForClass(
    CourseSection,
    {
        schemaOptions: {
            collection: `${WdIntCourseSectionsConfig.connector.prefix}_${
                WdIntCourseSectionsConfig.collectionName
            }`,
            timestamps: true,
            toJSON: { virtuals: true },
            toObject: { virtuals: true }
        }
    }
);
