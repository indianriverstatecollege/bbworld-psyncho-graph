import {
    IIntegration,
    enumMode
} from '../../../modules/types/integration.types';
import {
    WdStudentConnector,
    WdStudentConnectionConfig
} from '../connectors/student.connector';
import { StudentReports } from '../../../database/models/workday/studentReport';

export const WdIntStudentReportConfig: IIntegration = {
    connector: WdStudentConnector,
    endpoint:
        '/REST_RPT_Student_Report_for_Blackboard?activeOnly=1&academicPeriod=:term&studentId=:studentId&format=json',
    errorHandlers: [],
    mode: enumMode.test,
    model: null,
    modelManyKey: 'Report_Entry',
    collectionName: 'students',
    params: {
        studentId: '',
        term: ''
    },
    defaultHttpOptions: {
        method: 'GET',
        headers: {
            Authorization: `Basic ${
                WdStudentConnectionConfig.credentials.token
            }`
        },
        json: true
    }
};

WdIntStudentReportConfig.model = new StudentReports().setModelForClass(
    StudentReports,
    {
        schemaOptions: {
            collection: `${WdIntStudentReportConfig.connector.prefix}_${
                WdIntStudentReportConfig.collectionName
            }`,
            timestamps: true,
            strict: false
        }
    }
);
