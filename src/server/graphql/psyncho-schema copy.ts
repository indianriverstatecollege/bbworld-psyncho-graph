export default `
type Query {
  WdActiveCourseEnrollments: WdActiveCourseEnrollments
  WdStudentReport: WdStudentReport
  WdAdvisor: WdAdvisor
  getDbActiveCourses(terms: [String]!, refresh: Boolean): [[WdActiveCoursesReportEntry]]
  getDbActiveCoursesByTerm(term: String!, refresh: Boolean): [WdActiveCoursesReportEntry]
  getCourseDefinitions(term: String!, refresh: Boolean): [WdCourseDefinitionsReportEntry]
  getCourseDefinitionsByTerms(terms: [String]!, refresh: Boolean): [[WdCourseDefinitionsReportEntry]]
  getCourseDefinitionByExternalId(externalId: String!, term: String!, refresh: Boolean): WdCourseDefinitionsReportEntry
  getEmployee(user: String!, by: String, refresh: Boolean): WdEmployeeReportEntry
  getEmployees(users: [String]!, by: String, refresh: Boolean): [WdEmployeeReportEntry]
  getAllEmployees(refresh: Boolean): [WdEmployeeReportEntry]
  getStudent(user: String!, refresh: Boolean): WdStudentReportEntry
  getStudents(users: [String]!, term: String, action: String, refresh: Boolean): [WdStudentReportEntry]
  getAllStudents(term: String, refresh: Boolean): [WdStudentReportEntry]
  hello: String
  listCollections: [String]
  getBbDataSource(dataSourceId: String!, refresh: Boolean): BbDataSource
  getBbDataSources(dataSourceIds: [String]!, refresh: Boolean): [BbDataSource]
  getAllBbDataSources(refresh: Boolean): [BbDataSource]
  getBbTerm(termId: String!, refresh: Boolean): BbTerm
  getBbTerms(termIds: [String]!, refresh: Boolean): [BbTerm]
  getAllBbTerms(refresh: Boolean): [BbTerm]
  getBbUser(userId: String!, refresh: Boolean): BbUser
  getBbUsers(userIds: [String]!, refresh: Boolean): [BbUser]
  getAllBbUsers(refresh: Boolean): [BbUser]
  getBbCourse(courseId: String!, refresh: Boolean): BbCourse
  getBbCourses(courseIds: [String]!, refresh: Boolean): [BbCourse]
  getAllBbCourses(refresh: Boolean): [BbCourse]
  getBbMembership(courseId: String!, userId: String!, refresh: Boolean): BbMembership
  getBbMemberships(memberships: [MembershipsType]!, refresh: Boolean): [BbMembership]
  getAllBbMemberships(courseId: String!, refresh: Boolean): [BbMembership]
}

type Mutation {
  createBbDataSource(input: BbDataSourceInput!): BbDataSource
  updateBbDataSource(dataSourceId: String!, input: BbDataSourceInput!): BbDataSource
  deleteBbDataSource(dataSourceId: String!): String
  createBbTerm(input: BbTermInput!): BbTerm
  updateBbTerm(termId: String!, input: BbTermInput!): BbTerm
  deleteBbTerm(termId: String!): String
  createBbUser(input: BbUserInput!): BbUser
  updateBbUser(userId: String!, input: BbUserInput!): BbUser
  deleteBbUser(userId: String!): String
  createBbCourse(input: BbCourseInput!): BbCourse
  updateBbCourse(courseId: String!, input: BbCourseInput!): BbCourse
  deleteBbCourse(courseId: String!): String
  createBbMembership(courseId: String!, userId: String!, input: BbMembershipInput!): BbMembership
  updateBbMembership(courseId: String!, userId: String!, input: BbMembershipInput!): BbMembership
  deleteBbMembership(courseId: String!, userId: String!): String
}


"""
No description for BbCourse provided.
"""
type BbCourse {
  """
  The primary ID of the course_
  """
  id: String

  """
  A secondary unique ID for the course_ Used by LTI launches and other inter_server operations_
  """
  uuid: String

  """
  An optional externally_defined unique ID for the course_ Defaults to the courseId_
  
  Formerly known as 'batchUid'_
  """
  externalId: String

  """
  The ID of the data source associated with this course_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The Course ID attribute, shown to users in the UI_
  """
  courseId: String

  """
  The name of the course_
  """
  name: String

  """
  The description of the course_
  """
  description: String

  """
  The date this course was created_
  """
  created: String

  """
  Whether this object represents an Organization_ Defaults to false_
  """
  organization: Boolean

  """
  Whether the course is rendered using Classic or Ultra Course View_
  | Type      | Description
  | _________ | _________ |
  | Undecided | The ultra status is not decided_ |
  | Classic | The course is decided as classic_ |
  | Ultra | The course is decided as ultra |
  | UltraPreview | The course is currently in Ultra mode but during the preview state where it may still be reverted via a Restore to the classic state |
  """
  ultraStatus: BbCourseUltraStatus

  """
  Whether guests (users with the role guest) are allowed access to the course_ Defaults to true_
  """
  allowGuests: Boolean

  """
  This status does not affect availability of the course for viewing in any way_ readOnly is valid for both Ultra and Classic courses_ If an Ultra course is in readOnly mode, updates are not possible_ For a Classic course in readOnly mode, updates are still possible (through Web UI but not through REST i_e_ closed is enforced for original courses when updated through REST the same way Ultra courses are blocked) but new notifications are not generated_
  **Deprecated**: since 3400_8_0; use the v2 endpoint's closedComplete property instead
  """
  readOnly: Boolean

  """
  The ID of the term associated to this course_ This may optionally be the term's externalId using the syntax "externalId:spring_2016"_
  """
  termId: String

  """
  Settings controlling availability of the course to students_
  """
  availability: BbCourseAvailability

  """
  Settings controlling how students may enroll in the course_
  """
  enrollment: BbCourseEnrollment

  """
  Settings controlling localization within the course_
  """
  locale: BbCourseLocale

  """
  Whether the course has any cross_listed children_
  **Since**: 3000_11_0
  """
  hasChildren: Boolean

  """
  The cross_listed parentId associated with the course, if the course is a child course_
  
  **Since**: 3000_11_0
  """
  parentId: String

  """
  No description provided for BbCourse_
  """
  externalAccessUrl: String

  """
  No description provided for BbCourse_
  """
  guestAccessUrl: String
}


"""
No description for BbCourse provided.
"""
input BbCourseInput {
  """
  An optional externally_defined unique ID for the course_ Defaults to the courseId_
  
  Formerly known as 'batchUid'_
  """
  externalId: String

  """
  The ID of the data source associated with this course_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The name of the course_
  """
  name: String

  """
  The description of the course_
  """
  description: String

  """
  Whether this object represents an Organization_ Defaults to false_
  """
  organization: Boolean

  """
  Whether guests (users with the role guest) are allowed access to the course_ Defaults to true_
  """
  allowGuests: Boolean

  """
  The ID of the term associated to this course_ This may optionally be the term's externalId using the syntax "externalId:spring_2016"_
  """
  termId: String

  """
  Settings controlling availability of the course to students_
  """
  availability: BbCourseAvailabilityInput

  """
  Settings controlling how students may enroll in the course_
  """
  enrollment: BbCourseEnrollmentInput

  """
  Settings controlling localization within the course_
  """
  locale: BbCourseLocaleInput

  """
  The cross_listed parentId associated with the course, if the course is a child course_
  **Since**: 3000_11_0
  """
  parentId: String
}

enum BbCourseUltraStatus {
  Undecided
  Classic
  Ultra
  UltraPreview
}


"""
Settings controlling availability of the course to students.
"""
type BbCourseAvailability {
  """
  Whether the course is currently available to students_ Instructors can always access the course if they have 'Access unavailable course' entitlement_ If set to 'Term', the course's parent term availability settings will be used_
  | Type      | Description
  | _________ | _________ |
  | Yes | Students may access the course_ |
  | No | Students may not access the course_ |
  | Disabled | Disabled by the SIS_ Students may not access the course_  **Since**: 3100_0_0 |
  | Term | Availability is inherited from the term settings_ Requires a termId be set_ |
  """
  available: BbCourseAvailabilityAvailable

  """
  Settings controlling the length of time the course is available_
  """
  duration: BbCourseAvailabilityDuration
}


"""
Settings controlling availability of the course to students.
"""
input BbCourseAvailabilityInput {
  """
  Whether the course is currently available to students_ Instructors can always access the course if they have 'Access unavailable course' entitlement_ If set to 'Term', the course's parent term availability settings will be used_
  | Type      | Description
  | _________ | _________ |
  | Yes | Students may access the course_ |
  | No | Students may not access the course_ |
  | Disabled | Disabled by the SIS_ Students may not access the course_  **Since**: 3100_0_0 |
  | Term | Availability is inherited from the term settings_ Requires a termId be set_ |
  """
  available: BbCourseAvailabilityAvailable

  """
  Settings controlling the length of time the course is available_
  """
  duration: BbCourseAvailabilityDurationInput
}


enum BbCourseAvailabilityAvailable {
  Yes
  No
  Disabled
  Term
}

"""
Settings controlling the length of time the course is available.
"""
type BbCourseAvailabilityDuration {
  """
  The intended length of the course_
  
  
  | Type      | Description
   | _________ | _________ |
  | Continuous | Course is active on an ongoing basis_ |
  | DateRange | Course is only intended to be available between specific date ranges |
  | FixedNumDays | Course is only available for a set number of days |
  | Term | Course availablity is dictated by its associated term |
  """
  type: BbCourseAvailabilityDurationType

  """
  The date this course starts_ May only be set if availability_duration_type is DateRange_
  """
  start: String

  """
  The date this course ends_ May only be set if availability_duration_type is DateRange_
  """
  end: String

  """
  The number of days this course can be used_ May only be set if availability_duration_type is FixedNumDays_
  """
  daysOfUse: Int
}

"""
Settings controlling the length of time the course is available.
"""
input BbCourseAvailabilityDurationInput {
  """
  The intended length of the course_
  | Type      | Description
  | _________ | _________ |
  | Continuous | Course is active on an ongoing basis_ |
  | DateRange | Course is only intended to be available between specific date ranges |
  | FixedNumDays | Course is only available for a set number of days |
  | Term | Course availablity is dictated by its associated term |
  """
  type: BbCourseAvailabilityDurationType

  """
  The date this course starts_ May only be set if availability_duration_type is DateRange_
  """
  start: String

  """
  The date this course ends_ May only be set if availability_duration_type is DateRange_
  """
  end: String

  """
  The number of days this course can be used_ May only be set if availability_duration_type is FixedNumDays_
  """
  daysOfUse: Int
}


enum BbCourseAvailabilityDurationType {
  Continuous
  DateRange
  FixedNumDays
  Term
}

"""
Settings controlling how students may enroll in the course.
"""
type BbCourseEnrollment {
  """
  Specifies the enrollment options for the course_ Defaults to InstructorLed_
  | Type      | Description
  | _________ | _________ |
  | InstructorLed | Enrollment tasks for the course can only performed by the instructor |
  | SelfEnrollment | Instructors have the ability to enroll users, and students can also enroll themselves in the course |
  | EmailEnrollment | Instructors have the ability to enroll users, and students can email requests to the instructor for enrollment |
  """
  type: BbCourseEnrollmentType

  """
  The date on which enrollments are allowed for the course_ May only be set if enrollment_type is SelfEnrollment_
  """
  start: String

  """
  The date on which enrollment in this course ends_ May only be set if enrollment_type is SelfEnrollment_
  """
  end: String

  """
  The enrollment access code associated with this course_ May only be set if enrollment_type is SelfEnrollment_
  """
  accessCode: String
}


"""
Settings controlling how students may enroll in the course.
"""
input BbCourseEnrollmentInput {
  """
  Specifies the enrollment options for the course_ Defaults to InstructorLed_
  | Type      | Description
  | _________ | _________ |
  | InstructorLed | Enrollment tasks for the course can only performed by the instructor |
  | SelfEnrollment | Instructors have the ability to enroll users, and students can also enroll themselves in the course |
  | EmailEnrollment | Instructors have the ability to enroll users, and students can email requests to the instructor for enrollment |
  """
  type: BbCourseEnrollmentType

  """
  The date on which enrollments are allowed for the course_ May only be set if enrollment_type is SelfEnrollment_
  """
  start: String

  """
  The date on which enrollment in this course ends_ May only be set if enrollment_type is SelfEnrollment_
  """
  end: String

  """
  The enrollment access code associated with this course_ May only be set if enrollment_type is SelfEnrollment_
  """
  accessCode: String
}

enum BbCourseEnrollmentType {
  InstructorLed
  SelfEnrollment
  EmailEnrollment
}


"""
Settings controlling localization within the course.
"""
type BbCourseLocale {
  """
  The locale of this course_
  """
  id: String

  """
  Whether students are forced to use the course's specified locale_
  """
  force: Boolean
}



"""
Settings controlling localization within the course.
"""
input BbCourseLocaleInput {
  """
  Whether students are forced to use the course's specified locale_
  """
  force: Boolean
}

"""
No description for BbMembership provided.
"""
type BbMembership {
  """
  The primary ID of the user_
  """
  userId: String

  """
  The primary ID of the course_
  """
  courseId: String

  """
  The course associated with the membership_
  Shown when adding the query parameter: expand=course_ And can be filtered with the "fields" query parameter, for example "fields=course_id,course_externalId"_
  **Since**: 3500_4_0
  """
  course: String

  """
  The primary ID of the child, cross_listed course, in which the user is directly enrolled_ </p> This field is read only in Learn versions 3000_11_0 through 3400_0_0_ As of 3400_1_0, this field is mutable_  </p> If this membership's course is a parent course in a cross_listed set, the childCourseId can be updated to move the membership enrollment between child courses and the parent course in  the set_  Patching the childCourseId to "null" will move the membership to the parent course_
  **Since**: 3000_11_0
  """
  childCourseId: String

  """
  The ID of the data source associated with this course_  This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The date this membership was created_
  """
  created: String

  """
  Settings controlling availability of the course membership_
  """
  availability: String

  """
  The user's role in the course_
  
  These roles are also valid for an organization, although they are named differently in the UI_
  
  Custom course roles may also be referenced by their IDs_
  
  
  | Type      | Description
  | _________ | _________ |
  | Instructor | Has access to all areas in the Control Panel_ This role is generally given to those developing, teaching, or facilitating the class_ Instructors may access a course that is unavailable to students_ This role is customizable and may have different capabilities from what is documented here_ |
  | BbFacilitator | The facilitator is an instructor like role_ Facilitators are restricted versions of an instructor, in that they are able to deliver course instruction and administer all aspects of a pre_constructed course, but are not allowed to modify or alter the course_ This role is customizable and may have different capabilities from what is documented here_ |
  | TeachingAssistant | The teaching assistant role is that of a co_teacher_ Teaching assistants are able to administer all areas of a course_ Their only limitations are those imposed by the instructor or Blackboard administrator at your school_ This role is customizable and may have different capabilities from what is documented here_ |
  | CourseBuilder | Manages the course without having access to student grades_ This role is customizable and may have different capabilities from what is documented here_ |
  | Grader | Assists the instructor in the creation, management, delivery, and grading of items_ This role is customizable and may have different capabilities from what is documented here_ |
  | Student |  |
  | Guest | Has no access to the Control Panel_ Areas within the course are made available to guests, but typically they can only view course materials; they do not have access to tests or assessments, and do not have permission to post on discussion boards_ This role's behavior is immutable_ |
  """
  courseRoleId: BbMembershipCourseRoleId

  """
  If present, this date signals that the user associated with this membership has special access to the course regardless of the course's availability setting prior to the moment specified by this field_ After the date has passed, the membership will respect the course's availability_
  """
  bypassCourseAvailabilityUntil: String

  """
  This date signals the date this membership was used; in other words, the last date the user accessed the associated course or content contained by that course_
  
  The recording of any activity which would lead to this date getting updated does happen asynchronously in batches_ So, there may be some delay between an activity which would update this value and the availability of the new date_ It is recommended when using this value to note that activity within the last 5 minutes may not be taken into account_
  
  **Since**: 3300_9_0
  """
  lastAccessed: String
}

"""
No description for BbMembership provided.
"""
input BbMembershipInput {
  """
  The primary ID of the user_
  """
  userId: String

  """
  The primary ID of the course_
  """
  courseId: String

  """
  The course associated with the membership_
  Shown when adding the query parameter: expand=course_ And can be filtered with the "fields" query parameter, for example "fields=course_id,course_externalId"_
  **Since**: 3500_4_0
  """
  course: String

  """
  The primary ID of the child, cross_listed course, in which the user is directly enrolled_ </p> This field is read only in Learn versions 3000_11_0 through 3400_0_0_ As of 3400_1_0, this field is mutable_  </p> If this membership's course is a parent course in a cross_listed set, the childCourseId can be updated to move the membership enrollment between child courses and the parent course in  the set_  Patching the childCourseId to "null" will move the membership to the parent course_
  **Since**: 3000_11_0
  """
  childCourseId: String

  """
  The ID of the data source associated with this course_  This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  Settings controlling availability of the course membership_
  """
  availability: String

  """
  The user's role in the course_
  These roles are also valid for an organization, although they are named differently in the UI_
  Custom course roles may also be referenced by their IDs_
  
  | Type      | Description
  | _________ | _________ |
  | Instructor | Has access to all areas in the Control Panel_ This role is generally given to those developing, teaching, or facilitating the class_ Instructors may access a course that is unavailable to students_ This role is customizable and may have different capabilities from what is documented here_ |
  | BbFacilitator | The facilitator is an instructor like role_ Facilitators are restricted versions of an instructor, in that they are able to deliver course instruction and administer all aspects of a pre_constructed course, but are not allowed to modify or alter the course_ This role is customizable and may have different capabilities from what is documented here_ |
  | TeachingAssistant | The teaching assistant role is that of a co_teacher_ Teaching assistants are able to administer all areas of a course_ Their only limitations are those imposed by the instructor or Blackboard administrator at your school_ This role is customizable and may have different capabilities from what is documented here_ |
  | CourseBuilder | Manages the course without having access to student grades_ This role is customizable and may have different capabilities from what is documented here_ |
  | Grader | Assists the instructor in the creation, management, delivery, and grading of items_ This role is customizable and may have different capabilities from what is documented here_ |
  | Student |  |
  | Guest | Has no access to the Control Panel_ Areas within the course are made available to guests, but typically they can only view course materials; they do not have access to tests or assessments, and do not have permission to post on discussion boards_ This role's behavior is immutable_ |
  """
  courseRoleId: BbMembershipCourseRoleId
}

enum BbMembershipCourseRoleId {
  Instructor
  BbFacilitator
  TeachingAssistant
  CourseBuilder
  Grader
  Student
  Guest
}

input MembershipsType {
  courseId: String
  userId: String
}

"""
No description for BbUser provided.
"""
type BbUser {
  """
  The primary ID of the user_
  """
  id: String

  """
  A secondary unique ID for the user_ Used by LTI launches and other inter_server operations_
  """
  uuid: String

  """
  An optional externally_defined unique ID for the user_ Defaults to the userName_
  
  Formerly known as 'batchUid'_
  """
  externalId: String

  """
  The ID of the data source associated with this user_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The userName property, shown in the UI_
  """
  userName: String

  """
  The user's student ID name or number as defined by the school or institution_
  """
  studentId: String

  """
  The education level of this user_
  """
  educationLevel: BbUserEducationLevel

  """
  The gender of this user_
  """
  gender: BbUserGender

  """
  The birth date of this user_
  """
  birthDate: String

  """
  The date this user was created_
  """
  created: String

  """
  The date this user was last modified_
  """
  modified: String

  """
  The date this user last logged in_
  """
  lastLogin: String

  """
  The primary and secondary institution roles assigned to this user_ The primary institution role is the first item in the list, followed by all secondary institution roles sorted alphabetically_
  
  **Since**: 3300_3_0
  """
  institutionRoleIds: [String]

  """
  The system roles (the administrative user roles in the UI) for this user_ The first role in this list is the user's primary system role, while the remaining are secondary system roles_
  """
  systemRoleIds: [BbSystemRoleEnum]

  """
  Settings controlling availability of the user account_
  """
  availability: BbUserAvailability

  """
  Properties used to build the user's display name_
  """
  name: BbUserName

  """
  The user's job information_
  """
  job: BbUserJob

  """
  The user's contact information_
  """
  contact: BbUserContact

  """
  The user's mailing address_
  """
  address: BbUserAddress

  """
  The user's localization settings_
  """
  locale: BbUserLocale
}

enum BbSystemRoleEnum {
  SystemAdmin
  SystemSupport
  CourseCreator
  CourseSupport
  AccountAdmin
  Guest
  User
  Observer
  Integration
  Portal
}

enum BbUserEducationLevel {
  K8
  HighSchool
  Freshman
  Sophomore
  Junior
  Senior
  GraduateSchool
  PostGraduateSchool
  Unknown
}

enum BbUserGender {
  Female
  Male
  Unknown
}

"""
Settings controlling availability of the user account.
"""
type BbUserAvailability {
  """
  Whether the user is available within the system_ Unavailable users cannot log in_
  """
  available: BbUserAvailabilityAvailable
}

enum BbUserAvailabilityAvailable {
  Yes
  No
  Disabled
}

"""
Properties used to build the user's display name.
"""
type BbUserName {
  """
  The given (first) name of this user_
  """
  given: String

  """
  The family (last) name of this user_
  """
  family: String

  """
  The middle name of this user_
  """
  middle: String

  """
  The other name (nickname) of this user_
  """
  other: String

  """
  The suffix of this user's name_ Examples: Jr_, III, PhD_
  """
  suffix: String

  """
  The title of this user_ Examples: Mr_, Ms_, Dr_
  """
  title: String
}

"""
The user's job information.
"""
type BbUserJob {
  """
  The user's job title_
  """
  title: String

  """
  The department the user belongs to_
  """
  department: String

  """
  The company the user works for_
  """
  company: String
}

"""
The user's contact information.
"""
type BbUserContact {
  """
  The user's home phone number_
  """
  homePhone: String

  """
  The user's mobile phone number_
  """
  mobilePhone: String

  """
  The user's business phone number_
  """
  businessPhone: String

  """
  The user's business fax number_
  """
  businessFax: String

  """
  The user's email address_
  """
  email: String

  """
  The URL of the user's personal website_
  """
  webPage: String
}

"""
The user's mailing address.
"""
type BbUserAddress {
  """
  The street address of the user_
  """
  street1: String

  """
  An additional field to store the street address of the user_
  """
  street2: String

  """
  The city the user resides in_
  """
  city: String

  """
  The state or province the user resides in_
  """
  state: String

  """
  The zip code or postal code the user resides in_
  """
  zipCode: String

  """
  The country the user resides in_
  """
  country: String
}

"""
The user's localization settings.
"""
type BbUserLocale {
  """
  The locale specified by the user_ This locale will be used anywhere the user is allowed to customize their locale courses may force a specific locale, overriding the user's locale preference_
  """
  id: String

  """
  The calendar type specified by the user_
  """
  calendar: BbUserLocaleCalendar
  firstDayOfWeek: BbUserLocaleFirstDayOfWeek
}

enum BbUserLocaleCalendar {
  Gregorian
  GregorianHijri
  Hijri
  HijriGregorian
}

enum BbUserLocaleFirstDayOfWeek {
  Sunday
  Monday
  Saturday
}

"""
No description for BbTerm provided.
"""
type BbTerm {
  """
  The primary ID of the term_
  """
  id: String

  """
  An externally_defined unique ID for the term_
  
  Formerly known as 'sourcedidId'_
  """
  externalId: String

  """
  The ID of the data source associated with this term_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The name of the term_
  """
  name: String

  """
  Settings controlling availability of the term to students_
  """
  availability: BbTermAvailability
}

"""
No description for BbUser provided.
"""
input BbUserInput {
  """
  The primary ID of the user_
  """
  id: String

  """
  A secondary unique ID for the user_ Used by LTI launches and other inter_server operations_
  """
  uuid: String

  """
  An optional externally_defined unique ID for the user_ Defaults to the userName_
  
  Formerly known as 'batchUid'_
  """
  externalId: String

  """
  The ID of the data source associated with this user_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The userName property, shown in the UI_
  """
  userName: String

  """
  The password that will be used for the user.
  """
  password: String

  """
  The user's student ID name or number as defined by the school or institution_
  """
  studentId: String

  """
  The education level of this user_
  """
  educationLevel: BbUserEducationLevel

  """
  The gender of this user_
  """
  gender: BbUserGender

  """
  The birth date of this user_
  """
  birthDate: String

  """
  The date this user was created_
  """
  created: String

  """
  The date this user was last modified_
  """
  modified: String

  """
  The date this user last logged in_
  """
  lastLogin: String

  """
  The primary and secondary institution roles assigned to this user_ The primary institution role is the first item in the list, followed by all secondary institution roles sorted alphabetically_
  
  **Since**: 3300_3_0
  """
  institutionRoleIds: [String]

  """
  The system roles (the administrative user roles in the UI) for this user_ The first role in this list is the user's primary system role, while the remaining are secondary system roles_
  """
  systemRoleIds: [BbSystemRoleEnum]

  """
  Settings controlling availability of the user account_
  """
  availability: BbUserAvailabilityInput

  """
  Properties used to build the user's display name_
  """
  name: BbUserNameInput

  """
  The user's job information_
  """
  job: BbUserJobInput

  """
  The user's contact information_
  """
  contact: BbUserContactInput

  """
  The user's mailing address_
  """
  address: BbUserAddressInput

  """
  The user's localization settings_
  """
  locale: BbUserLocaleInput
}

"""
Settings controlling availability of the user account.
"""
input BbUserAvailabilityInput {
  """
  Whether the user is available within the system_ Unavailable users cannot log in_
  """
  available: BbUserAvailabilityAvailable
}

"""
Properties used to build the user's display name.
"""
input BbUserNameInput {
  """
  The given (first) name of this user_
  """
  given: String

  """
  The family (last) name of this user_
  """
  family: String

  """
  The middle name of this user_
  """
  middle: String

  """
  The other name (nickname) of this user_
  """
  other: String

  """
  The suffix of this user's name_ Examples: Jr_, III, PhD_
  """
  suffix: String

  """
  The title of this user_ Examples: Mr_, Ms_, Dr_
  """
  title: String
}

"""
The user's job information.
"""
input BbUserJobInput {
  """
  The user's job title_
  """
  title: String

  """
  The department the user belongs to_
  """
  department: String

  """
  The company the user works for_
  """
  company: String
}

"""
The user's contact information.
"""
input BbUserContactInput {
  """
  The user's home phone number_
  """
  homePhone: String

  """
  The user's mobile phone number_
  """
  mobilePhone: String

  """
  The user's business phone number_
  """
  businessPhone: String

  """
  The user's business fax number_
  """
  businessFax: String

  """
  The user's email address_
  """
  email: String

  """
  The URL of the user's personal website_
  """
  webPage: String
}

"""
The user's mailing address.
"""
input BbUserAddressInput {
  """
  The street address of the user_
  """
  street1: String

  """
  An additional field to store the street address of the user_
  """
  street2: String

  """
  The city the user resides in_
  """
  city: String

  """
  The state or province the user resides in_
  """
  state: String

  """
  The zip code or postal code the user resides in_
  """
  zipCode: String

  """
  The country the user resides in_
  """
  country: String
}

"""
The user's localization settings.
"""
input BbUserLocaleInput {
  """
  The locale specified by the user_ This locale will be used anywhere the user is allowed to customize their locale courses may force a specific locale, overriding the user's locale preference_
  """
  id: String

  """
  The calendar type specified by the user_
  """
  calendar: BbUserLocaleCalendar
  firstDayOfWeek: BbUserLocaleFirstDayOfWeek
}

input BbTermInput {
  externalId: String

  """
  The ID of the data source associated with this term_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
  """
  dataSourceId: String

  """
  The name of the term_
  """
  name: String

  """
  Settings controlling availability of the term to students_
  """
  availability: BbTermAvailabilityInput
}

"""
Settings controlling availability of the term to students.
"""
type BbTermAvailability {
  """
    Whether the term and the courses it contains are available to students_ 
    Instructors can always access their courses_
  | Type      | Description
  | _________ | _________ |
  | Yes | Students may access the term and the courses it contains_ |
  | No | Students may not access the term or the courses it contains_ |
  """
  available: BbTermAvailabilityAvailable

  """
  Settings controlling the length of time the term is available_
  """
  duration: BbTermAvailabilityDuration
}

input BbTermAvailabilityInput {
  """
  Whether the term and the courses it contains are available to students_ Instructors can always access their courses_
  
  
  | Type      | Description
  | _________ | _________ |
  | Yes | Students may access the term and the courses it contains_ |
  | No | Students may not access the term or the courses it contains_ |
  """
  available: BbTermAvailabilityAvailable

  """
  Settings controlling the length of time the term is available_
  """
  duration: BbTermAvailabilityDurationInput
}

enum BbTermAvailabilityAvailable {
  Yes
  No
}

"""
Settings controlling the length of time the term is available.
"""
type BbTermAvailabilityDuration {
  """
  The intended length of the term_ Possible values are:
  | Type      | Description
  | _________ | _________ |
  | Continuous | The term is active on an ongoing basis_ This is the default_ |
  | DateRange | The term will only be available between specific date ranges_ |
  | FixedNumDays | The term will only be available for a set number of days_ |
  """
  type: BbTermAvailabilityDurationType

  """
  The date this term starts_ May only be set if availability_duration_type is DateRange_
  """
  start: String

  """
  The date this term ends_ May only be set if availability_duration_type is DateRange_
  """
  end: String

  """
  The number of days courses within this term can be used_ May only be set if availability_duration_type is FixedNumDays_
  """
  daysOfUse: Int
}

"""
Settings controlling the length of time the term is available.
"""
input BbTermAvailabilityDurationInput {
  """
  The intended length of the term_ Possible values are:
  | Type      | Description
  | _________ | _________ |
  | Continuous | The term is active on an ongoing basis_ This is the default_ |
  | DateRange | The term will only be available between specific date ranges_ |
  | FixedNumDays | The term will only be available for a set number of days_ |
  """
  type: BbTermAvailabilityDurationType

  """
  The date this term starts_ May only be set if availability_duration_type is DateRange_
  """
  start: String

  """
  The date this term ends_ May only be set if availability_duration_type is DateRange_
  """
  end: String

  """
  The number of days courses within this term can be used_ May only be set if availability_duration_type is FixedNumDays_
  """
  daysOfUse: Int
}

enum BbTermAvailabilityDurationType {
  Continuous
  DateRange
  FixedNumDays
}

type BbDataSource {
  id: String
  externalId: String
  description: String
}

input BbDataSourceInput {
  externalId: String
  description: String
}

type Enrollments {
  studentId: String
  studentName: String
  status: String
}

type WdActiveCourseEnrollmentsReportEntry {
  externalId: String
  primaryInstructorEmail: String
  availability: String
  enrollments: [Enrollments]
}

type WdActiveCourseEnrollments {
  Report_Entry: [WdActiveCourseEnrollmentsReportEntry]
}

type WdStudentReportReportEntry {
  lastName: String
  homePhone: String
  homeAddressLine1: String
  cohort: String
  homeState: String
  birthMonthYear: String
  homePostalCode: String
  studentId: String
  firstName: String
  homeEmail: String
  homeCity: String
  workdayID: String
  middleName: String
  institutionalEmail: String
}

type WdStudentReport {
  Report_Entry: [WdStudentReportReportEntry]
}

type RoleAssignmentsWorkersAssigned {
  firstName: String
  lastName: String
  employeeId: String
  middleName: String
  userName: String
  primaryWorkEmail: String
}

type WdAdvisorReportEntry {
  studentId: String
  studentCohortType: String
  student: String
  studentCohort: String
  roleAssignments: String
  roleAssignmentsWorkersAssigned: [RoleAssignmentsWorkersAssigned]
}

type WdAdvisor {
  Report_Entry: [WdAdvisorReportEntry]
}

type WdActiveCourses {
  Report_Entry: [WdActiveCoursesReportEntry]
}

type WdActiveCoursesReportEntry {
  availabilty: String
  endDate: String
  courseId: String
  termId: String
  campus: String
  session: String
  externalId: String
  sectionId: String
  externalTermId: String
  startDate: String
}

type WdCourseDefinitions {
  Report_Entry: [WdCourseDefinitionsReportEntry]
}

type WdCourseDefinitionsReportEntry {
  academicLevel: String
  academicYear: String
  campus: String
  campusMeetingPattern: String
  capacity: String
  contactHours: String
  courseDescription: String
  courseId: String
  courseSectionDefinition: String
  courseSubject: String
  courseTags: String
  createdAt: String
  delivery: String
  endDate: String
  enrolledCapacity: String
  externalId: String
  externalTermId: String
  instructor: String
  instructors: String
  maximumUnits: String
  meetingDays: String
  publicNotes: String
  section: String
  shortTitle: String
  startDate: String
  status: String
  term: String
  termDates: String
  title: String
  updatedAt: String
}

type WdEmployees {
  Report_Entry: [WdEmployeeReportEntry]
}

type WdEmployeeReportEntry {
  given: String
  jobTitle: String
  externalId: String
  jobFamily: String
  company: String
  userName: String
  family: String
  department: String
  email: String
}

type WdStudents {
  Report_Entry: [WdStudentReportEntry]
}

type WdStudentReportEntry {
  lastName: String
  homePhone: String
  homeAddressLine1: String
  homeState: String
  birthMonthYear: String
  homePostalCode: String
  studentId: String
  firstName: String
  homeEmail: String
  mobilePhone: String
  programName: String
  homeCity: String
  workdayID: String
  middleName: String
  institutionalEmail: String
  programID: String
}
`;
