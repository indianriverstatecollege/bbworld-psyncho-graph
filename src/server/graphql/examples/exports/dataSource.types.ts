'use strict';
import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInputObjectType
} from 'graphql';

export const DataSourceType = new GraphQLObjectType({
    name: 'DataSourceType',
    description: 'A Blackboard Announcement Learn Object',
    fields: () => ({
        _id: { type: GraphQLString },
        id: { type: GraphQLString },
        externalId: { type: GraphQLString },
        description: { type: GraphQLString }
    })
});

export const DataSourceInputFilter = new GraphQLInputObjectType({
    name: 'DataSourceInputFilter',
    description: 'A JSON like object to filter our announcements',
    fields: () => ({
        _id: { type: GraphQLString },
        id: { type: GraphQLString },
        externalId: { type: GraphQLString },
        description: { type: GraphQLString }
    })
});

export const DataSourcesType = new GraphQLObjectType({
    name: 'DataSourcesType',
    description: 'A list of Blackboard Data Sources Learn Objects',
    fields: () => ({
        results: { type: new GraphQLList(DataSourceType) }
    })
});
