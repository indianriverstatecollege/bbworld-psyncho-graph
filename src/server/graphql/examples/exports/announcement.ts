'use strict';
import {
    GraphQLBoolean,
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInputObjectType
} from 'graphql';

export const AnnouncementDuration = new GraphQLObjectType({
    name: 'AnnouncementDuration',
    description: '',
    fields: () => ({
        type: { type: GraphQLString },
        start: { type: GraphQLString },
        end: { type: GraphQLString }
    })
});
export const AnnouncementInputDuration = new GraphQLInputObjectType({
    name: 'AnnouncementInputDuration',
    description: '',
    fields: () => ({
        type: { type: GraphQLString },
        start: { type: GraphQLString },
        end: { type: GraphQLString }
    })
});

export const AnnouncementAvailability = new GraphQLObjectType({
    name: 'AnnouncementAvailability',
    description: '',
    fields: () => ({
        duration: { type: AnnouncementDuration }
    })
});
export const AnnouncementInputAvailability = new GraphQLInputObjectType({
    name: 'AnnouncementInputAvailability',
    description: '',
    fields: () => ({
        duration: { type: AnnouncementInputDuration }
    })
});

export const AnnouncementType = new GraphQLObjectType({
    name: 'AnnouncementType',
    description: 'A Blackboard Announcement Learn Object',
    fields: () => ({
        _id: { type: GraphQLString },
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        body: { type: GraphQLString },
        availability: { type: AnnouncementAvailability },
        showAtLogin: { type: GraphQLBoolean },
        showInCourses: { type: GraphQLBoolean },
        created: { type: GraphQLString }
    })
});

export const AnnouncementsInputFilter = new GraphQLInputObjectType({
    name: 'AnnouncementsInputFilter',
    description: 'A JSON like object to filter our announcements',
    fields: () => ({
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        body: { type: GraphQLString },
        availability: { type: AnnouncementInputAvailability },
        showAtLogin: { type: GraphQLBoolean },
        showInCourses: { type: GraphQLBoolean },
        created: { type: GraphQLString }
    })
});

export const AnnouncementsType = new GraphQLObjectType({
    name: 'AnnouncementsType',
    description: 'A list of Blackboard Announcement Learn Objects',
    fields: () => ({
        results: { type: new GraphQLList(AnnouncementType) }
    })
});
