// import Announcements from 'announcements.gql'
// import Announcement from 'announcements.gql'
// import DataSrouces from 'dataSources.gql'
// import DataSrouce from 'dataSources.gql'

export default `
type Query {
    announcements: Announcements
    announcementsById(id: String!): Announcement
    dataSources: DataSources
    dataSourceById(id: String): DataSource
}
`;
