'use strict';
import * as DataLoader from 'dataloader';
import { WdIntStudentReportConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import {
    wdGetStudent,
    wdGetStudents,
    wdGetStudentsByTerm
} from '../../../../modules/dataFromWd';
import { StudentMap } from '../cache-maps/student.map';

async function fetchData(
    key: string,
    term: string,
    action: string,
    refresh: boolean
) {
    let data: any;
    if (action.toUpperCase() === 'ALL') {
        if (term.toUpperCase() !== 'ALL') {
            data = await wdGetStudentsByTerm(term, refresh);
        } else {
            data = await wdGetStudents(term, refresh);
        }
        return data;
    }

    data = await wdGetStudent(key, refresh);
    return data;
}
async function batchStudents(keys: any) {
    // getAllStudents [ 'ALL:ALL:ALL:false' ]
    // getAllStudents [ 'ALL:20193B:ALL:false' ]
    // getStudent     [ 'a1234567:ALL:SINGLE:false' ]
    // getStudents    [ 'a1234567:ALL:USERS:false', 'a1234568:ALL:USERS:false' ]

    console.log('KEYS ===>', keys);
    let refresh = false;
    let term = 'ALL';
    let action = 'SINGLE';

    keys = keys.map((key: any) => {
        term = key[1];
        action = key[2];
        refresh = key[3];
        return key[0];
    });

    console.log('KEYS    ===>', keys);
    console.log('TERM    ===>', term);
    console.log('ACTION  ===>', action);
    console.log('REFRESH ===>', refresh);

    // if the term is all then we need blank it out ''
    let results = keys.map(async (key: string) => {
        let criteria = {};
        let data: any;

        switch (action) {
            case 'SINGLE':
            case 'USERS':
                criteria = { studentId: { $regex: new RegExp(key, 'i') } };
                break;
            case 'ALL':
                term = term === 'ALL' ? '' : term;
                criteria = {};
                break;
        }

        if (refresh) {
            console.log('[ REFRESHING DATA ]');
            return fetchData(key, term, action, refresh);
        }

        console.log('CRITERIA =====>', criteria);

        data = await getDbData(WdIntStudentReportConfig.model, criteria, {
            isMany: true
        });

        console.log('DATABASE =====>', data.length, 'records.');

        if (data.length <= 0) {
            return fetchData(key, term, action, refresh);
        }

        return data;
    });
    console.log('RESULTS ====>', results);
    let _results = [];

    switch (action) {
        case 'SINGLE':
        // console.log('[RESULTS:SINGLE] ==> ', await _results);
        case 'USERS':
            // console.log('[RESULTS:USERS] ==> ', await _results);
            for (let r of results) {
                r = await r;
                WorkdayStudentLoader.prime(r[0].studentId.toLowerCase(), r[0]);
                WorkdayStudentLoader.prime(
                    r[0].institutionalEmail.toLowerCase(),
                    r[0]
                );
                _results.push(r[0]);
            }
            return _results;
        case 'ALL':
            // console.log('[RESULTS:ALL] ==> ', await results);
            for (let r of results) {
                const rs = await r;
                for (let _rs of rs) {
                    _rs = await _rs;

                    WorkdayStudentLoader.prime(
                        _rs.studentId.toLowerCase(),
                        _rs
                    );

                    if (_rs.institutionalEmail) {
                        WorkdayStudentLoader.prime(
                            _rs.institutionalEmail.toLowerCase(),
                            _rs
                        );
                    }
                }
                _results.push(r);
            }
            return _results;
    }
}

export const WorkdayStudentLoader = new DataLoader(batchStudents, {
    cacheKeyFn: (key: any) => {
        if (Array.isArray(key)) {
            switch (key[2]) {
                case 'SINGLE':
                case 'USERS':
                    return key[0].toLowerCase();
                case 'ALL':
                    return key[1];
            }
        }

        return key;
    },
    cache: true,
    cacheMap: new StudentMap()
});
