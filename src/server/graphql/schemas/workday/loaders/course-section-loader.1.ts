'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseSectionsConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { wdGetActiveCourses } from '../../../../modules/dataFromWd';
import { CourseSectionMap } from '../cache-maps/course-section.map';

export const getCourseSectionDataByTerm = async (
    term: string,
    refresh: boolean
) => {
    let data: any;

    if (refresh) {
        data = wdGetActiveCourses([term], refresh);
    } else {
        data = await getDbData(
            WdIntCourseSectionsConfig.model,
            { externalTermId: term },
            { isMany: true }
        );
    }

    if (!refresh && data.length === 0) {
        data = wdGetActiveCourses([term], true);
    }

    WorkdayCourseSectionLoader.prime(term, data);
    return data;
};

async function batchCourseSections(keys: any) {
    let refresh = false;

    keys = keys.map((key: any) => {
        const parts = key.split(':');
        refresh = parts[1].toLowerCase() === 'true';
        return parts[0];
    });

    let data = await getDbData(
        WdIntCourseSectionsConfig.model,
        { externalTermId: { $in: keys } },
        { isMany: true }
    );

    if (refresh || data.length <= 0) {
        data = await wdGetActiveCourses(keys, true);
    }

    let results = [];

    for (let term of keys) {
        const _data = data.filter(
            (courseSection: any) => courseSection.externalTermId === term
        );
        WorkdayCourseSectionLoader.prime(term, _data);
        results.push(_data);
    }

    return results;
}

export const WorkdayCourseSectionLoader = new DataLoader(batchCourseSections, {
    cacheKeyFn: (key: any) => {
        return key.split(':')[0];
    },
    cache: true,
    cacheMap: new CourseSectionMap()
});
