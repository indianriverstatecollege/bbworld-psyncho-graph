'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseDefinitionConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import {
    wdGetCourseDefinition,
    wdGetCourseDefinitionByTerm,
    wdGetCourseDefinitionByTerms
} from '../../../../modules/dataFromWd';
import { CourseDefinitionMap } from '../cache-maps/course-definition.map';
import { any } from 'bluebird';

async function batchCourseDefinitions(keys: any) {
    let data = keys.map(async (key: any) => {
        const [action, term, refresh] = key;
        let _data: any;
        // console.log('[ action, term, refresh]', action, term, refresh);
        switch (action) {
            case 'ALL':
            // All of Term
            case 'TERM':
                // by Terms
                // spread data in to one array
                _data = await getDbData(
                    WdIntCourseDefinitionConfig.model,
                    { externalTermId: term },
                    { isMany: true }
                );

                if (refresh || _data.length <= 0) {
                    _data = await wdGetCourseDefinitionByTerm(term, refresh);
                    // for some reason, the payload isn't real until you re-parse it.....????
                    _data = JSON.parse(JSON.stringify(_data));
                }

                WorkdayCourseDefinitionLoader.prime(term, _data);

                for (let cd of _data) {
                    WorkdayCourseDefinitionLoader.prime(cd.externalId, cd);
                }
                break;
            default:
                // Single Course
                _data = await getDbData(
                    WdIntCourseDefinitionConfig.model,
                    { externalId: action },
                    { isMany: true }
                );

                if (refresh || _data.length <= 0) {
                    _data = await wdGetCourseDefinition(action, term, refresh);
                }

                // console.log(_data);
                // extract out the inner array to be the main
                // ...speading
                _data = JSON.parse(JSON.stringify(_data[0]));
                WorkdayCourseDefinitionLoader.prime(action, _data);
                break;
        }

        return _data;
    });

    data = await Promise.all(data);

    // console.log(data);
    return Promise.all(data);
}

export const WorkdayCourseDefinitionLoader = new DataLoader(
    batchCourseDefinitions,
    {
        cacheKeyFn: (key: any) => {
            if (Array.isArray(key)) {
                switch (key[0]) {
                    case 'ALL':
                    case 'TERM':
                        return key[1];
                    default:
                        return key[0];
                }
            }
            return key;
        },
        cache: true,
        cacheMap: new CourseDefinitionMap()
    }
);
