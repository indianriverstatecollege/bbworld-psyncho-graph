'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseDefinitionConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import {
    wdGetCourseDefinition,
    wdGetCourseDefinitionByTerms
} from '../../../../modules/dataFromWd';
import { CourseDefinitionMap } from '../cache-maps/course-definition.map';

async function batchCourseDefinitions(keys: any) {
    console.log(keys);

    let compress = false;

    console.log(keys);
    console.log(compress);

    if (keys.length === 1 && /\|/gim.test(keys[0])) {
        keys = keys[0].split('|');
        compress = true;
    }

    let results = await keys.map(async (key: any) => {
        console.log(key);

        const parts = key.split(':');
        const refresh = parts[2].toLowerCase() === 'true';
        const term = parts[1];
        const action = parts[0];
        console.log('[ PARTS   ]', parts);
        console.log('[ REFRESH ]', refresh);
        console.log('[ TERM    ]', term);
        console.log('[ ACTION  ]', action);
        let data: any;
        switch (action) {
            case 'ALL':
                // All of Term
                data = await getDbData(
                    WdIntCourseDefinitionConfig.model,
                    { externalTermId: term },
                    { isMany: true }
                );

                break;
            case 'TERM':
                // by Terms
                // spread data in to one array
                data = await getDbData(
                    WdIntCourseDefinitionConfig.model,
                    { externalTermId: term },
                    { isMany: true }
                );

                break;
            default:
                // Single Course
                data = await getDbData(
                    WdIntCourseDefinitionConfig.model,
                    { externalId: action },
                    { isMany: true }
                );
                break;
        }

        if (refresh || data.length <= 0) {
            switch (action) {
                case 'ALL':
                // All of Term
                case 'TERM':
                    // by Terms
                    await wdGetCourseDefinitionByTerms([term], refresh);
                    data = await getDbData(
                        WdIntCourseDefinitionConfig.model,
                        { externalTermId: term },
                        { isMany: true }
                    );
                    WorkdayCourseDefinitionLoader.prime(term, data);
                    break;
                default:
                    // Single Course
                    await wdGetCourseDefinition(action, term, refresh);
                    data = await getDbData(
                        WdIntCourseDefinitionConfig.model,
                        { externalId: action },
                        { isMany: true }
                    );
                    WorkdayCourseDefinitionLoader.prime(action, data);
                    break;
            }
        }

        return data;
    });
    console.log(`
        
        
    RESULTS BEFORE COMPRESS CHECK...!!!...
    
    
    `);
    console.log(results);
    if (compress) {
        results = [results];
    }

    return Promise.all(results);
}

export const WorkdayCourseDefinitionLoader = new DataLoader(
    batchCourseDefinitions,
    {
        cacheKeyFn: (key: any) => key,
        cache: true,
        cacheMap: new CourseDefinitionMap()
    }
);
