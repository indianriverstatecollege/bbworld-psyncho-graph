'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseDefinitionTermConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { wdGetCourseDefinitionByTerms } from '../../../../modules/dataFromWd';
import { CourseDefinitionMap } from '../cache-maps/course-definition.map';

async function batchCourseDefinitions(keys: any) {
    console.log(keys);
    let refresh = false;
    let compress = false;

    keys = keys.map((key: any) => {
        const parts = key.split(':');
        refresh = parts[1].toLowerCase() === 'true';
        return parts[0];
    });
    console.log(keys);
    return;

    let data = await getDbData(
        WdIntCourseDefinitionTermConfig.model,
        { externalTermId: { $in: keys } },
        { isMany: true }
    );

    if (refresh || data.length <= 0) {
        data = await wdGetCourseDefinitionByTerms(keys, true);
    }

    let results = [];

    for (let term of keys) {
        const _data = data.filter(
            (course: any) => course.externalTermId === term
        );
        WorkdayCourseDefinitionTermLoader.prime(term, _data);
        results.push(_data);
    }

    return results;
}

export const WorkdayCourseDefinitionTermLoader = new DataLoader(
    batchCourseDefinitions,
    {
        cacheKeyFn: (key: any) => (Array.isArray(key) ? key.join('|') : key),
        cache: true,
        cacheMap: new CourseDefinitionMap()
    }
);
