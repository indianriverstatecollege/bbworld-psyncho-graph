'use strict';
import * as DataLoader from 'dataloader';
import { WdIntAdvisorReportConfig } from '../../../../configs/workday/workday.config';
import { Integration } from '../../../../modules/classes/integration';

export const getAdvisorReportInfo = (studentId: any, qs: any = null) => {
    const intConfig = { ...WdIntAdvisorReportConfig };
    intConfig.params = {
        studentId
    };

    const integration: Integration = new Integration(intConfig);
    return (integration as any).fetchData();
};

export const WorkdayAdvisorReportLoader = new DataLoader(
    (keys: any) =>
        new Promise(async (resolve: any, reject: any) => {
            const courses = await Promise.all(keys.map(getAdvisorReportInfo));
            if (courses !== [null]) {
                resolve(courses);
            } else {
                reject({
                    errors: {
                        message:
                            'Advisor Reports returned or was assigned to null.'
                    }
                });
            }
        }),
    { cache: true }
);
