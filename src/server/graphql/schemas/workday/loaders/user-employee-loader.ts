'use strict';
import * as DataLoader from 'dataloader';
import { WdIntEmployeeReportConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { wdGetEmployee, wdGetEmployees } from '../../../../modules/dataFromWd';
import { EmployeeMap } from '../cache-maps/employee.map';
import { inspect } from 'util';

async function batchEmployees(keys: any): Promise<any> {
    let refresh = false;
    let by: string;
    let action: any;

    keys = keys.map((key: any) => {
        by = key[1].toLowerCase();
        refresh = key[3];
        action = key[2];
        return key[0];
    });

    let results = [];

    for (let key of keys) {
        let criteria = {};

        switch (by) {
            case 'externalid':
                if (action === 'ALL') {
                    criteria = {};
                } else {
                    criteria = { externalId: key };
                }
                break;
            case 'username':
                criteria = { userName: key };
                break;
            case 'email':
                criteria = { email: key };
                break;
        }

        let data = await getDbData(WdIntEmployeeReportConfig.model, criteria, {
            isMany: true
        });

        if (refresh || data.length <= 0) {
            if (by === 'externalid' && action !== 'ALL') {
                data = await wdGetEmployee(key, true);
            } else {
                data = await wdGetEmployees(true);
            }
        }

        for (let user of await data) {
            WorkdayEmployeeLoader.prime(user.externalId, user);
            WorkdayEmployeeLoader.prime(user.userName, user);
            WorkdayEmployeeLoader.prime(user.email, user);
        }

        results.push(data);
    }

    switch (action) {
        case 'SINGLE':
            results = results[0];
            break;
        case 'USERS':
            results = [].concat(...results);
            break;
        default:
            return Promise.all(results);
    }

    return Promise.all(results);
}

export const WorkdayEmployeeLoader = new DataLoader(batchEmployees, {
    cacheKeyFn: (key: any) => {
        if (Array.isArray(key)) {
            return key[0];
        }
        return key;
    },
    cache: true,
    cacheMap: new EmployeeMap()
});
