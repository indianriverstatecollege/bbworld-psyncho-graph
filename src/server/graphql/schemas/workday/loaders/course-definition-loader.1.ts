'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseDefinitionConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { wdGetCourseDefinition } from '../../../../modules/dataFromWd';
import { CourseDefinitionMap } from '../cache-maps/course-definition.map';

async function batchCourseDefinitions(keys: any) {
    let refresh = false;
    let term = '';

    keys = keys.map((key: any) => {
        const parts = key.split(':');
        refresh = parts[2].toLowerCase() === 'true';
        term = parts[1];
        return parts[0];
    });

    let data = await getDbData(
        WdIntCourseDefinitionConfig.model,
        { externalId: { $in: keys } },
        { isMany: true }
    );

    if (!refresh && data.length > 0) {
        for (let i = 0; i <= keys.length - 1; i++) {
            WorkdayCourseDefinitionLoader.prime(keys[i], [data[i]]);
        }
        return Promise.resolve(data);
    }

    let results = [];

    for (let course of keys) {
        const data = wdGetCourseDefinition(course, term, true);
        WorkdayCourseDefinitionLoader.prime(course, data);
        results.push(Promise.resolve(data));
    }

    return Promise.resolve(results);
}

export const WorkdayCourseDefinitionLoader = new DataLoader(
    batchCourseDefinitions,
    {
        cacheKeyFn: (key: any) => key.split(':')[0],
        cache: true,
        cacheMap: new CourseDefinitionMap()
    }
);
