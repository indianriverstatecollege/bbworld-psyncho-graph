'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseInformationConfig } from '../../../../configs/workday/workday.config';
import { Integration } from '../../../../modules/classes/integration';
import { CourseInformationMap } from '../cache-maps/course-information.map';

export const getCourseInfo = async (filter: any = '') => {
    const intConfig = { ...WdIntCourseInformationConfig };
    const integration: Integration = new Integration(intConfig);

    if (filter === '') {
        return (integration as any).fetchData();
    }

    return (integration as any).fetchData().then((data: any) => {
        data.Report_Entry = data.Report_Entry.filter((el: any) => {
            let res = false;
            for (const key of Object.keys(filter)) {
                res = RegExp(`${filter[key]}`, 'gi').test(el[key]);
                // if one of the filters does not match, stop
                if (!res) {
                    break;
                }
            }
            return res;
        });
        return data;
    });
};

export const WorkdayCourseInfoLoader = new DataLoader(
    (keys: any) =>
        new Promise(async (resolve: any, reject: any) => {
            const data = await Promise.all(keys.map(getCourseInfo));
            if (data !== [null]) {
                resolve(data);
            } else {
                reject({
                    errors: {
                        message:
                            'Course Information returned or was assigned to null.'
                    }
                });
            }
        }),
    {
        cacheKeyFn: (key: any) => JSON.stringify(key),
        cache: true,
        cacheMap: new CourseInformationMap()
    }
);
