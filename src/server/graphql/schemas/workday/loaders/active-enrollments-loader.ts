'use strict';
import * as DataLoader from 'dataloader';
import { WdIntRegStudentCSConfig } from '../../../../configs/workday/workday.config';
import { Integration } from '../../../../modules/classes/integration';

export const getWorkdayCourseEnrollments = (
    externalCourseId: any,
    qs: any = null
) => {
    const intConfig = { ...WdIntRegStudentCSConfig };
    const courseParts = externalCourseId.split('-');
    const _course = courseParts[0];
    const term = courseParts[1];
    const campus = courseParts[2];
    const session = courseParts[3];
    const section = courseParts[4];
    const courseId = `${_course}-${campus}-${section}`;
    const academicPeriod = term.toString() + (session === 'M' ? '' : session);

    intConfig.params = {
        courseId,
        academicPeriod
    };

    const integration: Integration = new Integration(intConfig);
    return (integration as any).fetchData();
};

export const WorkdayActiveEnrollmentsLoader = new DataLoader(
    (keys: any) =>
        new Promise(async (resolve: any, reject: any) => {
            const courses = await Promise.all(
                keys.map(getWorkdayCourseEnrollments)
            );
            if (courses !== [null]) {
                resolve(courses);
            } else {
                reject({
                    errors: {
                        message:
                            'Active Course Enrollments returned or was assigned to null.'
                    }
                });
            }
        }),
    { cache: true }
);
