'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseDefinitionConfig } from '../../../../configs/workday/workday.config';
import { Integration } from '../../../../modules/classes/integration';
import { CourseDefinitionMap } from '../cache-maps/course-definition.map';
import { CourseListingMap } from '../cache-maps/course-listing.map';

export const getCourseListingInfo = (sectionDefinition: any, term: any) => {
    const intConfig = { ...WdIntCourseDefinitionConfig };
    intConfig.params = {
        sectionDefinition,
        term
    };

    const integration: Integration = new Integration(intConfig);
    return (integration as any).fetchData();
};

export const WorkdayCourseListingLoader = new DataLoader(
    (keys: any) =>
        new Promise(async (resolve: any, reject: any) => {
            const data = await Promise.all(
                keys.map((key: any) =>
                    getCourseListingInfo(key.sectionDefinition, key.term)
                )
            );
            if (data !== [null]) {
                resolve(data);
            } else {
                reject({
                    errors: {
                        message:
                            'Course Listing Info returned or was assigned to null.'
                    }
                });
            }
        }),
    {
        cacheKeyFn: (key: any) => JSON.stringify(key),
        cache: true,
        cacheMap: new CourseListingMap()
    }
);
