'use strict';
import * as DataLoader from 'dataloader';
import { WdIntEducationalInstConfig } from '../../../../configs/workday/workday.config';
import { Integration } from '../../../../modules/classes/integration';
import { EducationalInstitutionMap } from '../cache-maps/educational-institutions.map';

export const getEducationalInstInfo = async (filter: any = '') => {
    const intConfig = { ...WdIntEducationalInstConfig };
    const integration: Integration = new Integration(intConfig);

    if (filter === '') {
        return (integration as any).fetchData();
    }

    return (integration as any).fetchData().then((data: any) => {
        data.Report_Entry = data.Report_Entry.filter((el: any) => {
            let res = false;
            for (const key of Object.keys(filter)) {
                res = RegExp(`${filter[key]}`, 'gi').test(el[key]);
                // if one of the filters does not match, stop
                if (!res) {
                    break;
                }
            }
            return res;
        });
        return data;
    });
};

export const WorkdayEducationalInstLoader = new DataLoader(
    (keys: any) =>
        new Promise(async (resolve: any, reject: any) => {
            const data = await Promise.all(keys.map(getEducationalInstInfo));
            if (data !== [null]) {
                resolve(data);
            } else {
                reject({
                    errors: {
                        message:
                            'Educational Institutions returned or was assigned to null.'
                    }
                });
            }
        }),
    {
        cacheKeyFn: (key: any) => JSON.stringify(key),
        cache: true,
        cacheMap: new EducationalInstitutionMap()
    }
);
