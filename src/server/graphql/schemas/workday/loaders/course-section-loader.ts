'use strict';
import * as DataLoader from 'dataloader';
import { WdIntCourseSectionsConfig } from '../../../../configs/workday/workday.config';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { wdGetActiveCourses } from '../../../../modules/dataFromWd';
import { CourseSectionMap } from '../cache-maps/course-section.map';

async function batchCourseSections(keys: any) {
    let refresh = false;

    keys = keys.map((key: any) => {
        refresh = key[1];
        return key[0];
    });

    let data = await getDbData(
        WdIntCourseSectionsConfig.model,
        { externalTermId: { $in: keys } },
        { isMany: true }
    );

    if (refresh || data.length <= 0) {
        data = await wdGetActiveCourses(keys, true);
    }

    let results = [];

    for (let term of keys) {
        const _data = data.filter(
            (courseSection: any) => courseSection.externalTermId === term
        );
        WorkdayCourseSectionLoader.prime(term, _data);
        results.push(_data);
    }

    return results;
}

export const WorkdayCourseSectionLoader = new DataLoader(batchCourseSections, {
    cacheKeyFn: (key: any) => key[0],
    cache: true,
    cacheMap: new CourseSectionMap()
});
