import {
    getDbActiveCourses,
    getDbActiveCoursesByTerm
} from './courses/getActiveCourses.resolver';

import {
    getCourseDefinitions,
    getCourseDefinitionsByTerms,
    getCourseDefinitionByExternalId
} from './courses/getCourseDefinitions.resolver';

import {
    getEmployee,
    getEmployees,
    getAllEmployees
} from './users/getEmployees.resolver';

import {
    getStudent,
    getStudents,
    getAllStudents
} from './users/getStudents.resolver';

export const wdResolverMap = (io: any) => ({
    hello: () => 'Hello world!',
    getDbActiveCourses,
    getDbActiveCoursesByTerm,
    getCourseDefinitions,
    getCourseDefinitionsByTerms,
    getCourseDefinitionByExternalId,
    getEmployee,
    getEmployees,
    getAllEmployees,
    getStudent,
    getStudents,
    getAllStudents,
    listCollections(args: any, { db, io }, info: any) {
        const collections = Object.keys(db.collections);
        io.emit('database:collections', collections);
        return collections;
    }
});
