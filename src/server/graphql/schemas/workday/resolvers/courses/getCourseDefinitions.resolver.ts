export async function getCourseDefinitions(args: any, context: any, info: any) {
    if (args.refresh) {
        context.WorkdayCourseDefinitionLoader.clear(args.term);
    }

    return context.WorkdayCourseDefinitionLoader.load([
        'ALL',
        args.term,
        args.refresh
    ]);
}

export async function getCourseDefinitionsByTerms(
    args: any,
    context: any,
    info: any
) {
    if (args.refresh) {
        for (let term of args.terms) {
            context.WorkdayCourseDefinitionLoader.clear(term);
        }
    }

    return context.WorkdayCourseDefinitionLoader.loadMany(
        args.terms.map((term: any) => ['TERM', term, args.refresh])
    );
}

export async function getCourseDefinitionByExternalId(
    args: any,
    context: any,
    info: any
) {
    if (args.refresh) {
        context.WorkdayCourseDefinitionLoader.clear(args.externalId);
    }

    return context.WorkdayCourseDefinitionLoader.load([
        args.externalId,
        args.term,
        args.refresh
    ]);
}
