export async function getDbActiveCourses(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let term of args.terms) {
            context.WorkdayCourseSectionLoader.clear(term);
        }
    }

    return context.WorkdayCourseSectionLoader.loadMany(
        args.terms.map((term: any) => [term, args.refresh])
    );
}

export async function getDbActiveCoursesByTerm(
    args: any,
    context: any,
    info: any
) {
    if (args.refresh) {
        context.WorkdayCourseSectionLoader.clear(args.term);
    }

    return context.WorkdayCourseSectionLoader.load([args.term, args.refresh]);
}
