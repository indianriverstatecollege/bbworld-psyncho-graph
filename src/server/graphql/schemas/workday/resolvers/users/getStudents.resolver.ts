export async function getAllStudents(args: any, context: any, info: any) {
    if (args.refresh) {
        context.WorkdayStudentLoader.clearAll();
    }
    const term = args.term || 'ALL';
    return context.WorkdayStudentLoader.load([
        'ALL',
        term,
        'ALL',
        args.refresh
    ]);
}

export async function getStudents(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let user of args.users) {
            context.WorkdayStudentLoader.clear(user);
        }
        // TODO: Clearing cache isn't working so just
        // clearing out all for now on this resolver
        context.WorkdayStudentLoader.clearAll();
    }
    const term = args.term || 'ALL';
    return context.WorkdayStudentLoader.loadMany(
        args.users.map((user: any) => [user, term, 'USERS', args.refresh])
    );
}

export async function getStudent(args: any, context: any, info: any) {
    if (args.refresh) {
        context.WorkdayStudentLoader.clear(args.user);
    }
    return context.WorkdayStudentLoader.load([
        args.user,
        'ALL',
        'SINGLE',
        args.refresh
    ]);
}
