export async function getAllEmployees(args: any, context: any, info: any) {
    if (args.refresh) {
        context.WorkdayEmployeeLoader.clearAll();
    }

    const by = args.by ? args.by : 'externalId';

    return context.WorkdayEmployeeLoader.load(['ALL', by, 'ALL', args.refresh]);
}

export async function getEmployees(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let user of args.users) {
            context.WorkdayEmployeeLoader.clear(user);
        }
    }

    const by = args.by ? args.by : 'externalId';

    return context.WorkdayEmployeeLoader.loadMany(
        args.users.map((user: any) => [user, by, 'USERS', args.refresh])
    );
}

export async function getEmployee(args: any, context: any, info: any) {
    if (args.refresh) {
        context.WorkdayEmployeeLoader.clear(args.user);
    }
    let by = args.by ? args.by : 'externalId';
    return context.WorkdayEmployeeLoader.load([
        args.user,
        by,
        'SINGLE',
        args.refresh
    ]);
}
