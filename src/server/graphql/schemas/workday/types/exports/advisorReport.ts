'use strict';
import { GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';

const RoleAssignmentsWorkersAssigned = new GraphQLObjectType({
    name: 'RoleAssignmentsWorkersAssigned',
    description: 'A list of Advisors assigned to the student',
    fields: () => ({
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        employeeId: { type: GraphQLString },
        middleName: { type: GraphQLString },
        userName: { type: GraphQLString },
        primaryWorkEmail: { type: GraphQLString },
    })
    
});

const AdvisorReport = new GraphQLObjectType({
    name: 'AdvisorReport',
    description: 'A payload object that workday returns',
    fields: () => ({
        studentId: { type: GraphQLString },
        studentCohortType: { type: GraphQLString },
        student: { type: GraphQLString },
        studentCohort: { type: GraphQLString },
        roleAssignmentsWorkersAssigned: { type: new GraphQLList(RoleAssignmentsWorkersAssigned) },
        roleAssignments: { type: GraphQLString },
    })
});

export const AdvisorReportInfo = new GraphQLObjectType({
    name: 'AdvisorReportInfo',
    description:
        'An object payload from Workday containing the advisor information.',
    fields: () => ({
        Report_Entry: { type: new GraphQLList(AdvisorReport) }
    })
});

// tslint:disable-next-line:no-commented-code
// const CourseInputDuration = new GraphQLInputObjectType({
//   name: 'CourseInputDuration',
//   description: 'A Coures\'s description field.',
//   fields: () => _courseDuration,
// });
