'use strict';
import { GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';

const CourseListing =  new GraphQLObjectType({
    name: 'CourseListing',
    description: 'The course information',
    fields: () => ({
        endDate: { type: GraphQLString },
        enrolledCapacity: { type: GraphQLString },
        section: { type: GraphQLString },
        campusMeetingPattern: { type: GraphQLString },
        shortTitle: { type: GraphQLString },
        title: { type: GraphQLString },
        capacity: { type: GraphQLString },
        instructors: { type: GraphQLString },
        academicYear: { type: GraphQLString },
        term: { type: GraphQLString },
        courseId: { type: GraphQLString },
        delivery: { type: GraphQLString },
        meetingDays: { type: GraphQLString },
        courseTags: { type: GraphQLString },
        courseSectionDefinition: { type: GraphQLString },
        campus: { type: GraphQLString },
        contactHours: { type: GraphQLString },
        maximumUnits: { type: GraphQLString },
        academicLevel: { type: GraphQLString },
        termDates: { type: GraphQLString },
        courseSubject: { type: GraphQLString },
        instructor: { type: GraphQLString },
        publicNotes: { type: GraphQLString },
        startDate: { type: GraphQLString },
        status: { type: GraphQLString },
    })
});


 export const CourseListingInfo = new GraphQLObjectType({
    name: 'CourseListingInfo',
    description:
        'An object payload from Workday containing the advisor information.',
    fields: () => ({
        Report_Entry: { type: new GraphQLList(CourseListing) }
    })
});