'use strict';
import { GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
const CourseInformation = new GraphQLObjectType({
    name: 'CourseInformation',
    description: 'Describes the course information',
    fields: () => ({
        courseTags: { type: GraphQLString },
        prefix: { type: GraphQLString },
        allowedCampuses: { type: GraphQLString },
        workdayReferenceId: { type: GraphQLString },
        description: { type: GraphQLString },
        title: { type: GraphQLString },
        academicUnitsType: { type: GraphQLString },
        academicPeriods: { type: GraphQLString },
        number: { type: GraphQLString },
        academicLevel: { type: GraphQLString },
        workdayReferenceType: { type: GraphQLString },
        courseFormat: { type: GraphQLString },
        courseId: { type: GraphQLString },
        courseIdTitle: { type: GraphQLString }
    })
});

export const CourseInfo = new GraphQLObjectType({
    name: 'CourseInfo',
    description:
        'An object payload from Workday containing the course information.',
    fields: () => ({
        Report_Entry: { type: new GraphQLList(CourseInformation) }
    })
});
