'use strict';
import { GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';

const StudentReport = new GraphQLObjectType({
    name: 'StudentReport',
    description: 'A payload object that workday returns',
    fields: () => ({
        lastName: { type: GraphQLString },
        homePhone: { type: GraphQLString },
        homeAddressLine1: { type: GraphQLString },
        cohort: { type: GraphQLString },
        homeState: { type: GraphQLString },
        birthMonthYear: { type: GraphQLString },
        homePostalCode: { type: GraphQLString },
        studentId: { type: GraphQLString },
        firstName: { type: GraphQLString },
        homeEmail: { type: GraphQLString },
        homeCity: { type: GraphQLString },
        workdayID: { type: GraphQLString },
        middleName: { type: GraphQLString },
        institutionalEmail: { type: GraphQLString }
    })
});

export const StudentReportInfo = new GraphQLObjectType({
    name: 'StudentReportInfo',
    description:
        'An object payload from Workday containing the student information.',
    fields: () => ({
        Report_Entry: { type: new GraphQLList(StudentReport) }
    })
});

// tslint:disable-next-line:no-commented-code
// const CourseInputDuration = new GraphQLInputObjectType({
//   name: 'CourseInputDuration',
//   description: 'A Coures\'s description field.',
//   fields: () => _courseDuration,
// });
