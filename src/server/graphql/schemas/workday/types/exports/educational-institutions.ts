'use strict';
import { GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';

const EducationalInstitution = new GraphQLObjectType({
    name: 'EducationalInstitution',
    description: 'A payload object that workday returns',
    fields: () => ({
        studentElectronicID: { type: GraphQLString },
        country: { type: GraphQLString },
        studentElectronicMessageType: { type: GraphQLString },
        institutionLevel: { type: GraphQLString },
        addressLn1: { type: GraphQLString },
        city: { type: GraphQLString },
        institutionName: { type: GraphQLString },
        postalCode: { type: GraphQLString },
        institutionType: { type: GraphQLString },
        state: { type: GraphQLString }
    })
});

export const EducationalInstitutionInfo = new GraphQLObjectType({
    name: 'EducationalInstitutionInfo',
    description:
        'An object payload from Workday containing the student information.',
    fields: () => ({
        Report_Entry: { type: new GraphQLList(EducationalInstitution) }
    })
});

// tslint:disable-next-line:no-commented-code
// const CourseInputDuration = new GraphQLInputObjectType({
//   name: 'CourseInputDuration',
//   description: 'A Coures\'s description field.',
//   fields: () => _courseDuration,
// });
