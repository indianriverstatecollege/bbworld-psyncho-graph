'use strict';
import {
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

const CourseEnrollment = new GraphQLObjectType({
  name: 'CourseEnrollment',
  description: 'A Workday course enrollment object',
  fields: () => ({
    studentId: { type: GraphQLString },
    studentName: { type: GraphQLString },
    status: { type: GraphQLString },
  }),
});

const CourseEnrollments = new GraphQLObjectType({
  name: 'CourseEnrollments',
  description: 'A payload object that workday returns',
  fields: () => ({
    externalId: { type: GraphQLString },
    primaryInstructorEmail: { type: GraphQLString },
    availability: { type: GraphQLString },
    enrollments: { type: new GraphQLList(CourseEnrollment)},
  }),
});

export const ActiveCourseEnrollments = new GraphQLObjectType({
  name: 'ActiveCourseEnrollments',
  description: 'An object payload from Workday containing the current course roster.',
  fields: () => ({
    Report_Entry: {type: new GraphQLList(CourseEnrollments)},
  }),
});

// tslint:disable-next-line:no-commented-code
// const CourseInputDuration = new GraphQLInputObjectType({
//   name: 'CourseInputDuration',
//   description: 'A Coures\'s description field.',
//   fields: () => _courseDuration,
// });
