import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class Enrollment {
    @Field()
    studentId: string;
    
    @Field()
    studentName: string;

    @Field()
    status: string;
}
 
@ObjectType()
export class ActiveErollments {
    @Field()
    externalId: string;
    
    @Field()
    primaryInstructorEmail: string;

    @Field()
    availability: string;

    @Field(type => [Enrollment])
    enrollments: Enrollment[];
}