import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class StudentReport {
    @Field()
    public homePhone: string;

    @Field()
    public homeAddressLine1: string;

    @Field()
    public cohort: string;

    @Field()
    public homeState: string;

    @Field()
    public birthMonthYear: string;

    @Field()
    public homePostalCode: string;

    @Field()
    public studentId: string;

    @Field()
    public firstName: string;

    @Field()
    public homeEmail: string;

    @Field()
    public homeCity: string;

    @Field()
    public workdayID: string;

    @Field()
    public middleName: string;

    @Field()
    public institutionalEmail: string;
}
