import { GraphQLBoolean, GraphQLNonNull, GraphQLString } from 'graphql';
import { CourseListingInfo } from '../types/exports/course-list';

export const courseListingInfoQuery = {
    type: CourseListingInfo,
    description: `Get the course listing information via Workday`,
    args: {
        sectionDefinition: { type: new GraphQLNonNull(GraphQLString) },
        term: { type: new GraphQLNonNull(GraphQLString) },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            // ctx.WorkdayCourseListingLoader.clear(args.id);
            // TODO: look into cacheKeyFn for dataloader options to
            // be able to clear an single cached reference
            // clearing all for now as id won't work
            ctx.WorkdayCourseListingLoader.clear({
                sectionDefinition: args.sectionDefinition,
                term: args.term
            });
        }

        // TODO: this does NOT cache the load!!
        // look into cacheKeyFn!!
        return ctx.WorkdayCourseListingLoader.load({
            sectionDefinition: args.sectionDefinition,
            term: args.term
        });
    }
};
