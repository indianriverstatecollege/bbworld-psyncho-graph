import {
    GraphQLBoolean,
    GraphQLNonNull,
    GraphQLString,
    GraphQLInputObjectType
} from 'graphql';
import { CourseInfo } from '../types/exports/course-information';
const CourseInformationInputFilter = new GraphQLInputObjectType({
    name: 'CourseInformationInputFilter',
    description:
        'Accepts a JSON object to that will applied to filter out the results',
    fields: () => ({
        courseTags: { type: GraphQLString },
        prefix: { type: GraphQLString },
        allowedCampuses: { type: GraphQLString },
        workdayReferenceId: { type: GraphQLString },
        description: { type: GraphQLString },
        title: { type: GraphQLString },
        academicUnitsType: { type: GraphQLString },
        academicPeriods: { type: GraphQLString },
        number: { type: GraphQLString },
        academicLevel: { type: GraphQLString },
        workdayReferenceType: { type: GraphQLString },
        courseFormat: { type: GraphQLString },
        courseId: { type: GraphQLString },
        courseIdTitle: { type: GraphQLString }
    })
});

export const courseInfoQuery = {
    type: CourseInfo,
    description: `Get the educational institutions information via Workday`,
    args: {
        filter: { type: CourseInformationInputFilter },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            ctx.WorkdayCourseInfoLoader.clear(args.filter || '');
        }
        return ctx.WorkdayCourseInfoLoader.load(args.filter || '');
    }
};
