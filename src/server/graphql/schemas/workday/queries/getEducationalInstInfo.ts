import {
    GraphQLBoolean,
    GraphQLNonNull,
    GraphQLString,
    GraphQLInputObjectType
} from 'graphql';
import { EducationalInstitutionInfo } from '../types/exports/educational-institutions';
const EducationalInstitutionInputFilter = new GraphQLInputObjectType({
    name: 'EducationalInstitutionInputFilter',
    description:
        'Accepts a JSON object to that will applied to filter out the results',
    fields: () => ({
        studentElectronicID: { type: GraphQLString },
        country: { type: GraphQLString },
        studentElectronicMessageType: { type: GraphQLString },
        institutionLevel: { type: GraphQLString },
        addressLn1: { type: GraphQLString },
        city: { type: GraphQLString },
        institutionName: { type: GraphQLString },
        postalCode: { type: GraphQLString },
        institutionType: { type: GraphQLString },
        state: { type: GraphQLString }
    })
});

export const educationalInstInfoQuery = {
    type: EducationalInstitutionInfo,
    description: `Get the educational institutions information via Workday`,
    args: {
        filter: { type: EducationalInstitutionInputFilter },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            ctx.WorkdayEducationalInstLoader.clear(args.filter || '');
        }
        return ctx.WorkdayEducationalInstLoader.load(args.filter || '');
    }
};
