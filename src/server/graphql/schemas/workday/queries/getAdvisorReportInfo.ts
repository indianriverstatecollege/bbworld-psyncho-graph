import { GraphQLBoolean, GraphQLNonNull, GraphQLString } from 'graphql';
import { AdvisorReportInfo } from '../types/exports/advisorReport';

export const advisorReportInfoQuery = {
    type: AdvisorReportInfo,
    description: `Get the student / advisor information via Workday`,
    args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            ctx.WorkdayAdvisorReportLoader.clear(args.id);
        }
        return ctx.WorkdayAdvisorReportLoader.load(args.id);
    }
};
