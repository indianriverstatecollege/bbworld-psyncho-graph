import { GraphQLBoolean, GraphQLNonNull, GraphQLString } from 'graphql';
import { StudentReportInfo } from '../types/exports/studentReport';

export const studentReportInfoQuery = {
    type: StudentReportInfo,
    description: `Get the student information via Workday`,
    args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            ctx.WorkdayStudentReportLoader.clear(args.id);
        }
        return ctx.WorkdayStudentReportLoader.load(args.id);
    }
};
