import { GraphQLBoolean, GraphQLNonNull, GraphQLString } from 'graphql';
import { ActiveCourseEnrollments } from '../types/exports/course';

export const activeCourseEnrollmentsByIdQuery = {
    type: ActiveCourseEnrollments,
    description: `Get the active enrollments for a course via Workday`,
    args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            ctx.WorkdayActiveEnrollmentsLoader.clear(args.id);
        }
        return ctx.WorkdayActiveEnrollmentsLoader.load(args.id);
    }
};
