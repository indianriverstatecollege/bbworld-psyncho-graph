import { CacheMap } from '../../../modules/classes/cache-map';

export const cacheMapFactory = (name: string) =>
    // tslint:disable-next-line
    new (require('safe-eval')(`(class ${name} extends CacheMap {})`, {
        CacheMap
    }))();
