'use strict';
import * as DataLoader from 'dataloader';
import { IResolverArgsOptions } from './resolverFactory';

interface ILoaderKeys {
    input: any;
    options: IResolverArgsOptions;
}
interface ILoaderConfig {
    integration: any;
    errorMessage: string;
    cache: boolean;
    cacheCheckFn: any;
    cacheMap: any;
}

export const loaderFactory = (config: ILoaderConfig) => {
    const integration = config.integration;
    return new DataLoader(
        (keys: any) =>
            new Promise(async (resolve: any, reject: any) => {
                const results = await Promise.all(
                    keys.map((data: ILoaderKeys) => {
                        if (data.input) {
                            integration.params = data.input;
                        }

                        switch (data.options.resolverAction) {
                            case 'create':
                                return integration.createData(data);
                            case 'read':
                                return integration.fetchData(data);
                            case 'update':
                                return integration.updateData(data);
                            case 'delete':
                                return integration.removeData(data);
                        }
                    })
                );
                if (results !== [null]) {
                    resolve(results);
                } else {
                    reject({
                        errors: {
                            message: config.errorMessage
                        }
                    });
                }
            }),
        {
            cacheKeyFn: (key: any) => JSON.stringify(key),
            cache: config.cache,
            cacheMap: config.cacheMap
        }
    );
};
