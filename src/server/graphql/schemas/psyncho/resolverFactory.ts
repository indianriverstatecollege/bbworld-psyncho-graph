import { GraphQLObjectType } from 'graphql';

enum EresolverCacheActions {
    force = 'force',
    refresh = 'refresh',
    keep = 'keep'
}

enum EResolverLoadFuncs {
    load = 'load',
    loadMany = 'loadMany',
    empty = ''
}

enum EResolverMethod {
    mutatation = 'mutatation',
    query = 'query'
}
enum EResolverCacheFunc {
    clear = 'clear',
    clearAll = 'clearAll'
}

export interface IResolverConfig {
    type: GraphQLObjectType;
    description: string;
    args: any;
    resolverMethod: EResolverMethod;
    resolverType: string;
}

enum EResolverActions {
    create = 'create',
    read = 'read',
    update = 'update',
    delete = 'delete'
}

export interface IResolverArgsOptions {
    resolverAction: EResolverActions;
    resolverCache?: EresolverCacheActions;
    resolverCacheFunc?: EResolverCacheFunc;
    resolverLoadFunc?: EResolverLoadFuncs;
}

interface IResolverArgs {
    input: any;
    options: IResolverArgsOptions;
}

export const graphQLResolverFactory: any = (config: IResolverConfig) => ({
    type: config.type,
    description: config.description,
    args: config.args,
    resolve: (_: any, args: IResolverArgs, ctx: any, info: any) => {
        if (
            args.options.resolverCache === EresolverCacheActions.refresh ||
            args.options.resolverCache === EresolverCacheActions.force
        ) {
            ctx[config.resolverType][args.options.resolverCacheFunc]();
        }

        if (config.resolverMethod === EResolverMethod.mutatation) {
            return ctx[config.resolverType](args);
        }

        return ctx[config.resolverType][args.options.resolverLoadFunc](args);
    }
});
