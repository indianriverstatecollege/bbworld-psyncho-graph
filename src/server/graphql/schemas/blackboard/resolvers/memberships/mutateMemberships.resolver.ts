import {
    createBbMembership as create,
    updateBbMembership as update,
    deleteBbMembership as remove
} from '../../../../../modules/dataFromAndToBb';

export async function createBbMembership(
    { courseId, userId, input }: any,
    context: any
) {
    console.log(input);
    let data: any = await create(courseId, userId, input);
    context.BbMembershipsLoader.prime(`${data.courseId}-${data.userId}`, data);
    context.BbMembershipsLoader.prime(`${courseId}-${userId}`, data);
    return data;
}

export async function updateBbMembership(
    { courseId, userId, input }: any,
    context: any
) {
    console.log(userId);
    console.log(input);
    let data: any = await update(courseId, userId, input);
    context.BbMembershipsLoader.prime(`${data.courseId}-${data.userId}`, data);
    context.BbMembershipsLoader.prime(`${courseId}-${userId}`, data);
    return data;
}

export async function deleteBbMembership(
    { courseId, userId }: any,
    context: any
) {
    console.log(userId);
    let msg: any = await remove(courseId, userId);
    context.BbMembershipsLoader.clear(`${courseId}-${userId}`);
    return msg;
}
