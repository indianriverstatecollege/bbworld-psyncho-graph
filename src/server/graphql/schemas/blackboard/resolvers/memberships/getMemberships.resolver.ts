export async function getAllBbMemberships(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbMembershipsLoader.clear(args.courseId);
    }

    return context.BbMembershipsLoader.load([
        'ALL',
        args.courseId,
        'ALL',
        args.refresh
    ]);
}

export async function getBbMemberships(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let userId of args.userIds) {
            context.BbMembershipsLoader.clear(`${args.courseId}-${userId}`);
        }
    }

    return context.BbMembershipsLoader.loadMany(
        args.userIds.map((userId: any) => [
            'MEMBERSHIP',
            args.courseId,
            userId,
            args.refresh
        ])
    );
}

export async function getBbMembership(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbMembershipsLoader.clear(`${args.courseId}-${args.userId}`);
    }

    return context.BbMembershipsLoader.load([
        'SINGLE',
        args.courseId,
        args.userId,
        args.refresh
    ]);
}
