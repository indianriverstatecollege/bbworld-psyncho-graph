export async function getAllBbDataSources(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbDataSourcesLoader.clearAll();
    }

    return context.BbDataSourcesLoader.load(['ALL', 'ALL', args.refresh]);
}

export async function getBbDataSources(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let dataSourceId of args.dataSourceIds) {
            context.BbDataSourcesLoader.clear(dataSourceId);
        }
    }

    return context.BbDataSourcesLoader.loadMany(
        args.dataSourceIds.map((dataSourceId: any) => [
            'DATASOURCE',
            dataSourceId,
            args.refresh
        ])
    );
}

export async function getBbDataSource(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbDataSourcesLoader.clear(args.dataSourceId);
    }

    return context.BbDataSourcesLoader.load([
        'SINGLE',
        args.dataSourceId,
        args.refresh
    ]);
}
