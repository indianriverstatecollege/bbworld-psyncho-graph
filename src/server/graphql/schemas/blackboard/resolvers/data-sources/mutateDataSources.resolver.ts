import {
    createBbDataSource as create,
    updateBbDataSource as update,
    deleteBbDataSource as remove
} from '../../../../../modules/dataFromAndToBb';

export async function createBbDataSource({ input }: any, context: any) {
    console.log(input);
    let data: any = await create(input);
    context.BbDataSourcesLoader.prime(data.id, data);
    context.BbDataSourcesLoader.prime(data.externalId, data);
    return data;
}

export async function updateBbDataSource(
    { dataSourceId, input }: any,
    context: any
) {
    console.log(dataSourceId);
    console.log(input);
    let data: any = await update(dataSourceId, input);
    context.BbDataSourcesLoader.prime(data.id, data);
    context.BbDataSourcesLoader.prime(data.externalId, data);
    return data;
}

export async function deleteBbDataSource({ dataSourceId }: any, context: any) {
    console.log(dataSourceId);
    let msg: any = await remove(dataSourceId);
    context.BbDataSourcesLoader.clear(dataSourceId);
    return msg;
}
