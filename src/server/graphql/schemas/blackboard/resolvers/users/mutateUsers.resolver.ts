import {
    createBbUser as create,
    updateBbUser as update,
    deleteBbUser as remove
} from '../../../../../modules/dataFromAndToBb';

export async function createBbUser({ input }: any, context: any) {
    console.log(input);
    let data: any = await create(input);
    context.BbUsersLoader.prime(data.id, data);
    context.BbUsersLoader.prime(data.externalId, data);
    return data;
}

export async function updateBbUser({ userId, input }: any, context: any) {
    console.log(userId);
    console.log(input);
    let data: any = await update(userId, input);
    context.BbUsersLoader.prime(data.id, data);
    context.BbUsersLoader.prime(data.externalId, data);
    return data;
}

export async function deleteBbUser({ userId }: any, context: any) {
    console.log(userId);
    let msg: any = await remove(userId);
    context.BbUsersLoader.clear(userId);
    return msg;
}
