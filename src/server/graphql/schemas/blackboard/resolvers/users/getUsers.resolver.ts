export async function getAllBbUsers(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbUsersLoader.clearAll();
    }

    return context.BbUsersLoader.load(['ALL', 'ALL', args.refresh]);
}

export async function getBbUsers(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let userId of args.userIds) {
            context.BbUsersLoader.clear(userId);
        }
    }

    return context.BbUsersLoader.loadMany(
        args.userIds.map((userId: any) => ['USER', userId, args.refresh])
    );
}

export async function getBbUser(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbUsersLoader.clear(args.userId);
    }

    return context.BbUsersLoader.load(['SINGLE', args.userId, args.refresh]);
}
