import {
    createBbCourse as create,
    updateBbCourse as update,
    deleteBbCourse as remove
} from '../../../../../modules/dataFromAndToBb';

export async function createBbCourse({ input }: any, context: any) {
    console.log(input);
    let data: any = await create(input);
    context.BbCoursesLoader.prime(data.id, data);
    context.BbCoursesLoader.prime(data.externalId, data);
    return data;
}

export async function updateBbCourse({ courseId, input }: any, context: any) {
    console.log(courseId);
    console.log(input);
    let data: any = await update(courseId, input);
    context.BbCoursesLoader.prime(data.id, data);
    context.BbCoursesLoader.prime(data.externalId, data);
    return data;
}

export async function deleteBbCourse({ courseId }: any, context: any) {
    console.log(courseId);
    let msg: any = await remove(courseId);
    context.BbCoursesLoader.clear(courseId);
    return msg;
}
