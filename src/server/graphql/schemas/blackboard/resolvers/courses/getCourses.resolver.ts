export async function getAllBbCourses(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbCoursesLoader.clearAll();
    }

    return context.BbCoursesLoader.load(['ALL', 'ALL', args.refresh]);
}

export async function getBbCourses(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let courseId of args.courseIds) {
            context.BbCoursesLoader.clear(courseId);
        }
    }

    return context.BbCoursesLoader.loadMany(
        args.courseIds.map((courseId: any) => [
            'COURSE',
            courseId,
            args.refresh
        ])
    );
}

export async function getBbCourse(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbCoursesLoader.clear(args.courseId);
        if (/:/i.test(args.courseId)) {
            const id = args.courseId.split(':')[1];
            context.BbCoursesLoader.clear(id);
        }
    }

    return context.BbCoursesLoader.load([
        'SINGLE',
        args.courseId,
        args.refresh
    ]);
}

export const BbCourse = {
    id: ({ id }: any) => `WHAT ${id} THE HELLLLL!!!`,
    uuid: ({ uuid }: any) => uuid,
    externalId: ({ externalId }: any) => externalId,
    dataSourceId: ({ dataSourceId }: any) => dataSourceId,
    // dataSource: ({ dataSourceId }: any) =>
    //     context.BbDataSourcesLoader.load(dataSourceId),
    courseId: ({ courseId }: any) => courseId,
    name: ({ name }: any) => name,
    description: ({ description }: any) => description,
    created: ({ created }: any) => created,
    organization: ({ organization }: any) => organization,
    ultraStatus: ({ ultraStatus }: any) => ultraStatus,
    allowGuests: ({ allowGuests }: any) => allowGuests,
    readOnly: ({ readOnly }: any) => readOnly,
    termId: ({ termId }: any) => termId,
    // term: ({ termId }: any) => context.BbTermsLoader.load(termId),
    availability: ({ availability }: any) => availability,
    enrollment: ({ enrollment }: any) => enrollment,
    locale: ({ locale }: any) => locale,
    hasChildren: ({ hasChildren }: any) => hasChildren,
    parentId: ({ parentId }: any) => parentId,
    externalAccessUrl: ({ externalAccessUrl }: any, args: any, context: any) =>
        externalAccessUrl,
    guestAccessUrl: ({ guestAccessUrl }: any) => guestAccessUrl
};
