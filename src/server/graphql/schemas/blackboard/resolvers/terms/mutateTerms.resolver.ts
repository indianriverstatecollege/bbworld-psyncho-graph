import {
    createBbTerm as create,
    updateBbTerm as update,
    deleteBbTerm as remove
} from '../../../../../modules/dataFromAndToBb';

export async function createBbTerm({ input }: any, context: any) {
    console.log(input);
    let data: any = await create(input);
    context.BbTermsLoader.prime(data.id, data);
    context.BbTermsLoader.prime(data.externalId, data);
    return data;
}

export async function updateBbTerm({ termId, input }: any, context: any) {
    console.log(termId);
    console.log(input);
    let data: any = await update(termId, input);
    context.BbTermsLoader.prime(data.id, data);
    context.BbTermsLoader.prime(data.externalId, data);
    return data;
}

export async function deleteBbTerm({ termId }: any, context: any) {
    console.log(termId);
    let msg: any = await remove(termId);
    context.BbTermsLoader.clear(termId);
    return msg;
}
