export async function getAllBbTerms(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbTermsLoader.clearAll();
    }

    return context.BbTermsLoader.load(['ALL', 'ALL', args.refresh]);
}

export async function getBbTerms(args: any, context: any, info: any) {
    if (args.refresh) {
        for (let termId of args.termIds) {
            context.BbTermsLoader.clear(termId);
        }
    }

    return context.BbTermsLoader.loadMany(
        args.termIds.map((termId: any) => ['TERM', termId, args.refresh])
    );
}

export async function getBbTerm(args: any, context: any, info: any) {
    if (args.refresh) {
        context.BbTermsLoader.clear(args.termId);
    }

    return context.BbTermsLoader.load(['SINGLE', args.termId, args.refresh]);
}
