import {
    getBbDataSource,
    getBbDataSources,
    getAllBbDataSources
} from './data-sources/getDataSources.resolver';

import {
    createBbDataSource,
    updateBbDataSource,
    deleteBbDataSource
} from './data-sources/mutateDataSources.resolver';

import {
    getBbTerm,
    getBbTerms,
    getAllBbTerms
} from './terms/getTerms.resolver';

import {
    createBbTerm,
    updateBbTerm,
    deleteBbTerm
} from './terms/mutateTerms.resolver';

import {
    getBbUser,
    getBbUsers,
    getAllBbUsers
} from './users/getUsers.resolver';

import {
    createBbUser,
    updateBbUser,
    deleteBbUser
} from './users/mutateUsers.resolver';

import {
    getBbCourse,
    getBbCourses,
    getAllBbCourses,
    BbCourse
} from './courses/getCourses.resolver';
import { courseByIdQuery } from './courses/getCourses';
import {
    createBbCourse,
    updateBbCourse,
    deleteBbCourse
} from './courses/mutateCourses.resolver';

import {
    getBbMembership,
    getBbMemberships,
    getAllBbMemberships
} from './memberships/getMemberships.resolver';

import {
    createBbMembership,
    updateBbMembership,
    deleteBbMembership
} from './memberships/mutateMemberships.resolver';

export const bbResolverMap = (io: any) => ({
    getBbDataSource,
    getBbDataSources,
    getAllBbDataSources,
    getBbTerm,
    getBbTerms,
    getAllBbTerms,
    getBbUser,
    getBbUsers,
    getAllBbUsers,
    getBbCourse,
    getBbCourses,
    getAllBbCourses,
    getBbMembership,
    getBbMemberships,
    getAllBbMemberships,
    courseByIdQuery
});

export const bbMutationMap = (io: any) => ({
    createBbDataSource,
    updateBbDataSource,
    deleteBbDataSource,
    createBbTerm,
    updateBbTerm,
    deleteBbTerm,
    createBbUser,
    updateBbUser,
    deleteBbUser,
    createBbCourse,
    updateBbCourse,
    deleteBbCourse,
    createBbMembership,
    updateBbMembership,
    deleteBbMembership
});

export const bbFieldResolvers = (io: any) => ({
    BbCourse
});
