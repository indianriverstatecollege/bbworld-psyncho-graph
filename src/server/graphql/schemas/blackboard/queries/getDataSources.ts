import { GraphQLBoolean, GraphQLString, GraphQLList } from 'graphql';
import {
    DataSourceInputFilter,
    DataSourcesType
} from '../../../examples/exports/dataSource.types';

export const getBbDataSourcesQuery = {
    type: DataSourcesType,
    description: `Get the data sources from Blackboard Learn`,
    args: {
        id: { type: GraphQLString },
        ids: { type: new GraphQLList(GraphQLString) },
        filter: { type: DataSourceInputFilter },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            if (args.ids) {
                args.ids.map((id: any) => ctx.BbDataSourcesLoader.clear(id));
            } else if (args.id) {
                ctx.BbDataSourcesLoader.clear(args.id);
            } else {
                ctx.BbDataSourcesLoader.clear(args.filter || '');
            }
        }
        if (args.ids) {
            // return ctx.BbDataSourcesLoader.loadMany(args.ids);
            return ctx['BbDataSourcesLoader'].load(args.ids);
        } else if (args.id) {
            return ctx['BbDataSourcesLoader'].load(args.id);
        } else {
            return ctx['BbDataSourcesLoader'].load(args.filter || '');
        }
    }
};
