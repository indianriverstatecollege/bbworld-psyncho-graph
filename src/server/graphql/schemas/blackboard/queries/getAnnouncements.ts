import { GraphQLBoolean, GraphQLString, GraphQLList } from 'graphql';
import {
    AnnouncementsType,
    AnnouncementsInputFilter
} from '../../../examples/exports/announcement';

export const getBbAnnouncementsQuery = {
    type: AnnouncementsType,
    description: `Get the announcements from Blackboard Learn`,
    args: {
        id: { type: GraphQLString },
        ids: { type: new GraphQLList(GraphQLString) },
        filter: { type: AnnouncementsInputFilter },
        refresh: { type: GraphQLBoolean }
    },
    resolve: (_: any, args: any, ctx: any) => {
        if (args.refresh) {
            if (args.ids) {
                args.ids.map((id: any) => ctx.BbAnnouncementsLoader.clear(id));
            } else if (args.id) {
                ctx.BbAnnouncementsLoader.clear(args.id);
            } else {
                ctx.BbAnnouncementsLoader.clear(args.filter || '');
            }
        }
        if (args.ids) {
            // return ctx.BbAnnouncementsLoader.loadMany(args.ids);
            return ctx.BbAnnouncementsLoader.load(args.ids);
        } else if (args.id) {
            return ctx.BbAnnouncementsLoader.load(args.id);
        } else {
            return ctx.BbAnnouncementsLoader.load(args.filter || '');
        }
    }
};
