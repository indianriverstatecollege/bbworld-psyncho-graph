type Query {
    getBbUser(userId: String!, refresh: Boolean): BbUser
    getBbUsers(userIds: [String]!, refresh: Boolean): [BbUser]
    getAllBbUsers(refresh: Boolean): [BbUser]
}

type Mutation {
    createBbUser(input: BbUserInput!): BbUser
    updateBbUser(userId: String!, input: BbUserInput!): BbUser
    deleteBbUser(userId: String!): String
}

"""
No description for BbUser provided.
"""
type BbUser {
    """
    The primary ID of the user_
    """
    id: String

    """
    A secondary unique ID for the user_ Used by LTI launches and other inter_server operations_
    """
    uuid: String

    """
    An optional externally_defined unique ID for the user_ Defaults to the userName_

    Formerly known as 'batchUid'_
    """
    externalId: String

    """
    The ID of the data source associated with this user_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
    """
    dataSourceId: String

    """
    The userName property, shown in the UI_
    """
    userName: String

    """
    The user's student ID name or number as defined by the school or institution_
    """
    studentId: String

    """
    The education level of this user_
    """
    educationLevel: BbUserEducationLevel

    """
    The gender of this user_
    """
    gender: BbUserGender

    """
    The birth date of this user_
    """
    birthDate: String

    """
    The date this user was created_
    """
    created: String

    """
    The date this user was last modified_
    """
    modified: String

    """
    The date this user last logged in_
    """
    lastLogin: String

    """
    The primary and secondary institution roles assigned to this user_ The primary institution role is the first item in the list, followed by all secondary institution roles sorted alphabetically_

    **Since**: 3300_3_0
    """
    institutionRoleIds: [String]

    """
    The system roles (the administrative user roles in the UI) for this user_ The first role in this list is the user's primary system role, while the remaining are secondary system roles_
    """
    systemRoleIds: [BbSystemRoleEnum]

    """
    Settings controlling availability of the user account_
    """
    availability: BbUserAvailability

    """
    Properties used to build the user's display name_
    """
    name: BbUserName

    """
    The user's job information_
    """
    job: BbUserJob

    """
    The user's contact information_
    """
    contact: BbUserContact

    """
    The user's mailing address_
    """
    address: BbUserAddress

    """
    The user's localization settings_
    """
    locale: BbUserLocale
}

enum BbSystemRoleEnum {
    SystemAdmin
    SystemSupport
    CourseCreator
    CourseSupport
    AccountAdmin
    Guest
    User
    Observer
    Integration
    Portal
}

enum BbUserEducationLevel {
    K8
    HighSchool
    Freshman
    Sophomore
    Junior
    Senior
    GraduateSchool
    PostGraduateSchool
    Unknown
}

enum BbUserGender {
    Female
    Male
    Unknown
}

"""
Settings controlling availability of the user account.
"""
type BbUserAvailability {
    """
    Whether the user is available within the system_ Unavailable users cannot log in_
    """
    available: BbUserAvailabilityAvailable
}

enum BbUserAvailabilityAvailable {
    Yes
    No
    Disabled
}

"""
Properties used to build the user's display name.
"""
type BbUserName {
    """
    The given (first) name of this user_
    """
    given: String

    """
    The family (last) name of this user_
    """
    family: String

    """
    The middle name of this user_
    """
    middle: String

    """
    The other name (nickname) of this user_
    """
    other: String

    """
    The suffix of this user's name_ Examples: Jr_, III, PhD_
    """
    suffix: String

    """
    The title of this user_ Examples: Mr_, Ms_, Dr_
    """
    title: String
}

"""
The user's job information.
"""
type BbUserJob {
    """
    The user's job title_
    """
    title: String

    """
    The department the user belongs to_
    """
    department: String

    """
    The company the user works for_
    """
    company: String
}

"""
The user's contact information.
"""
type BbUserContact {
    """
    The user's home phone number_
    """
    homePhone: String

    """
    The user's mobile phone number_
    """
    mobilePhone: String

    """
    The user's business phone number_
    """
    businessPhone: String

    """
    The user's business fax number_
    """
    businessFax: String

    """
    The user's email address_
    """
    email: String

    """
    The URL of the user's personal website_
    """
    webPage: String
}

"""
The user's mailing address.
"""
type BbUserAddress {
    """
    The street address of the user_
    """
    street1: String

    """
    An additional field to store the street address of the user_
    """
    street2: String

    """
    The city the user resides in_
    """
    city: String

    """
    The state or province the user resides in_
    """
    state: String

    """
    The zip code or postal code the user resides in_
    """
    zipCode: String

    """
    The country the user resides in_
    """
    country: String
}

"""
The user's localization settings.
"""
type BbUserLocale {
    """
    The locale specified by the user_ This locale will be used anywhere the user is allowed to customize their locale courses may force a specific locale, overriding the user's locale preference_
    """
    id: String

    """
    The calendar type specified by the user_
    """
    calendar: BbUserLocaleCalendar
    firstDayOfWeek: BbUserLocaleFirstDayOfWeek
}

enum BbUserLocaleCalendar {
    Gregorian
    GregorianHijri
    Hijri
    HijriGregorian
}

enum BbUserLocaleFirstDayOfWeek {
    Sunday
    Monday
    Saturday
}

"""
No description for BbUser provided.
"""
input BbUserInput {
    """
    The primary ID of the user_
    """
    id: String

    """
    A secondary unique ID for the user_ Used by LTI launches and other inter_server operations_
    """
    uuid: String

    """
    An optional externally_defined unique ID for the user_ Defaults to the userName_

    Formerly known as 'batchUid'_
    """
    externalId: String

    """
    The ID of the data source associated with this user_ This may optionally be the data source's externalId using the syntax "externalId:math101"_
    """
    dataSourceId: String

    """
    The userName property, shown in the UI_
    """
    userName: String

    """
    The password that will be used for the user.
    """
    password: String

    """
    The user's student ID name or number as defined by the school or institution_
    """
    studentId: String

    """
    The education level of this user_
    """
    educationLevel: BbUserEducationLevel

    """
    The gender of this user_
    """
    gender: BbUserGender

    """
    The birth date of this user_
    """
    birthDate: String

    """
    The date this user was created_
    """
    created: String

    """
    The date this user was last modified_
    """
    modified: String

    """
    The date this user last logged in_
    """
    lastLogin: String

    """
    The primary and secondary institution roles assigned to this user_ The primary institution role is the first item in the list, followed by all secondary institution roles sorted alphabetically_

    **Since**: 3300_3_0
    """
    institutionRoleIds: [String]

    """
    The system roles (the administrative user roles in the UI) for this user_ The first role in this list is the user's primary system role, while the remaining are secondary system roles_
    """
    systemRoleIds: [BbSystemRoleEnum]

    """
    Settings controlling availability of the user account_
    """
    availability: BbUserAvailabilityInput

    """
    Properties used to build the user's display name_
    """
    name: BbUserNameInput

    """
    The user's job information_
    """
    job: BbUserJobInput

    """
    The user's contact information_
    """
    contact: BbUserContactInput

    """
    The user's mailing address_
    """
    address: BbUserAddressInput

    """
    The user's localization settings_
    """
    locale: BbUserLocaleInput
}

"""
Settings controlling availability of the user account.
"""
input BbUserAvailabilityInput {
    """
    Whether the user is available within the system_ Unavailable users cannot log in_
    """
    available: BbUserAvailabilityAvailable
}

"""
Properties used to build the user's display name.
"""
input BbUserNameInput {
    """
    The given (first) name of this user_
    """
    given: String

    """
    The family (last) name of this user_
    """
    family: String

    """
    The middle name of this user_
    """
    middle: String

    """
    The other name (nickname) of this user_
    """
    other: String

    """
    The suffix of this user's name_ Examples: Jr_, III, PhD_
    """
    suffix: String

    """
    The title of this user_ Examples: Mr_, Ms_, Dr_
    """
    title: String
}

"""
The user's job information.
"""
input BbUserJobInput {
    """
    The user's job title_
    """
    title: String

    """
    The department the user belongs to_
    """
    department: String

    """
    The company the user works for_
    """
    company: String
}

"""
The user's contact information.
"""
input BbUserContactInput {
    """
    The user's home phone number_
    """
    homePhone: String

    """
    The user's mobile phone number_
    """
    mobilePhone: String

    """
    The user's business phone number_
    """
    businessPhone: String

    """
    The user's business fax number_
    """
    businessFax: String

    """
    The user's email address_
    """
    email: String

    """
    The URL of the user's personal website_
    """
    webPage: String
}

"""
The user's mailing address.
"""
input BbUserAddressInput {
    """
    The street address of the user_
    """
    street1: String

    """
    An additional field to store the street address of the user_
    """
    street2: String

    """
    The city the user resides in_
    """
    city: String

    """
    The state or province the user resides in_
    """
    state: String

    """
    The zip code or postal code the user resides in_
    """
    zipCode: String

    """
    The country the user resides in_
    """
    country: String
}

"""
The user's localization settings.
"""
input BbUserLocaleInput {
    """
    The locale specified by the user_ This locale will be used anywhere the user is allowed to customize their locale courses may force a specific locale, overriding the user's locale preference_
    """
    id: String

    """
    The calendar type specified by the user_
    """
    calendar: BbUserLocaleCalendar
    firstDayOfWeek: BbUserLocaleFirstDayOfWeek
}
