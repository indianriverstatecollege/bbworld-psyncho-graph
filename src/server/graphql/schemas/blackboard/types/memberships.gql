type Query {
    getBbMembership(
        courseId: String!
        userId: String!
        refresh: Boolean
    ): BbMembership
    getBbMemberships(
        memberships: [MembershipsType]!
        refresh: Boolean
    ): [BbMembership]
    getAllBbMemberships(courseId: String!, refresh: Boolean): [BbMembership]
}

type Mutation {
    createBbMembership(
        courseId: String!
        userId: String!
        input: BbMembershipInput!
    ): BbMembership
    updateBbMembership(
        courseId: String!
        userId: String!
        input: BbMembershipInput!
    ): BbMembership
    deleteBbMembership(courseId: String!, userId: String!): String
}

"""
No description for BbMembership provided.
"""
type BbMembership {
    """
    The primary ID of the user_
    """
    userId: String

    """
    The primary ID of the course_
    """
    courseId: String

    """
    The course associated with the membership_
    Shown when adding the query parameter: expand=course_ And can be filtered with the "fields" query parameter, for example "fields=course_id,course_externalId"_
    **Since**: 3500_4_0
    """
    course: String

    """
    The primary ID of the child, cross_listed course, in which the user is directly enrolled_ </p> This field is read only in Learn versions 3000_11_0 through 3400_0_0_ As of 3400_1_0, this field is mutable_  </p> If this membership's course is a parent course in a cross_listed set, the childCourseId can be updated to move the membership enrollment between child courses and the parent course in  the set_  Patching the childCourseId to "null" will move the membership to the parent course_
    **Since**: 3000_11_0
    """
    childCourseId: String

    """
    The ID of the data source associated with this course_  This may optionally be the data source's externalId using the syntax "externalId:math101"_
    """
    dataSourceId: String

    """
    The date this membership was created_
    """
    created: String

    """
    Settings controlling availability of the course membership_
    """
    availability: String

    """
    The user's role in the course_

    These roles are also valid for an organization, although they are named differently in the UI_

    Custom course roles may also be referenced by their IDs_


    | Type      | Description
    | _________ | _________ |
    | Instructor | Has access to all areas in the Control Panel_ This role is generally given to those developing, teaching, or facilitating the class_ Instructors may access a course that is unavailable to students_ This role is customizable and may have different capabilities from what is documented here_ |
    | BbFacilitator | The facilitator is an instructor like role_ Facilitators are restricted versions of an instructor, in that they are able to deliver course instruction and administer all aspects of a pre_constructed course, but are not allowed to modify or alter the course_ This role is customizable and may have different capabilities from what is documented here_ |
    | TeachingAssistant | The teaching assistant role is that of a co_teacher_ Teaching assistants are able to administer all areas of a course_ Their only limitations are those imposed by the instructor or Blackboard administrator at your school_ This role is customizable and may have different capabilities from what is documented here_ |
    | CourseBuilder | Manages the course without having access to student grades_ This role is customizable and may have different capabilities from what is documented here_ |
    | Grader | Assists the instructor in the creation, management, delivery, and grading of items_ This role is customizable and may have different capabilities from what is documented here_ |
    | Student |  |
    | Guest | Has no access to the Control Panel_ Areas within the course are made available to guests, but typically they can only view course materials; they do not have access to tests or assessments, and do not have permission to post on discussion boards_ This role's behavior is immutable_ |
    """
    courseRoleId: BbMembershipCourseRoleId

    """
    If present, this date signals that the user associated with this membership has special access to the course regardless of the course's availability setting prior to the moment specified by this field_ After the date has passed, the membership will respect the course's availability_
    """
    bypassCourseAvailabilityUntil: String

    """
    This date signals the date this membership was used; in other words, the last date the user accessed the associated course or content contained by that course_

    The recording of any activity which would lead to this date getting updated does happen asynchronously in batches_ So, there may be some delay between an activity which would update this value and the availability of the new date_ It is recommended when using this value to note that activity within the last 5 minutes may not be taken into account_

    **Since**: 3300_9_0
    """
    lastAccessed: String
}

"""
No description for BbMembership provided.
"""
input BbMembershipInput {
    """
    The primary ID of the user_
    """
    userId: String

    """
    The primary ID of the course_
    """
    courseId: String

    """
    The course associated with the membership_
    Shown when adding the query parameter: expand=course_ And can be filtered with the "fields" query parameter, for example "fields=course_id,course_externalId"_
    **Since**: 3500_4_0
    """
    course: String

    """
    The primary ID of the child, cross_listed course, in which the user is directly enrolled_ </p> This field is read only in Learn versions 3000_11_0 through 3400_0_0_ As of 3400_1_0, this field is mutable_  </p> If this membership's course is a parent course in a cross_listed set, the childCourseId can be updated to move the membership enrollment between child courses and the parent course in  the set_  Patching the childCourseId to "null" will move the membership to the parent course_
    **Since**: 3000_11_0
    """
    childCourseId: String

    """
    The ID of the data source associated with this course_  This may optionally be the data source's externalId using the syntax "externalId:math101"_
    """
    dataSourceId: String

    """
    Settings controlling availability of the course membership_
    """
    availability: String

    """
    The user's role in the course_
    These roles are also valid for an organization, although they are named differently in the UI_
    Custom course roles may also be referenced by their IDs_

    | Type      | Description
    | _________ | _________ |
    | Instructor | Has access to all areas in the Control Panel_ This role is generally given to those developing, teaching, or facilitating the class_ Instructors may access a course that is unavailable to students_ This role is customizable and may have different capabilities from what is documented here_ |
    | BbFacilitator | The facilitator is an instructor like role_ Facilitators are restricted versions of an instructor, in that they are able to deliver course instruction and administer all aspects of a pre_constructed course, but are not allowed to modify or alter the course_ This role is customizable and may have different capabilities from what is documented here_ |
    | TeachingAssistant | The teaching assistant role is that of a co_teacher_ Teaching assistants are able to administer all areas of a course_ Their only limitations are those imposed by the instructor or Blackboard administrator at your school_ This role is customizable and may have different capabilities from what is documented here_ |
    | CourseBuilder | Manages the course without having access to student grades_ This role is customizable and may have different capabilities from what is documented here_ |
    | Grader | Assists the instructor in the creation, management, delivery, and grading of items_ This role is customizable and may have different capabilities from what is documented here_ |
    | Student |  |
    | Guest | Has no access to the Control Panel_ Areas within the course are made available to guests, but typically they can only view course materials; they do not have access to tests or assessments, and do not have permission to post on discussion boards_ This role's behavior is immutable_ |
    """
    courseRoleId: BbMembershipCourseRoleId
}

enum BbMembershipCourseRoleId {
    Instructor
    BbFacilitator
    TeachingAssistant
    CourseBuilder
    Grader
    Student
    Guest
}

input MembershipsType {
    courseId: String
    userId: String
}
