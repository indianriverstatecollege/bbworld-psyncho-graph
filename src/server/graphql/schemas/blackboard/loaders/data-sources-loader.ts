'use strict';
import * as DataLoader from 'dataloader';
import {
    getAllBbDataSources,
    getBbDataSource
} from '../../../../modules/dataFromAndToBb';
import { DataSourceMap } from '../cache-maps/data-source.map';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { BbDataSourcesIntConfig } from '../../../../configs/blackboard/blackboard.config';
async function batchDataSources(keys: any) {
    let refresh = false;
    let action = '';
    keys = keys.map((key: any) => {
        refresh = key[2];
        action = key[0];
        return key[1];
    });

    let results = [];

    for (let key of keys) {
        let data: any;
        const dbKey = /:/i.test(key) ? key.split(':')[1] : key;
        switch (action) {
            case 'ALL':
                data = await getDbData(
                    BbDataSourcesIntConfig.model,
                    {
                        $or: [
                            {
                                id: { $regex: new RegExp(dbKey), $options: 'i' }
                            },
                            {
                                externalId: {
                                    $regex: new RegExp(dbKey),
                                    $options: 'i'
                                }
                            }
                        ]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getAllBbDataSources(refresh);
                }

                for (let ds of data) {
                    BbDataSourcesLoader.prime(ds.id, ds);
                    BbDataSourcesLoader.prime(ds.externalId, ds);
                }
                BbDataSourcesLoader.prime(key, data);
                break;
            default:
                data = await getDbData(
                    BbDataSourcesIntConfig.model,
                    {
                        $or: [{ id: dbKey }, { externalId: dbKey }]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getBbDataSource(key, refresh);
                }

                data = data[0];
                BbDataSourcesLoader.prime(data.id, data);
                BbDataSourcesLoader.prime(data.externalId, data);
                break;
        }

        results.push(data);
    }
    return Promise.all(results);
}

export const BbDataSourcesLoader = new DataLoader(batchDataSources, {
    cacheKeyFn: (key: any) => {
        if (Array.isArray(key)) {
            switch (key[0]) {
                case 'ALL':
                    return key[0];
                default:
                    if (/:/i.test(key[1])) {
                        return key[1].split(':');
                    }
                    return key[1];
            }
        }
        return key;
    },
    cache: true,
    cacheMap: new DataSourceMap()
});
