'use strict';
import * as DataLoader from 'dataloader';
import {
    getAllBbCourses,
    getBbCourse
} from '../../../../modules/dataFromAndToBb';
import { CourseMap } from '../cache-maps/course.map';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { BbCoursesIntConfig } from '../../../../configs/blackboard/blackboard.config';
async function batchCourses(keys: any) {
    let refresh = false;
    let action = '';
    keys = keys.map((key: any) => {
        refresh = key[2];
        action = key[0];
        return key[1];
    });

    let results = [];

    for (let key of keys) {
        let data: any;
        const dbKey = /:/i.test(key) ? key.split(':')[1] : key;
        switch (action) {
            case 'ALL':
                data = await getDbData(
                    BbCoursesIntConfig.model,
                    {
                        $or: [
                            {
                                id: { $regex: new RegExp(dbKey), $options: 'i' }
                            },
                            {
                                externalId: {
                                    $regex: new RegExp(dbKey),
                                    $options: 'i'
                                }
                            }
                        ]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getAllBbCourses(refresh);
                }

                for (let t of data) {
                    BbCoursesLoader.prime(t.id, t);
                    BbCoursesLoader.prime(t.externalId, t);
                }

                BbCoursesLoader.prime(key, data);
                break;
            default:
                data = await getDbData(
                    BbCoursesIntConfig.model,
                    {
                        $or: [{ id: dbKey }, { externalId: dbKey }]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getBbCourse(key, refresh);
                }

                data = data[0];
                BbCoursesLoader.prime(data.id, data);
                BbCoursesLoader.prime(data.externalId, data);
                break;
        }

        results.push(data);
    }
    console.log(results);
    return Promise.all(results);
}

export const BbCoursesLoader = new DataLoader(batchCourses, {
    cacheKeyFn: (key: any) => {
        if (Array.isArray(key)) {
            switch (key[0]) {
                case 'ALL':
                    return key[0];
                default:
                    if (/:/i.test(key[1])) {
                        return key[1].split(':');
                    }
                    return key[1];
            }
        }
        return key;
    },
    cache: false,
    cacheMap: new CourseMap()
});
