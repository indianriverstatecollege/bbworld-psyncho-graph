'use strict';
import * as DataLoader from 'dataloader';
import { getAllBbTerms, getBbTerm } from '../../../../modules/dataFromAndToBb';
import { TermMap } from '../cache-maps/term.map';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { BbTermsIntConfig } from '../../../../configs/blackboard/blackboard.config';
async function batchTerms(keys: any) {
    let refresh = false;
    let action = '';
    keys = keys.map((key: any) => {
        refresh = key[2];
        action = key[0];
        return key[1];
    });

    let results = [];

    for (let key of keys) {
        let data: any;
        const dbKey = /:/i.test(key) ? key.split(':')[1] : key;
        switch (action) {
            case 'ALL':
                data = await getDbData(
                    BbTermsIntConfig.model,
                    {
                        $or: [
                            {
                                id: { $regex: new RegExp(dbKey), $options: 'i' }
                            },
                            {
                                externalId: {
                                    $regex: new RegExp(dbKey),
                                    $options: 'i'
                                }
                            }
                        ]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getAllBbTerms(refresh);
                }

                for (let t of data) {
                    BbTermsLoader.prime(t.id, t);
                    BbTermsLoader.prime(t.externalId, t);
                }
                BbTermsLoader.prime(key, data);
                break;
            default:
                data = await getDbData(
                    BbTermsIntConfig.model,
                    {
                        $or: [{ id: dbKey }, { externalId: dbKey }]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getBbTerm(key, refresh);
                }

                data = data[0];
                BbTermsLoader.prime(data.id, data);
                BbTermsLoader.prime(data.externalId, data);
                break;
        }

        results.push(data);
    }
    console.log(results);
    return Promise.all(results);
}

export const BbTermsLoader = new DataLoader(batchTerms, {
    cacheKeyFn: (key: any) => {
        if (Array.isArray(key)) {
            switch (key[0]) {
                case 'ALL':
                    return key[0];
                default:
                    if (/:/i.test(key[1])) {
                        return key[1].split(':');
                    }
                    return key[1];
            }
        }
        return key;
    },
    cache: true,
    cacheMap: new TermMap()
});
