'use strict';
import * as DataLoader from 'dataloader';
import { BbAnnouncementsIntConfig } from '../../../../configs/blackboard/blackboard.config';
import { Integration } from '../../../../modules/classes/integration';
import { AnncouncementsMap } from '../cache-maps/announcements.map';
import { inspect } from 'util';
const getAnnouncement = (arg: any) => {
    // if arg is a string, single announcement
    // if arg is an array, then many announcements
    // else if object, then filter
    const intConfig = { ...BbAnnouncementsIntConfig };
    let integration: Integration;

    switch (typeof arg) {
        case 'string':
            intConfig.params = {
                announcementId: arg
            };
            integration = new Integration(intConfig);
            return (integration as any).fetchData();
        case 'object':
            if (Array.isArray(arg)) {
                return Promise.all(arg.map((id: any) => {
                    const config = {...intConfig};
                    config.params = {
                        announcementId: id
                    }
                    const _int = new Integration(config);
                    return (_int as any).fetchData()
                }))
                    .then((data: any) => {
                        
                        // rebuild the payload correctly
                        let _data = [];
                        for(const announcement of data) {
                            _data.push(announcement.results[0]);
                        }
                        // console.log('IDS:', inspect(_data, true, 99, true));
                        return {
                            results: _data
                        }
                    });
            } else {
                integration = new Integration(intConfig);
                // filter object
                if (arg === '') {
                    return (integration as any).fetchData();
                }

                return (integration as any).fetchData().then((data: any) => {
                    data.results = data.results.filter((el: any) => {
                        let res = false;
                        for (const key of Object.keys(arg)) {
                            res = RegExp(`${arg.filter[key]}`, 'gi').test(
                                el[key]
                            );
                            // if one of the filters does not match, stop
                            if (!res) {
                                break;
                            }
                        }
                        return res;
                    });
                    return data;
                });
            }
    }
};

export const BbAnnouncementsLoader = new DataLoader(
    (keys: any) => {
        return new Promise(async (resolve: any, reject: any) => {
            console.log(keys);
            const announcements = await Promise.all(keys.map(getAnnouncement));

            if (announcements !== [null]) {
                resolve(announcements);
            } else {
                reject({
                    errors: {
                        message: 'Announcements was returned or assigned as null!'
                    }
                });
            }
        });
    },
    {
        cacheKeyFn: key => JSON.stringify(key),
        cache: true,
        cacheMap: new AnncouncementsMap()
    }
);
