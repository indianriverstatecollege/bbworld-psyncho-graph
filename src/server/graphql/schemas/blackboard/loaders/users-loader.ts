'use strict';
import * as DataLoader from 'dataloader';
import { getAllBbUsers, getBbUser } from '../../../../modules/dataFromAndToBb';
import { UserMap } from '../cache-maps/user.map';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { BbUsersIntConfig } from '../../../../configs/blackboard/blackboard.config';
async function batchUsers(keys: any) {
    let refresh = false;
    let action = '';
    keys = keys.map((key: any) => {
        refresh = key[2];
        action = key[0];
        return key[1];
    });

    let results = [];

    for (let key of keys) {
        let data: any;
        const dbKey = /:/i.test(key) ? key.split(':')[1] : key;
        switch (action) {
            case 'ALL':
                data = await getDbData(
                    BbUsersIntConfig.model,
                    {
                        $or: [
                            {
                                id: { $regex: new RegExp(dbKey), $options: 'i' }
                            },
                            {
                                externalId: {
                                    $regex: new RegExp(dbKey),
                                    $options: 'i'
                                }
                            }
                        ]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getAllBbUsers(refresh);
                }

                for (let t of data) {
                    BbUsersLoader.prime(t.id, t);
                    BbUsersLoader.prime(t.externalId, t);
                }

                BbUsersLoader.prime(key, data);
                break;
            default:
                data = await getDbData(
                    BbUsersIntConfig.model,
                    {
                        $or: [{ id: dbKey }, { externalId: dbKey }]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    data = await getBbUser(key, refresh);
                }

                data = data[0];
                BbUsersLoader.prime(data.id, data);
                BbUsersLoader.prime(data.externalId, data);
                break;
        }

        results.push(data);
    }
    console.log(results);
    return Promise.all(results);
}

export const BbUsersLoader = new DataLoader(batchUsers, {
    cacheKeyFn: (key: any) => {
        if (Array.isArray(key)) {
            switch (key[0]) {
                case 'ALL':
                    return key[0];
                default:
                    if (/:/i.test(key[1])) {
                        return key[1].split(':');
                    }
                    return key[1];
            }
        }
        return key;
    },
    cache: true,
    cacheMap: new UserMap()
});
