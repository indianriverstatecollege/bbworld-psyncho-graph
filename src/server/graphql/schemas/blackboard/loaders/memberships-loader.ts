'use strict';
import * as DataLoader from 'dataloader';
import {
    getAllBbMemberships,
    getBbMembership
} from '../../../../modules/dataFromAndToBb';
import { MembershipMap } from '../cache-maps/membership.map';
import { getDbData } from '../../../../modules/dataFromAndToDb';
import { BbMembershipsIntConfig } from '../../../../configs/blackboard/blackboard.config';
async function batchMemberships(keys: any) {
    let refresh = false;
    let action = '';
    let courseId = '';
    keys = keys.map((key: any) => {
        refresh = key[2];
        action = key[0];
        return key[1];
    });

    let results = [];
    return;
    for (let key of keys) {
        let data: any;
        const dbKey = /:/i.test(key) ? key.split(':')[1] : key;
        switch (action) {
            case 'ALL':
                data = await getDbData(
                    BbMembershipsIntConfig.model,
                    {
                        $and: [
                            {
                                userId: dbKey
                            },
                            {
                                courseId: dbKey
                            }
                        ]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    // data = await getAllBbMemberships(refresh);
                }

                for (let t of data) {
                    BbMembershipsLoader.prime(t.id, t);
                    BbMembershipsLoader.prime(t.externalId, t);
                }

                BbMembershipsLoader.prime(key, data);
                break;
            default:
                data = await getDbData(
                    BbMembershipsIntConfig.model,
                    {
                        $or: [{ id: dbKey }, { externalId: dbKey }]
                    },
                    { isMany: true }
                );

                if (refresh || data.length <= 0) {
                    // data = await getBbMembership(key, refresh);
                }

                data = data[0];
                BbMembershipsLoader.prime(data.id, data);
                BbMembershipsLoader.prime(data.externalId, data);
                break;
        }

        results.push(data);
    }
    console.log(results);
    return Promise.all(results);
}

export const BbMembershipsLoader = new DataLoader(batchMemberships, {
    cacheKeyFn: (key: any) => {
        console.log(key);
        if (Array.isArray(key)) {
            switch (key[0]) {
                case 'ALL':
                    return key[1];
                default:
                    let _key = '';
                    if (/:/i.test(key[1])) {
                        _key = '';
                        return key[1].split(':');
                    }
                    return key[1];
            }
        }
        return key;
    },
    cache: true,
    cacheMap: new MembershipMap()
});
