import {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLBoolean
} from 'graphql';
import { activeCourseEnrollmentsByIdQuery } from './workday/queries/getActiveCourses';
import { helloQuery } from './psyncho/hello';
import { studentReportInfoQuery } from './workday/queries/getStudentReportInfo';
import { advisorReportInfoQuery } from './workday/queries/getAdvisorReportInfo';
import { courseListingInfoQuery } from './workday/queries/getCourseListingInfo';
import { educationalInstInfoQuery } from './workday/queries/getEducationalInstInfo';
import { courseInfoQuery } from './workday/queries/getCourseInformation';
import { getBbAnnouncementsQuery } from './blackboard/queries/getAnnouncements';
// import { getBbDataSourcesQuery } from './blackboard/queries/getDataSources';
import { graphQLResolverFactory } from './psyncho/resolverFactory';
import {
    DataSourcesType,
    DataSourceInputFilter
} from '../examples/exports/dataSource.types';
const BbDataSourceArgs = new GraphQLInputObjectType({
    name: 'BbDataSourceArgs',
    description: 'Input Args Type',
    fields: () => ({
        id: { type: GraphQLString },
        ids: { type: new GraphQLList(GraphQLString) },
        filter: { type: DataSourceInputFilter },
        refresh: { type: GraphQLBoolean }
    })
});

const BbDataSourceOptions = new GraphQLInputObjectType({
    name: 'BbDataSourceOptions',
    description: 'Options for this resolver',
    fields: () => ({
        resolverAction: { type: GraphQLString },
        resolverCache: { type: GraphQLString },
        resolverCacheFunc: { type: GraphQLString },
        resolverLoadFunc: { type: GraphQLString }
    })
});

const BbDataSourcesResolver = graphQLResolverFactory({
    type: DataSourcesType,
    description: `Get the data sources from Blackboard Learn`,
    args: {
        input: { type: BbDataSourceArgs },
        options: { type: BbDataSourceOptions }
    },
    resolverMethod: 'query',
    resolverType: 'BbDataSourcesLoader'
});
const RootQueryType = new GraphQLObjectType({
    name: 'RootQueryType',
    description: 'A set of BbQL Queries for your appetite!',
    fields: () => ({
        hello: helloQuery,
        activeCourseEnrollmentsById: activeCourseEnrollmentsByIdQuery,
        studentInfoById: studentReportInfoQuery,
        advisorInfoById: advisorReportInfoQuery,
        courseListingInfoById: courseListingInfoQuery,
        educationalInstInfo: educationalInstInfoQuery,
        courseInfo: courseInfoQuery,
        getBbAnnouncements: getBbAnnouncementsQuery,
        // getBbDataSources: getBbDataSourcesQuery
        getBbDataSources: BbDataSourcesResolver
    })
});

// tslint:disable-next-line:no-commented-code
// const RootMutationType = new GraphQLObjectType({
//     name: 'RootMutationType',
//     description: 'A set of BbQL Mutation for your appetite!',
//     fields: () => ({
//         createDataSource: CreateDataSourceMutation,
//     }),
// });

export const PsynchoSchema = new GraphQLSchema({
    query: RootQueryType
    // mutation: RootMutationType,
});
